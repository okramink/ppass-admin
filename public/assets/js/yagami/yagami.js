  YagamiBox = new function() {
    this.init = function( options ){
      var defaults = {
         id    : 'TheLigthBox',
         width : '500px',
         height :'500px',
         url : null,
         auto : true,
         data : {},
         headers: true,
         title : 'My Window',
         animated:false,
         autohide : false,
         bindkey : true,
         showTitle:true,
         showCloseBtn:true,
         content:"auto",
         background:'#fff',
         afterOpen: function(){},
         onBeforeOpen: function(){},
         onLoaded: function(){},
         afterLoaded:function(){},
         onBeforeLoaded:function(){},
         onBeforeClose:function(){},
         onCheckBoxMain: function(){},
         setDomContent: function(){}
      }
      //the options extends
      this.opts = $.extend(defaults, options);

      //open the model
      this.open();

      return this;

    }
    this.open = function(){
      //this level
      var that = this;

      var body = document.body,
          html = document.documentElement;

      //onBeForeOpen callback
      this.opts.onBeforeOpen.call(this,{});

      document.body.style.overflow = 'hidden';
      window.scrollTo(0,0);

      var overlay       = document.createElement("div");
      overlay.className     = 'yagami-overlay';
      document.body.appendChild(overlay);

      var windowPanelWidth   = $(window).width();
      var windowPanelHeight  = $(window).height();

      var thewindow          = document.createElement("div");
      thewindow.className    = 'yagami';
      thewindow.style.top    = '5%';
      thewindow.style.width  = '1px';
      thewindow.style.height = '1px';
      overlay.appendChild(thewindow);

      if(this.opts.showTitle){
        var theTitle     = document.createElement("div");
        theTitle.className = 'title';
        theTitle.innerHTML = this.opts.title;
        thewindow.appendChild(theTitle);
      }

      var thewindowContent              = document.createElement("div");
      thewindowContent.className        = 'yagami-content';
      thewindow.appendChild(thewindowContent);

      if(this.opts.showCloseBtn){
        var theclose         = document.createElement("div");
        theclose.className   = 'yagami-close';
        theclose.title       = 'Close';
        theclose.alt         = 'Close';
        theclose.onclick     = function(){
          that.close();
        }
        thewindow.appendChild(theclose);
      }

      var xp = $(window).width();
      var yp = 100;
      var zp = parseFloat(this.opts.width);
      var wp = zp * xp / yp;

      var windowPanelLeft    = ((wp) - ( windowPanelWidth / 4 )  );

      that.opts.setDomContent.call(this,thewindowContent);

      
      //set propierts to window
      $(thewindow).css({width : that.opts.width , background:this.opts.background, opacity:100});
      $(thewindow).css({height:this.opts.height});

      var titleHeight   =  $(theTitle).height() ?  $(theTitle).height() : 0;
      var contentHeight = $(thewindow).height() ;    

      
      $(thewindow).css({height: ( contentHeight + titleHeight ) +'px'});
      
      $(thewindowContent).css({height:this.opts.content});
      

      //set final render position 
      var dd = $(overlay).height() - $(thewindow).height()      
      $(thewindow).css({top:dd/3+'px'});   
     
      that.opts.afterOpen.call(this,{});


      if( this.opts.bindkey ){
        $( document ).on( 'keydown', function ( e ) {
          if ( e.keyCode === 27 ) {
             that.close();
             $( document ).off( "keydown" );
          }
        });
      }

      if( this.opts.autohide )
      $(overlay).click(function(){
        $(this).remove();
      });

      // $(window).resize(function(){
      //   var windowPanelWidth   = $(window).width();
      //   var windowPanelHeight  = $(window).height();
      //   thewindowContent.style.height     = parseInt(windowPanelHeight) - 310 + 'px';
      //   $(thewindow).css({height:this.opts.height }); //,maxHeight: windowPanelHeight - 250 + 'px'
      // });
    }
    this.getRemote = function(container){
      var that = this;
      that.opts.onBeforeLoaded.call(this,{});
      if(this.opts.url) {
        $.ajax({
          type: 'GET',
          url: this.opts.url,
          data: this.opts.data,
          dataType: 'html',
          asyn:false,
          success:function( r ){
            $(container).html(r);
            that.opts.afterLoaded.call(this,{});
          }
        });
      }
    }
  this.close = function (){
    $('.yagami-overlay').remove();
    document.body.style.overflow = 'auto';
  }
}