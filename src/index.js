/*eslint-disable*/

import React from 'react';
import ReactDOM from 'react-dom';
import App from './app';
import 'react-select/dist/react-select.css';


ReactDOM.render(<App />,document.getElementById('root'));

// import registerServiceWorker from './registerServiceWorker';
// registerServiceWorker();
