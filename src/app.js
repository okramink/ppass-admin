import React           from 'react';

import {
combineReducers,
createStore ,
applyMiddleware }      from 'redux';
import { Provider }    from 'react-redux';
import thunk 		       from 'redux-thunk';

//router
import Routes from './router';

//reducers
import auth 			    from './reducers/auth';
import devices        from './reducers/devices';
import users          from './reducers/users';
import posts          from './reducers/posts';
import passports      from './reducers/passports';
import files          from './reducers/files';
import affiliates     from './reducers/affiliates';
import playlists      from './reducers/playlists';
import bookings      from './reducers/bookings';

import { loadTranslations, setLocale, syncTranslationWithStore, i18nReducer } from 'react-redux-i18n';
import {translations}  from './translations';
import Globals         from './globals';

import { SocketProvider } from 'socket.io-react';
import io                 from 'socket.io-client';


//
import { composeWithDevTools } from 'redux-devtools-extension';

//combined reducers
const reducers  = combineReducers({
  auth,
  devices,
  users,
  posts,
  passports,
  files,
  affiliates,
  playlists,
  bookings,
  i18n: i18nReducer
});


let componse = composeWithDevTools( applyMiddleware(thunk))

//store
let store = createStore(
  reducers, 
  componse
);

let lang  = window.localStorage.getItem('lang') || 'en';

syncTranslationWithStore(store)
store.dispatch(loadTranslations(translations));
store.dispatch(setLocale(lang));

let socket = io.connect(Globals.socket,  { transports: ['websocket', 'polling']} );

export default class App extends React.Component{
  render(){
    return(
      <SocketProvider socket={socket}>
        <Provider store={store} socket={socket}>
            <Routes />
        </Provider>
      </SocketProvider>
    )
  }
}