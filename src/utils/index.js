import 'whatwg-fetch'

//Request
//document.querySelector('form')
export const Request = (source,data={},label='') => {
    return fetch(source,{
    	body:data,
	    method: 'POST',
	    headers:{ 'Accept': 'application/json', 'Authorization':`bearer ${ window.localStorage.getItem('appToken') }`}
	})
  .catch(e =>{
    window.bootbox.alert('Ha ocurrido un error de comunicación con el servidor, favor de intentar nuevamente!', label )
    //return e;
  })
	.then( response => {
    if(response) {
      return response.json()
    }else{
      return response;
    }
  })
  .then( response => {
     if( response && response.code === 1){
        window.bootbox.alert('Sesión finalizada!.')
        window.location.href = '/login';
     }else{
      return response;
     }
  })
};

//Form
export const Form = (data) => {
     const formData  = new FormData();
     for(let name in data) {
          if(typeof data[name] === 'object' ){
            let tmp = data[name];
              for( let x in tmp){ 
                formData.append(name, JSON.stringify(tmp[x]));
              }
        }else{
          formData.append(name,data[name]);
        }       
     }
  return formData;
};

