/*eslint-disable*/
import React     from 'react';
import PropTypes from 'prop-types';
import {
  Router, Switch, Route, Redirect
} from 'react-router-dom'

import createBrowserHistory from 'history/createBrowserHistory'


// Layouts
import MainLayout       from  './layouts/main';
import LoginLayout      from  './layouts/login';
import EmptyLayout      from  './layouts/empty';
import FullLayout       from  './layouts/full';

// Routes
import Dashboard        from './routes/dashboard';
import Profile          from './routes/profile';
import Settings         from './routes/settings';
import Affiliates       from './routes/affiliates';
import AffiliatesFa     from './routes/affiliatesfa';
import affiliatesfaonlycredit     from './routes/affiliatesfaonlycredit';
import AffiliatesViews  from './routes/affiliates/view';
import AffiliatesViewsonlycredit  from './routes/affiliates/viewonlycredit';
import Passports        from './routes/passports';
import PassportsPlus    from './routes/passportsPlus';
import Bookings         from './routes/bookings';
import PassportsFa      from './routes/passportsFa';
import Devices          from './routes/devices';
import Locations        from './routes/locations';
import Posts            from './routes/posts';
import PostsAdd         from './routes/posts/add';
import roomUpgrade      from './routes/roomUpgrade';

import PostsFa          from './routes/postsFa';
import PostsAddFa       from './routes/posts/addFa';

import UsersFa            from './routes/usersfa';
import Users            from './routes/users';
import Layouts          from './routes/layouts';
import Login            from './routes/login';
import Notfound         from './routes/404';

import jwtDecode        from 'jwt-decode';

//
import Notification     from './routes/notification';

import {
 getToken,
 set
} from './actions/auth';

const history = createBrowserHistory()

class Routes extends React.Component {
  static get contextTypes() {
    return {
      store: PropTypes.object.isRequired
    }
  }
   constructor(props,context){
    super(props,context);
    this.context = context;
    this.store   = this.context.store;
    this.state = {
      response : null,
      data: null
    }
  }
  componentWillMount(){
    var isAuthenticated = sessionStorage.getItem('isAuthenticated' );
    var token           = window.localStorage.getItem('appToken');
     if( isAuthenticated ){
        this.store.dispatch(set(JSON.parse(sessionStorage.getItem('appUser'))));
     }else{
      if( token ){
         this.store.dispatch(getToken({token:token}))
         .then(response =>{
            if(response.code === 0){
              var user    = jwtDecode(response.token);
              window.localStorage.setItem('appToken',  response.token);
              sessionStorage.setItem('appToken', (response.token));
              sessionStorage.setItem('appUser',  JSON.stringify(user));
              sessionStorage.setItem('isAuthenticated',true);
              sessionStorage.setItem('response',response);
              this.setState({response:true});
              this.store.dispatch(set(user));
            }
            if(response.error){
              this.sessionError()
            }
         })
         .catch(error =>{
           this.sessionError()
         })
      }else{
        this.setState({response:true});
        if( !sessionStorage.getItem('isAuthenticated' ) ){
          history.push('/login');
        }
      }
     }
  }
  sessionError(){
    sessionStorage.removeItem('isAuthenticated');
    window.localStorage.removeItem('appToken');
    sessionStorage.removeItem('response_user')
    history.push('/login');
  }
  render(){
    let role = null
    
    if(JSON.parse(sessionStorage.getItem('appUser')) != null){
      role = JSON.parse(sessionStorage.getItem('appUser'))
      console.log(role)
      if(role.role == 'operator'){
        return(
          <Router history={history} >
              <Switch>
                  <MainLayout  exact path="/"       component={AffiliatesFa} /> 
                  <MainLayout  exact path="/affiliates"     component={AffiliatesFa} /> 
                  <MainLayout  path="/affiliates/view/:id"  component={AffiliatesViews} />           
                  <LoginLayout path="/login"            component={Login} />
                  <MainLayout  path="/bookings"    component={Bookings} />    
                  <MainLayout  path="/roomUpgrade"  component={roomUpgrade} />  
                  <MainLayout  exact path="/postsfa"  component={PostsFa} />
                  <MainLayout  path="/postsfa/addfa"    component={PostsAddFa} />
                  <MainLayout  path="/postsfa/updatefa/:id"    component={PostsAddFa} /> 
                  
                  <EmptyLayout component={Notfound} />
                </Switch>
          </Router>
        )
      }else if(role.role == 'operator-adds'){
        return(
          <Router history={history} >
              <Switch>
                  <MainLayout  exact path="/"       component={AffiliatesFa} /> 
                  <MainLayout exact path="/affiliates"     component={affiliatesfaonlycredit} /> 
                  <MainLayout  path="/affiliates/view/:id"  component={AffiliatesViewsonlycredit} />           
                  <LoginLayout path="/login"            component={Login} />
                  <EmptyLayout component={Notfound} />
                </Switch>
          </Router>
        )
      }else{
        if(role.email == 'gv@preferencepass.com'){
          return(
            <Router history={history} >
                <Switch>
                    <MainLayout  exact path="/"       component={Dashboard} />
                    <MainLayout  path="/profile"      component={Profile} />
                    <MainLayout  path="/settings"     component={Settings} />
                    <MainLayout  path="/passports"    component={PassportsPlus} />
                    <MainLayout  path="/bookings"    component={Bookings} />
                    <MainLayout  exact path="/posts"  component={Posts} />
                    <MainLayout  path="/posts/add"    component={PostsAdd} />
                    <MainLayout  path="/posts/update/:id"    component={PostsAdd} />
                    <MainLayout  exact path="/postsfa"  component={PostsFa} />
                    <MainLayout  path="/postsfa/addfa"    component={PostsAddFa} />
                    <MainLayout  path="/postsfa/updatefa/:id"    component={PostsAddFa} /> 
                    <MainLayout  path="/usersfa"            component={UsersFa} /> 
                    <MainLayout  path="/users"            component={Users} />
                    <MainLayout  exact path="/affiliates"       component={Affiliates} />
                    <MainLayout  path="/affiliates/view/:id"  component={AffiliatesViews} />     
                    <MainLayout  path="/roomUpgrade"  component={roomUpgrade} />           
                    <LoginLayout path="/login"            component={Login} />
                    <MainLayout path="/notification"      component={Notification} />
      
                    <EmptyLayout component={Notfound} />
                  </Switch>
            </Router>
          )
        }else if(role.email == 'germanv.poblete@gmail.com') {
          return(
            <Router history={history} >
                <Switch>
                  <MainLayout  exact path="/"       component={Dashboard} />
                  <MainLayout  path="/profile"      component={Profile} />
                  <MainLayout  path="/settings"     component={Settings} />
                  <MainLayout  path="/passports"    component={Passports} />
                  <MainLayout  exact path="/posts"  component={Posts} />
                  <MainLayout  path="/posts/add"    component={PostsAdd} />
                  <MainLayout  path="/posts/update/:id"    component={PostsAdd} />
                  <MainLayout  exact path="/postsfa"  component={PostsFa} />
                  <MainLayout  path="/postsfa/addfa"    component={PostsAddFa} />
                  <MainLayout  path="/postsfa/updatefa/:id"    component={PostsAddFa} /> 
                  <MainLayout  path="/users"            component={Users} />
                  <MainLayout  exact path="/affiliates"       component={Affiliates} />
                  <MainLayout  path="/affiliates/view/:id"  component={AffiliatesViews} />              
                  <LoginLayout path="/login"            component={Login} />
    
                  <EmptyLayout component={Notfound} />
                </Switch>
            </Router>
          )
        }else{
          return(
            <Router history={history} >
                <Switch>
                    <MainLayout  exact path="/"       component={Dashboard} />
                    <MainLayout  path="/profile"      component={Profile} />
                    <MainLayout  path="/settings"     component={Settings} />
                    <MainLayout  path="/passports"    component={Passports} />
                    <MainLayout  exact path="/posts"  component={Posts} />
                    <MainLayout  path="/posts/add"    component={PostsAdd} />
                    <MainLayout  path="/posts/update/:id"    component={PostsAdd} />
                    <MainLayout  path="/postsfa/addfa"    component={PostsAddFa} />
                    <MainLayout  path="/postsfa/updatefa/:id"    component={PostsAddFa} /> 
                    <MainLayout  path="/users"            component={Users} />
                    <MainLayout  exact path="/affiliates"       component={Affiliates} />
                    <MainLayout  path="/affiliates/view/:id"  component={AffiliatesViews} />              
                    <LoginLayout path="/login"            component={Login} />
      
                    <EmptyLayout component={Notfound} />
                  </Switch>
            </Router>
          )
        }
      }
    }else{
      return (
        <Router history={history} >
            <Switch>           
                <LoginLayout path="/login"            component={Login} />
  
                <EmptyLayout component={Notfound} />
              </Switch>
        </Router>
      )
    }
    
  }
}
export default Routes;