import {Request,Form}  from '../utils';
import Globals         from '../globals';

export const  REQUEST_USERS    = 'REQUEST_USERS';
export const  SET_DATA_USERS   = 'SET_DATA_USERS';
export const  SET_DATA_USERSFA = 'SET_DATA_USERSFA';
export const  SET_DATA_ADMINS  = 'SET_DATA_ADMINS';


const load = () =>{
return {
    type: REQUEST_USERS,
    loader:true
  };
}

const setData = (data) =>{
return {
    type  :SET_DATA_USERS,
    loader:false,
    data  :data 
  };
}
const setDataFa = (data) =>{
  return {
      type  :SET_DATA_USERSFA,
      loader:false,
      data  :data 
    };
  }

const setDataAdmins = (data) =>{
return {
    type  :SET_DATA_ADMINS,
    loader:false,
    data  :data 
  };
}

//--------------------------------------------------------------------
export const loadUsers = (data) =>{   
   var service = Globals.api+'/users/table';
   var form    = Form(data);
    return  dispatch => {
    dispatch(load());
    return Request(service,form)
    .then(response =>{
        dispatch(setData(response))
    })
  }
}

export const loadUsersFA = (data) =>{   
  var service = Globals.api+'/usersfa/table';
  var form    = Form(data);
   return  dispatch => {
   dispatch(load());
   return Request(service,form)
   .then(response =>{
       dispatch(setDataFa(response))
   })
 }
}

export const loadAdmins = (data) =>{   
   var service = Globals.api+'/users/admins';
   var form    = Form(data);
    return  dispatch => {
    dispatch(load());
    return Request(service,form)
    .then(response =>{
        dispatch(setDataAdmins(response))
    })
  }
}

export const save = (data) =>{   
   var service = Globals.api+'/users/save';
   var form    = Form(data);
    return  dispatch => {
    return Request(service,form)
    .then(response =>{
        dispatch(loadUsers(response))
         window.Light.close();
    })
  }
}
export const saveFA = (data) =>{   
  var service = Globals.api+'/usersfa/saveFA';
  var form    = Form(data);
   return  dispatch => {
   return Request(service,form)
   .then(response =>{
       dispatch(loadUsers(response))
        window.Light.close();
   })
 }
}

export const remove = (data) =>{   
   var service = Globals.api+'/users/remove';
   var form    = Form(data);
    return  dispatch => {
    // dispatch(requestRemove());
    return Request(service,form)
    .then(response =>{
        dispatch(loadUsers())
        window.bootbox.alert('Los datos fueron eliminados correctamente');
    })
  }
}

export const removeFa = (data) =>{   
  var service = Globals.api+'/usersfa/remove';
  var form    = Form(data);
   return  dispatch => {
   // dispatch(requestRemove());
   return Request(service,form)
   .then(response =>{
       dispatch(loadUsersFA())
       window.bootbox.alert('Los datos fueron eliminados correctamente');
   })
 }
}

export const sendPayment = (data) =>{   
   var service = Globals.api+'/users/payment';
   var form    = Form(data);
    return  dispatch => {    
    return Request(service,form)   
  }
}
