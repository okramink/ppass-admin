import {Request,Form}  from '../utils';
import Globals         from '../globals';


export const  SET_POST         = 'SET_POST';
export const  REQUEST_POSTS    = 'REQUEST_POSTS';
export const  SET_DATA_POSTS   = 'SET_DATA_POSTS';


export const  ADD_POSTS_TIME         = 'ADD_POSTS_TIME';
export const  ADD_POSTS_TAGS         = 'ADD_POSTS_TAGS';
export const  ADD_POSTS_CATEGORIES   = 'ADD_POSTS_CATEGORIES';

export const  SET_POST_HTML_CONTENT      = 'SET_POST_HTML_CONTENT';
export const  SET_POST_IMAGE_FEATURED     = 'SET_POST_IMAGE_FEATURED';
export const  REMOVE_POST_IMAGE_FEATURED  = 'REMOVE_POST_IMAGE_FEATURED';

export const  SET_ALERT  = 'SET_ALERT';

const load = () =>{
return {
    type: REQUEST_POSTS,
    loader:true
  };
}

const setData = (data) =>{
return {
    type  :SET_DATA_POSTS,
    loader:false,
    data  :data
  };
}

//--------------------------------------------------------------------
export const loadPosts = (data) =>{
   var service = Globals.api+'/posts/table';
   var form    = Form(data);
    return  dispatch => {
    dispatch(load());
    return Request(service,form)
    .then(response =>{
        dispatch(setData(response))
    })
  }
}

export const loadpostcredit = (data) =>{
  var service = Globals.api+'/postfa/allData';
  var form    = Form(data);
   return  dispatch => {
   dispatch(load());
   return Request(service,form)
   .then(response =>{
       //dispatch(setData(response))
       return response
   })
 }
}

export const loadPostsFa = (data) =>{
  var service = Globals.api+'/postfa/table';
  var form    = Form(data);
   return  dispatch => {
   dispatch(load());
   return Request(service,form)
   .then(response =>{
       dispatch(setData(response))
   })
 }
}

export const save = (data) =>{
   var service = Globals.api+'/posts/save';
   var form    = Form(data);
    return  dispatch => {
    return Request(service,form)
    .then(response =>{
      if (response.error) {
        // dispatch(setPost(response));
        dispatch(setAlert(false,null,null))
        dispatch(setAlert(true,'danger','Error saving offer, please try again'))
      } else {
        dispatch(setPost(response.data));
        dispatch(setAlert(false,null,null))
        dispatch(setAlert(true,'success','Offer has been successfully saved'))
      }
    })
    .catch(error => {
      dispatch(setPost({error:'Something went wrong, please try again'}))
      dispatch(setAlert(false,null,null))
      dispatch(setAlert(true,'danger','Something went wrong, please try again'))
    });
  }
}

export const saveFa = (data) =>{
  var service = Globals.api+'/postfa/save';
  var form    = Form(data);
   return  dispatch => {
   return Request(service,form)
   .then(response =>{
     if (response.error) {
       // dispatch(setPost(response));
       dispatch(setAlert(false,null,null))
       dispatch(setAlert(true,'danger','Error saving offer, please try again'))
     } else {
       dispatch(setPost(response.data));
       dispatch(setAlert(false,null,null))
       dispatch(setAlert(true,'success','Offer has been successfully saved'))
     }
   })
   .catch(error => {
     dispatch(setPost({error:'Something went wrong, please try again'}))
     dispatch(setAlert(false,null,null))
     dispatch(setAlert(true,'danger','Something went wrong, please try again'))
   });
 }
}

export const remove = (data) =>{
   var service = Globals.api+'/posts/remove';
   var form    = Form(data);
    return  dispatch => {
    // dispatch(requestRemove());
    return Request(service,form)
    .then(response =>{
        dispatch(loadPosts())
        window.bootbox.alert('Los datos fueron eliminados correctamente');
    })
  }
}

export const removeFa = (data) =>{
  var service = Globals.api+'/postfa/remove';
  var form    = Form(data);
   return  dispatch => {
   // dispatch(requestRemove());
   return Request(service,form)
   .then(response =>{
       dispatch(loadPosts())
       window.bootbox.alert('Los datos fueron eliminados correctamente');
   })
 }
}

export const getPost = (data) =>{
   var service = Globals.api+'/posts/get';
   var form    = Form(data);
    return  dispatch => {
    return Request(service,form)
    .then(response =>{
       dispatch(setPost(response.data))
       return response.data;
    })
  }
}

export const getPostFa = (data) =>{
  var service = Globals.api+'/postfa/get';
  var form    = Form(data);
   return  dispatch => {
   return Request(service,form)
   .then(response =>{
      dispatch(setPost(response.data))
      return response.data;
   })
 }
}

/*
* Tags
*
*------------------------------------------------------------*/

export const setPost = (post) =>{
return {
    type  :SET_POST,
    post  :post
  };
}

export const setTime = (value) =>{
  return {
    type  :ADD_POSTS_TIME,
    value  :value
  };
}

export const setTags = (value) =>{
return {
    type  :ADD_POSTS_TAGS,
    value  :value
  };
}

export const setCategories = (value) =>{
return {
    type  :ADD_POSTS_CATEGORIES,
    value  :value
  };
}

export const setHTMLContent = (value) =>{
return {
    type  : SET_POST_HTML_CONTENT,
    value :value
  };
}

export const setImageFeature = (value) =>{
return {
    type  : SET_POST_IMAGE_FEATURED,
    value :value
  };
}

export const removeImageFeature = (value) =>{
return {
    type  : REMOVE_POST_IMAGE_FEATURED
  };
}


export const setAlert = (status,mode='secondary', msg,icon) =>{
  return {
    type   : SET_ALERT,
    status : status,
    msg    : msg,
    icon   : icon,
    mode   : mode
  };
}



