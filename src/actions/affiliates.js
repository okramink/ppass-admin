import {Request,Form}  from '../utils';
import Globals         from '../globals';


export const  SET_DATA              = 'SET_DATA';
export const  SET_CURRENT           = 'SET_CURRENT';

export const  REQUEST_AFFILIATES    = 'REQUEST_AFFILIATES';
export const  SET_DATA_AFFILIATES   = 'SET_DATA_AFFILIATES';

export const  SHOW_POSTS              = 'SHOW_POSTS';

export const  ADD_AFFILIATES_TAGS         = 'ADD_AFFILIATES_TAGS';
export const  ADD_AFFILIATES_CATEGORIES   = 'ADD_AFFILIATES_CATEGORIES';

export const  SET_POST_HTML_CONTENT      = 'SET_POST_HTML_CONTENT';
export const  SET_POST_IMAGE_FEATURED     = 'SET_POST_IMAGE_FEATURED';
export const  REMOVE_POST_IMAGE_FEATURED  = 'REMOVE_POST_IMAGE_FEATURED';
export const  SET_DATA_PASSPORTS          = 'SET_DATA_PASSPORTS';

const load = () =>{
return {
    type: REQUEST_AFFILIATES,
    loader:true
  };
}

const showPosts = (data) =>{
  return {
      type  :SHOW_POSTS,
      loader:false,
      data  :data
    };
  }

const setCurrent = (data) =>{
return {
    type  :SET_CURRENT,
    loader:false,
    data  :data 
  };
}

const setDataAffilates = (data) =>{
return {
    type  :SET_DATA_AFFILIATES,
    loader:false,
    data  :data 
  };
}

const setPassportsData = (data) =>{
return {
    type  :SET_DATA_PASSPORTS,
    loader:false,
    data  :data 
  };
}

//--------------------------------------------------------------------
export const loadAffiliates = (data) =>{   
   var service = Globals.api+'/affiliates/table';
   var form    = Form(data);
    return  dispatch => {
    dispatch(load());
    return Request(service,form)
    .then(response =>{
        dispatch(setDataAffilates(response))
    })
  }
}
export const loadAffiliatesFa = (data) =>{   
  var service = Globals.api+'/affiliates/tableFa';
  var form    = Form(data);
   return  dispatch => {
   dispatch(load());
   return Request(service,form)
   .then(response =>{
       dispatch(setDataAffilates(response))
   })
 }
}
export const save = (data) =>{   
   var service = Globals.api+'/affiliates/save';
   var form    = Form(data);
    return  dispatch => {
    return Request(service,form)
    .then(response =>{
        dispatch(setCurrent(response.data))
        dispatch(loadAffiliates())
         window.Light.close();
    })
  }
}

export const saveAffiliatedUser = (data) =>{   
   var service = Globals.api+'/affiliates/save-membership';
   var form    = Form(data);
    return  dispatch => {
    return Request(service,form)
    .then(response =>{       
        dispatch(loadAffiliatesPassports({affiliated:data.affiliated}))
         window.Light.close();
    })
  }
}

export const saveAffiliatedUserFa = (data) =>{   
  var service = Globals.api+'/affiliates/save-membership-fa';
  var form    = Form(data);
   return  dispatch => {
   return Request(service,form)
   .then(response =>{
      console.log(response)
      if(response.status == 3){
        dispatch(setPassportsData(response))
        window.Light.close();
      } else {
        dispatch(loadAffiliatesPassports({affiliated:data.affiliated}))
        window.Light.close();
      }
   })
 }
}

export const saveAffiliatedUserFaActivated_edit = (data) =>{   
  var service = Globals.api+'/affiliates/save-membership-fa-activation-edit';
  var form    = Form(data);
   return  dispatch => {
   return Request(service,form)
   .then(response =>{       
     console.log(response)
       dispatch(setPassportsData(response))
        window.Light.close();
   })
 }
}

export const saveAffiliatedUserFaActivated = (data) =>{   
  var service = Globals.api+'/affiliates/save-membership-fa-activation';
  var form    = Form(data);
   return  dispatch => {
   return Request(service,form)
   .then(response =>{       
     console.log(response)
       dispatch(setPassportsData(response))
        window.Light.close();
   })
 }
}

export const saveAffiliatedUserFActivated = (data) =>{   
  console.log(data)
  var service = Globals.api+'/affiliates/save-membership-fActivation';
  var form    = Form(data);
   return  dispatch => {
   return Request(service,form)
   .then(response =>{       
     console.log(response)
       dispatch(setPassportsData(response))
        window.Light.close();
   })
 }
}

export const saveAffiliatedPostUser = (data) => {
  var service = Globals.api+'/affiliates/save-membershipPost';
   var form    = Form(data);
    return  dispatch => {
    return Request(service,form)
    .then(response =>{       
        dispatch(loadAffiliatesPassports({affiliated:data.affiliated}))
         window.Light.close();
    })
  }
}


export const loadAffiliatesPassports = (data) =>{   
   var service = Globals.api+'/affiliates/passports';
   var form    = Form(data);
    return  dispatch => {
    dispatch(load());
    return Request(service,form)
    .then(response =>{
        dispatch(setPassportsData(response))
    })
  }
}


export const getAffiliatesPassports = ( id ) => {
  let service = Globals.api+'/affiliates/posts';
  var form    = Form(id);
  return  dispatch => {
    return Request(service,form)
    .then(response =>{
      dispatch(showPosts(response))
      return response
    })
  }
} 

export const getAffiliatesPassportsLimited = ( id ) => {
  let service = Globals.api+'/affiliates/passportsLimited';
  var form    = Form(id);
  return  dispatch => {
    return Request(service,form)
    .then(response =>{
      console.log(response)
      dispatch(setPassportsData(response))
      return response
    })
  }
} 

export const saveAprovalPassport = ( pass ) => {
  let service = Globals.api+'/affiliates/save_aproval';
  var form    = Form(pass);
  return  dispatch => {
    return Request(service,form)
    .then(response =>{
      console.log(response)
      dispatch(setPassportsData(response))
      return response
    })
  }
}

export const saveAprovalPassportStatus = ( pass ) => {
  let service = Globals.api+'/affiliates/save_aprovalStatus';
  var form    = Form(pass);
  return  dispatch => {
    return Request(service,form)
    .then(response =>{
      console.log(response)
      dispatch(setPassportsData(response))
      return response
    })
  }
}

export const remove = (data) =>{   
   var service = Globals.api+'/affiliates/remove';
   var form    = Form(data);
    return  dispatch => {
    // dispatch(requestRemove());
    return Request(service,form)
    .then(response =>{
        dispatch(loadAffiliates())
        window.bootbox.alert('Los datos fueron eliminados correctamente');
    })
  }
}

export const getAffiliated = (data) =>{   
   var service = Globals.api+'/affiliates/get';
   var form    = Form(data);
    return  dispatch => {
    return Request(service,form)
    .then(response =>{                  
        dispatch(setCurrent(response.data))
       return response.data;
    })
  }
}


