import {Request,Form}  from '../utils';
import Globals         from '../globals';


export const  SET_POST         = 'SET_POST';
export const  REQUEST_PASSPORTS    = 'REQUEST_PASSPORTS';
export const  SET_DATA_PASSPORTS   = 'SET_DATA_PASSPORTS';
export const  SET_DATA_PASSPORTSMaster   = 'SET_DATA_PASSPORTSMaster';


export const  ADD_PASSPORTS_TAGS         = 'ADD_PASSPORTS_TAGS';
export const  ADD_PASSPORTS_CATEGORIES   = 'ADD_PASSPORTS_CATEGORIES';

export const  SET_POST_HTML_CONTENT      = 'SET_POST_HTML_CONTENT';
export const  SET_POST_IMAGE_FEATURED     = 'SET_POST_IMAGE_FEATURED';
export const  REMOVE_POST_IMAGE_FEATURED  = 'REMOVE_POST_IMAGE_FEATURED';


const load = () =>{
return {
    type: REQUEST_PASSPORTS,
    loader:true
  };
}

const setData = (data) =>{
return {
    type  :SET_DATA_PASSPORTS,
    loader:false,
    data  :data 
  };
}
const setDataMaster = (data) =>{
return {
    type  :SET_DATA_PASSPORTSMaster,
    loader:false,
    data  :data 
  };
}

//--------------------------------------------------------------------
export const loadPassports = (data) =>{   
  console.log(data)
   var service = Globals.api+'/passport/table';
   var form    = Form(data);
    return  dispatch => {
    dispatch(load());
    return Request(service,form)
    .then(response =>{
        dispatch(setData(response))
    })
  }
}
export const loadPassportsFa = (data) =>{   
  var service = Globals.api+'/passport/tableFa';
  var form    = Form(data);
   return  dispatch => {
   dispatch(load());
   return Request(service,form)
   .then(response =>{
       dispatch(setData(response))
   })
 }
}
export const loadPassportsMasters = (exist, code) =>{   
  var service = Globals.api+'/passport/tableMaster';
  var form    = Form({exist:exist, code: code});
  return  dispatch => {
    dispatch(load());
    return Request(service,form).then(response => {
      dispatch(setDataMaster(response))
      return response.data
    })
  }
}

export const save = (data) =>{   
   var service = Globals.api+'/passport/save';
   var form    = Form(data);
    return  dispatch => {
    return Request(service,form)
    .then(response =>{
      dispatch(setPost(response.data));      
    })
  }
}

export const saveRoomUpgrade = (data) => {
  var service = Globals.api+'/passport/saveRoomUpgrademanualy';
  var form    = Form(data);
  console.log(data)
  return  dispatch => {
    return Request(service,form)
    .then(response =>{
      console.log(response)
      dispatch(setData(response))
      window.Light.close();      
    })
  }
}

export const remove = (data) =>{   
   var service = Globals.api+'/passport/remove';
   var form    = Form(data);
    return  dispatch => {
    // dispatch(requestRemove());
    return Request(service,form)
    .then(response =>{
        dispatch(loadPassports())
        window.bootbox.alert('Los datos fueron eliminados correctamente');
    })
  }
}
 
export const returnRoomUpgrade = (data) =>{   
  var service = Globals.api+'/passport/returnRoomUpgrade';
  var form    = Form(data);
   return  dispatch => {
   // dispatch(requestRemove());
   return Request(service,form)
   .then(response =>{
       dispatch(loadPassports())
       window.bootbox.alert('Los datos fueron eliminados correctamente');
   })
 }
}

export const getPost = (data) =>{   
   var service = Globals.api+'/passport/get';
   var form    = Form(data);
    return  dispatch => {
    return Request(service,form)
    .then(response =>{            
       dispatch(setPost(response.data))
       return response.data;
    })
  }
}


/*
* Tags
*
*------------------------------------------------------------*/

export const setPost = (post) =>{
return {
    type  :SET_POST,
    post  :post 
  };
}

export const setTags = (value) =>{
return {
    type  :ADD_PASSPORTS_TAGS,
    value  :value 
  };
}

export const setCategories = (value) =>{
return {
    type  :ADD_PASSPORTS_CATEGORIES,
    value  :value 
  };
}

export const setHTMLContent = (value) =>{
return {
    type  : SET_POST_HTML_CONTENT,
    value :value 
  };
}

export const setImageFeature = (value) =>{
return {
    type  : SET_POST_IMAGE_FEATURED,
    value :value 
  };
}

export const removeImageFeature = (value) =>{
return {
    type  : REMOVE_POST_IMAGE_FEATURED
  };
}


