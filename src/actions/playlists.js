import {Request,Form}  from '../utils';
import Globals         from '../globals';


export const  SET_DATA_LIST           = 'SET_DATA_LIST';
export const  SET_TEMPORAL_LIST       = 'SET_TEMPORAL_LIST';
export const  REMOVE_TEMPORAL_LIST    = 'REMOVE_TEMPORAL_LIST';
export const  RESET_TEMPORAL_LIST     = 'RESET_TEMPORAL_LIST';
export const  REMOVE_ITEM_LIST        = 'REMOVE_ITEM_LIST';

export const  SET_COMPLET_LIST        = 'SET_COMPLET_LIST';

export const  SET_SEARCH_RESULT      = 'SET_SEARCH_RESULT';

export const  SET_SELECTED_VALUE      = 'SET_SELECTED_VALUE';



const setListData = (data) =>{
return {
    type  :SET_DATA_LIST,
    loader:false,
    data  :data 
  };
}


//--------------------------------------------------------------------


export const loadLists = (data) =>{
   var service = Globals.api+'/lists/table';
    var form    = Form(data);
    return  dispatch => {
    // dispatch(load());
    return Request(service,form)
    .then(response =>{   
      dispatch(setListData(response))
    })
  }
}


export const saveList = (data,cb) =>{   
   var service = Globals.api+'/lists/save';
   var form    = Form(data);
    return  dispatch => {
    // dispatch(loaderSave());
    return Request(service,form)
    .catch(e=>{
        window.bootbox.alert('Error from Server');
        return false;
    })
    .then(response =>{
        dispatch(loadLists());
        cb(response.data);
    })
  }
}


export const removeList = (data) =>{   
   var service = Globals.api+'/lists/remove';
   var form    = Form(data);
    return  dispatch =>{
    return Request(service,form)
    .then(response =>{
        dispatch(loadLists())
        window.bootbox.alert('Los datos fueron eliminados correctamente');
    })
  }
}

export const setTempList= (file,cb) =>{   
  return {
    type: SET_TEMPORAL_LIST,
    data:file
  };
}

export const resetList= (file,cb) =>{   
  return {
    type: RESET_TEMPORAL_LIST
  };
}


export const removeItemList= (idx) =>{   
  return {
    type: REMOVE_ITEM_LIST,
    idx:idx
  };
}

export const setListUpdate= (data) =>{   
  return {
    type: SET_COMPLET_LIST,
    data:data
  };
}


//---------------------------------------------



export const setSelectedValue = (value) =>{
  return {
    type   :SET_SELECTED_VALUE,
    value  :value
  };
}

export const setSearchResult = (data) =>{
  return {
    type   :SET_SEARCH_RESULT,
    loader :false,
    data   :data 
  };
}

export const search = (data) =>{    
    let service  = Globals.api+'/lists/search';
    let form     = Form(data);
    return  dispatch => {
    // dispatch(load());
    return Request(service,form)    
  }
}

