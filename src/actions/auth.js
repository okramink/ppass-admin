import {Request,Form}  from '../utils';
import Globals         from '../globals';


export const  REQUEST_TOKEN    = 'REQUEST_TOKEN';
export const  SET_TOKEN        = 'SET_TOKEN';

export const  REQUEST_SESSION = 'REQUEST_SESSION';
export const  SET_SESSION     = 'SET_SESSION';


const load = () =>{
return {
    type: REQUEST_SESSION,
    loader:true
  };
}



//--------------------------------------------------------------------

export const start = (data) =>{
   var form    = Form(data);
   var service = Globals.api+'/auth';
     return  dispatch => { 
        dispatch(load());
        return Request(service,form)
  }
}

export const set = (user) =>{
   return {
    type   :'SET_SESSION',
    loader :false,
    user   :user
  };
}

export const getToken = (token) =>{
   var form    = Form(token);
   var service = Globals.api+'/auth/token';
    return  dispatch => {
    dispatch(load());
    return Request(service,form)
  }
}