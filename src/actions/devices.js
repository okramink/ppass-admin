import {Request,Form}  from '../utils';
import Globals         from '../globals';

export const  REQUEST_DEVICES          = 'REQUEST_DEVICES';
export const  SET_DATA_DEVICES         = 'SET_DATA_DEVICES';

export const  HIDE_SEARCH_DEVICES      = 'HIDE_SEARCH_DEVICES';
export const  REQUEST_SEARCH_DEVICES   = 'REQUEST_SEARCH_DEVICES';
export const  SET_SEARCH_DATA_DEVICES  = 'SET_SEARCH_DATA_DEVICES';

export const  SET_DATA_DEVICE          = 'SET_DATA_DEVICE';
export const  REQUEST_SAVE_DEVICE      = 'REQUEST_SAVE_DEVICE';

export const  SHOW_DEVICE_FORM         = 'SHOW_DEVICE_FORM';

export const  SET_UPDATE  = 'SET_UPDATE';


const load = () =>{
return {
    type: REQUEST_DEVICES,
    loader:true
  };
}
const setData = (data) =>{
return {
    type  :SET_DATA_DEVICES,
    loader:false,
    data  :data 
  };
}

const loaderSave = () =>{
return {
    type: REQUEST_SAVE_DEVICE,
    loader:true
  };
}
export const setCustomerData = (data) =>{
return {
    type  :SET_DATA_DEVICE,
    loader:false,
    data  :data
  };
}

//--------------------------------------------------------------------

export const loadDevices = (data) =>{
    var service = Globals.api+'/devices/table';
    var form    = Form(data);
    return  dispatch => {
    dispatch(load());
    return Request(service,form)
    .then(response =>{   
      dispatch(setData(response))
    })
  }
}



export const save = (data,cb) =>{   
   var service = Globals.api+'/devices/save';
   var form    = Form(data);
    return  dispatch => {
    dispatch(loaderSave());
    return Request(service,form)
    .catch(e=>{
        window.bootbox.alert('Error from Server');
        return false;
    })
    .then(response =>{
        dispatch(loadDevices());
        cb(response.data);
    })
  }
}

export const remove = (data) =>{   
   var service = Globals.api+'/devices/remove';
   var form    = Form(data);
    return  dispatch =>{
    return Request(service,form)
    .then(response =>{
        // dispatch(loadCustomers())
        window.bootbox.alert('Los datos fueron eliminados correctamente');
    })
  }
}

export const showCardForm = (s) =>{
return {
    type  :SHOW_DEVICE_FORM,
    status:s 
  };
}




//---------------------------------------------

// const setUpdate = (data) =>{
//   return {
//     type   :SET_UPDATE,
//     loader :false,
//     data   :data 
//   };
// }


export const update = (data,cb) =>{   
   var service = Globals.api+'/devices/update';
   var form    = Form(data);
    return  dispatch => {
    dispatch(loaderSave());
    return Request(service,form)
    .catch(e=>{
        window.bootbox.alert('Error from Server');
        return false;
    })
    .then(response =>{ 
      dispatch(loadDevices());
      cb(response.data);
    })
  }
}