import {Request,Form}  from '../utils';
import Globals         from '../globals';

export const  REQUEST_FILES          = 'REQUEST_FILES';
export const  SET_DATA_FILES         = 'SET_DATA_FILES';

export const  HIDE_SEARCH_FILES      = 'HIDE_SEARCH_FILES';
export const  REQUEST_SEARCH_FILES   = 'REQUEST_SEARCH_FILES';
export const  SET_SEARCH_DATA_FILES  = 'SET_SEARCH_DATA_FILES';

export const  SET_DATA_FILE          = 'SET_DATA_FILE';
export const  REQUEST_SAVE_FILE      = 'REQUEST_SAVE_FILE';

export const  SHOW_FILE_FORM         = 'SHOW_FILE_FORM';


export const  SET_TEMPORAL_FILE               = 'SET_TEMPORAL_FILE';
export const  SET_TEMPORAL_FILE_PROGRESS      = 'SET_TEMPORAL_FILE_PROGRESS';
export const  SET_TEMPORAL_FILE_UPLOADED      = 'SET_TEMPORAL_FILE_UPLOADED';
export const  REMOVE_TEMPORAL_FILE            = 'REMOVE_TEMPORAL_FILE';


export const  SET_DATA_LIST           = 'SET_DATA_LIST';
export const  SET_TEMPORAL_LIST       = 'SET_TEMPORAL_LIST';
export const  REMOVE_TEMPORAL_LIST    = 'REMOVE_TEMPORAL_LIST';
export const  RESET_TEMPORAL_LIST     = 'RESET_TEMPORAL_LIST';
export const  REMOVE_ITEM_LIST        = 'REMOVE_ITEM_LIST';

export const  SET_COMPLET_LIST        = 'SET_COMPLET_LIST';



const load = () =>{
return {
    type: REQUEST_FILES,
    loader:true
  };
}
const setData = (data) =>{
return {
    type  :SET_DATA_FILES,
    loader:false,
    data  :data 
  };
}


const setListData = (data) =>{
return {
    type  :SET_DATA_LIST,
    loader:false,
    data  :data 
  };
}

const loaderSave = () =>{
return {
    type: REQUEST_SAVE_FILE,
    loader:true
  };
}
export const setCustomerData = (data) =>{
return {
    type  :SET_DATA_FILE,
    loader:false,
    data  :data
  };
}

//--------------------------------------------------------------------

export const loadFiles = (data) =>{
   var service = Globals.api+'/files/table';
    var form    = Form(data);
    return  dispatch => {
    dispatch(load());
    return Request(service,form)
    .then(response =>{   
      dispatch(setData(response))
    })
  }
}



export const save = (data,cb) =>{   
   var service = Globals.api+'/files/save';
   var form    = Form(data);
    return  dispatch => {
    dispatch(loaderSave());
    return Request(service,form)
    .catch(e=>{
        window.bootbox.alert('Error from Server');
        return false;
    })
    .then(response =>{
        dispatch(loadFiles());
        cb(response.data);
    })
  }
}

export const remove = (data) =>{   
   var service = Globals.api+'/files/remove';
   var form    = Form(data);
    return  dispatch =>{
    return Request(service,form)
    .then(response =>{
        dispatch(loadFiles())
        window.bootbox.alert('Los datos fueron eliminados correctamente');
    })
  }
}

export const showCardForm = (s) =>{
return {
    type  :SHOW_FILE_FORM,
    status:s 
  };
}

export const setTemporal = (file,cb) =>{   
  return {
    type: SET_TEMPORAL_FILE,
    data:file
  };
}

export const setTemporalProgress = (idx,percent) =>{   
  return {
    type: SET_TEMPORAL_FILE_PROGRESS,
    idx:idx,
    percent:percent,
    uploaded:false
  };
}
export const setTemporalUploaded = (idx) =>{   
  return {
    type: SET_TEMPORAL_FILE_UPLOADED,
    idx:idx
  };
}

export const removeTemporal = (idx) =>{   
  return {
    type: REMOVE_TEMPORAL_FILE,
    idx:idx
  };
}


export const loadLists = (data) =>{
   var service = Globals.api+'/lists/table';
    var form    = Form(data);
    return  dispatch => {
    dispatch(load());
    return Request(service,form)
    .then(response =>{   
      dispatch(setListData(response))
    })
  }
}


export const saveList = (data,cb) =>{   
   var service = Globals.api+'/lists/save';
   var form    = Form(data);
    return  dispatch => {
    dispatch(loaderSave());
    return Request(service,form)
    .catch(e=>{
        window.bootbox.alert('Error from Server');
        return false;
    })
    .then(response =>{
        dispatch(loadLists());
        cb(response.data);
    })
  }
}


export const removeList = (data) =>{   
   var service = Globals.api+'/lists/remove';
   var form    = Form(data);
    return  dispatch =>{
    return Request(service,form)
    .then(response =>{
        dispatch(loadLists())
        window.bootbox.alert('Los datos fueron eliminados correctamente');
    })
  }
}

export const setTempList= (file,cb) =>{   
  return {
    type: SET_TEMPORAL_LIST,
    data:file
  };
}

export const resetList= (file,cb) =>{   
  return {
    type: RESET_TEMPORAL_LIST
  };
}


export const removeItemList= (idx) =>{   
  return {
    type: REMOVE_ITEM_LIST,
    idx:idx
  };
}

export const setListUpdate= (data) =>{   
  return {
    type: SET_COMPLET_LIST,
    data:data
  };
}


