import {Request,Form}  from '../utils';
import Globals         from '../globals';


export const  REQUEST_BOOKINGS    = 'REQUEST_BOOKINGS';

const load = (data) =>{
  console.log(data)
  return {
    type: REQUEST_BOOKINGS,
    loader: true,
    data  : data
  };
}

//--------------------------------------------------------------------
export const loadBookings = (data) =>{   
    var service = Globals.api+'/bookings/bookAll';
    var form    = Form(data);
    console.log(data)
     return  dispatch => {
     
     return Request(service,form)
     .then(response =>{
        dispatch(load(response));
         console.log(response)
     })
   }
 }
 export const findBookings = (data) => {
  var service = Globals.api+'/bookings/findBookingsFromPassport';
  var form    = Form(data);
  console.log(data)
  return dispatch => {
    return Request(service,form)
    .then(response =>{
      dispatch(load(response));
        console.log(response)
        return response
    })
  }
 }
 export const cancel = (data) =>{   
    var service = Globals.api+'/bookings/cancelBook';
    var form    = Form(data);
    console.log(data)
    return  dispatch => {
      return Request(service,form)
      .then(response =>{
        dispatch(load(response));
         console.log(response)
      })
   }
 }
 export const bookReservation = (data) =>{   
    var service = Globals.api+'/bookings/bookReservation';
    var form    = Form(data);
    console.log(data)
    return  dispatch => {
    
    return Request(service,form)
      .then(response =>{
          dispatch(load(response));
          window.Light.close();
      })
    }
  }
  export const bookConfirmation = (data) =>{   
    var service = Globals.api+'/bookings/bookConfirmation';
    var form    = Form(data);
    console.log(data)
    return  dispatch => {
    
    return Request(service,form)
      .then(response =>{
          dispatch(load(response));
          window.Light.close();
      })
    }
  }
 export const print = (data) =>{   
  var service = Globals.api+'/bookings/printBook';
  var form    = Form(data);
  console.log(data)
   return  dispatch => {
   
   return Request(service,form)
   .then(response =>{
      dispatch(load(response));
       console.log(response)
   })
 }
}

export const printConfirmation = (data) =>{   
  var service = Globals.api+'/bookings/printBookConfirmation';
  var form    = Form(data);
  console.log(data)
   return  dispatch => {
   
   return Request(service,form)
   .then(response =>{
      dispatch(load(response));
       console.log(response)
   })
 }
}

export const saveAprovalBooking = (data) => {
  var service = Globals.api+'/bookings/aprovalBookingPrinted';
  var form    = Form(data);
  return  dispatch => {
   
  return Request(service,form)
    .then(response =>{
        dispatch(load(response));
    })
  }
}

export const savebook = (data) =>{   
  var service = Globals.api+'/bookings/booknote';
  var form    = Form(data);
   return  dispatch => {
   
   return Request(service,form)
   .then(response =>{
      dispatch(load(response));
      window.Light.close();
   })
 }
}

export const savebooking = (data) => {  
  var service = Globals.api+'/bookings/createFa';
  //console.log(data)
  const form = Form(data);  
  return  dispatch => {  
      return Request(service,form)
      .then(response =>{
        console.log(response)
        return response
        dispatch(load(response));
    })
  }
}
