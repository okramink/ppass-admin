import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import {I18n,Translate} from 'react-redux-i18n';
import Table     from '../components/Table';

import Creator from '../components/playlists/Creator';

//Actions
import {
  loadLists,
  removeList
} from '../actions/files';


class Playlists extends React.Component {
   static get contextTypes() {
    return {
        router: PropTypes.object.isRequired,
        store: PropTypes.object.isRequired
    }
  }
  constructor(props,context){
    super(props,context);
    this.context = context;
    this.store   = this.context.store;
    this.state   = this.store.getState();

    this.headers = [        
        {db:'id',label:I18n.t('ID'), width:'30px', align:'left', badge:'success', onclick: (o) => {
            window.Light = window.YagamiBox.init({
                title: I18n.t('Update list'),
                width:'80%',
		        height:'80%',
		        content:"100%",   
                setDomContent : (container) => {                    
                  ReactDOM.render(<Creator data={o} context={this.context} modal={true}  />, container);
                }
            })
         }},        
        {db:"title",label:I18n.t('Title'),width:'160px', type:'text' , align:'left'},
        {db:"id",label:I18n.t('media'),width:'30', type:'text' , align:'center', badge:'primary', value:(o,i)=> {
        	return o.content.length
        }},
        {db:'createdAt',label:I18n.t('Created'), icon:'ion-calendar', badge:'link',type:'datetime',width:'100px', align:'center'},
        {db:'updatedAt',label:' ', icon:'ion-backspace-outline', badge:'danger-link',width:'20px', align:'center',
        value:(o)=>{
            return ' Eliminar';
        },
        onclick:(o)=>{
            window.bootbox.confirm(I18n.t('Are you sure to delete this list?') ,(result)=>{
	            if(result){
	                this.store.dispatch(removeList({id:o.id}));
	            }
            })
        }}
    ]
  }
  componentWillUnmount() {
    this.unsuscribe();
  }
  componentDidMount(){
     this.unsuscribe = this.store.subscribe(() => {
        this.setState(this.store.getState());
     });
  }
  addList(){
  	window.Light = window.YagamiBox.init({
        title: I18n.t('Create list'),
        width:'80%',
        height:'80%',
        content:"100%",        
        setDomContent : (container) => {
            ReactDOM.render(<Creator context={this.context} modal={true} />, container);
        }
    })
  }
   request(data){
    this.store.dispatch(loadLists(data));
  }
  render() {
    return (
        <div className="page container">
        <div className="row">
          <div className="col-md-12">
            <button ref="addFile" onClick={this.addList.bind(this) } className="btn btn-sm btn-success float-right"> <i className="ion-plus-circled" /> <Translate value="Create"/> </button>
            <h1><i className="ion-navicon-round"/> <Translate value="Playlists"/></h1>
           <hr className="divider" />
            
          </div>
        </div>
        <div className="row">
          <div className="col-md-12">
            <Table data={this.state.files.listTable} source={this.request.bind(this)} headers={this.headers} page={1} rows={20} placeholder={I18n.t('Search Lists')} search={true} />
          </div>
          
        </div>
      </div>
    )
  }
}
export default Playlists;

