import React, {Component}     from 'react';

import ReactQuill           from 'react-quill'; // ES6
import 'react-quill/dist/quill.snow.css';
import 'react-quill/dist/quill.bubble.css';

class Notification extends Component {
    constructor(props){
        super(props)

        this.modules = {
            toolbar: [ 
                ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
                ['blockquote', 'code-block'],
            
                [{ 'header': 1 }, { 'header': 2 }],               // custom button values
                [{ 'list': 'ordered'}, { 'list': 'bullet' }],
                [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
                [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
                [{ 'direction': 'rtl' }],                         // text direction
            
                [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
                [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
            
                [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
                [{ 'font': [] }],
                [{ 'align': [] }],
            
                ['clean']
            ]
        };

		this.formats = [
		    'font',
		    'size',
		    'bold', 'italic', 'underline',
		    'list', 'bullet',
		    'align',
		    'color', 'background'
	  	];

	  	this.state = {
			comments: ''
		}

		this.rteChange = this.rteChange.bind(this);
    }

    rteChange = (content, delta, source, editor) => {
		console.log(editor.getHTML()); // rich text
		console.log(editor.getText()); // plain text
        console.log(editor.getLength()); // number of characters
	}

    render(){
        return (
            <div className="page container">
                <div className="row">
                    <div className="col-md-12">
                        <div className="row">
                            <div className="col-sm-12">
                            <div className="form-group form-group-quill-container" >
                                <ReactQuill theme="snow"   
                                    modules={this.modules}
                                    formats={this.formats} 
                                    onChange={this.rteChange}
			                        value={this.state.comments || ''}
                                />
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Notification;
