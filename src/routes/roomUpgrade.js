import React    from 'react';
import PropTypes from 'prop-types';
import {I18n, Translate} from 'react-redux-i18n';
import moment from 'moment';
import { connect } from 'react-redux';

import ReactDOM  from 'react-dom';
import AddForm from '../components/roomupgrade/BookingList';
import RoomUpgrade from '../components/roomupgrade/Roomupgrade'

//Actions
import {
  loadPassports,
  returnRoomUpgrade
} from '../actions/passports';

import{
findBookings
} from '../actions/bookings';

//Components
import Table    from '../components/Table';

//Users routes
class roomUpgrade extends React.Component {

  static get contextTypes() {
    return {
        router: PropTypes.object.isRequired,
        store: PropTypes.object.isRequired,
    }
  }
  constructor(props,context){
    super(props,context);
    this.context = context;
    this.store   = this.context.store;
    this.state   = this.store.getState();

    this.creditResort = this.creditResort.bind(this)

    this.headers = [
        {db:'icontrol',label:I18n.t('#Control'), icon:'ion-edit', width:'150px',badge:'link', onclick: (o) => {
            // window.location.href = '/passports/update/'+o.id;
        }},
        {db:'room',label:I18n.t('#Room'),type:'text',width:'100px', align:'center', icon:'ion-calendar', badge:'primary', value:(o)=>{
            //console.log(o)
            return o.room ? o.room : 'Not Found'
        }},
        {db:"name",label:I18n.t('Name / Email'), type:'text' , align:'left', width:'150px', value:(o,i)=>{
          let name = o.lastname ? o.name  +' '+ o.lastname : o.name
          return `${name} - ${o.email}`;
        }},    
        {db:'days',label:I18n.t('#Days'),type:'text',width:'100px', align:'center', icon:'ion-calendar', badge:'primary',value:(o)=>{
          //console.log(o)
          return o.nightsUpgrate ? Number(o.nightsUpgrate) : '0'
        }},
        {db:'initial_credit',label:I18n.t('Credit Type'),type:'money',width:'100px', align:'center', icon:'ion-calendar', badge:'primary', value:(o)=>{
          return o.initial_credit ? o.initial_credit : o.credit
        }},
        {db:'credit',label:I18n.t('Remaining'),type:'money',width:'100px', align:'center', icon:'ion-calendar', badge:'primary', value:(o)=>{
          return o.credit ? o.credit : o.initial_credit
        }},
        {db:"expiration",label:I18n.t('Expiration'),type:'date',align:'center',width:'100px', icon:'ion-calendar', badge:'primary',
        value:(o) => {
          //console.log(o)
          return o.expiration ? o.status == 0 ? 'Pending to be activated' : moment() > moment(o.expiration) ? 'Expired' : 'Active' : 'Pending to be activated'
        } },
        {db:'roomupgrade',label:I18n.t('Credit Activation'),type:'text',width:'100px', align:'center', icon:'ion-calendar', badge:'primary', value:(o)=>{
            //console.log(o)
            return o.status == 1 ? 'Activated' : 'Pending'
        }},
        {db:'roomupgrade',label:I18n.t('Room Upgrade'),type:'text',width:'100px', align:'center', icon:'ion-calendar', badge:'primary', value:(o)=>{
          //console.log(o)
          return o.status == 1 ? o.roomupgrade == 'true' ? 'available' : o.nightsUpgrate == undefined ? 'unavailable' : o.nightsUpgrate == 0 ? 'rejected' : o.cancelRoomUpgrate ? 'Canceled' : 'available' : 'pending'
        },
        onclick:(o)=>{
          console.log(o)
          if(o.status == 1 && o.roomupgrade != 'true' && o.cancelRoomUpgrate != true && o.nightsUpgrate && moment() < moment(o.expiration)){
            window.bootbox.confirm(I18n.t('Do you want to cancel this Room Upgrade'), (result)=>{
            if(result){
                let data = {id: o.id, nights: Number(o.nightsUpgrate)}
                this.store.dispatch(returnRoomUpgrade(data));
            }
            })
          }else if(o.status == 1 && o.roomupgrade == 'true'){
            window.Light = window.YagamiBox.init({
              title: I18n.t('Room Upgrade - Activation'),
              width:'600px',
              height:'480px',
              setDomContent : (container) => {
                  ReactDOM.render(<RoomUpgrade data={o} store={this.store} modal={true} />, container);
              }
            })
          }else if(o.status == 1 && o.cancelRoomUpgrate == true){
            window.Light = window.YagamiBox.init({
              title: I18n.t('Room Upgrade - Activation'),
              width:'600px',
              height:'480px',
              setDomContent : (container) => {
                  ReactDOM.render(<RoomUpgrade data={o} store={this.store} modal={true} />, container);
              }
            })
          }
        }},
        {db:'bookings', label:I18n.t('Booking List'), type:'text',width:'100px', align:'center', icon:'ion-calendar', badge:'primary',
        value:(o)=>{
          //console.log(o)
          return 'Booking List'
        },onclick: (o)=>{
          //console.log(o)
          let data = {email:o.email}
          this.store.dispatch(findBookings(data))
          //console.log(this.props)
          window.Light = window.YagamiBox.init({
            title: I18n.t('Bookings'),
            width:'85%',
            height:'520px',
            setDomContent : (container) => {
                ReactDOM.render(<AddForm data={this.props.bookings} store={this.store} passports={o} modal={true} />, container);
            }
          })
        }
      }
    ]

  }
  componentWillUnmount() {
    this.unsuscribe();
  }
  componentDidMount(){
     this.unsuscribe = this.store.subscribe(() => {
        this.setState(this.store.getState());
     });
  }
  request(params){
    params.roomUpgrade = true
    params.code = 'cbcfa'
    params.credit = 'all'
    this.store.dispatch(loadPassports(params));
    
    this.setState({credit: 3})
  }
  addForm(){
     window.location.href = '/posts/add'
  }
  creditResort(data){
    let params={}
    console.log(data.target.value)
    this.setState({credit: data.target.value})
    if(data.target.value == 1){
      params.credit = '500'
    }else if(data.target.value == 2){
      params.credit = '1000'
    }else if(data.target.value == 3){
      params.credit = 'all'
    }
    params.code = 'cbcfa'
    params.roomUpgrade = true
    this.store.dispatch(loadPassports(params));
  }
  render() {
    console.log(this.state.passports)
    return (
        <div className="page container">
        <div className="row">
          <div className="col-md-12">
              <button ref="adduser" onClick={this.addForm.bind(this) } className="btn btn-sm btn-success float-right sr-only"> <i className="ion-plus-circled" /> <Translate value="Add"/> </button>
            <h1><i className="ion-compose"/> <Translate value="Room Upgrade"/></h1>
            <hr className="divider" />
          </div>
        </div>
        <div className="row">
          <div className="col-md-12">
            <h3>Filter:</h3>
            <div className="col-md-6">
              <div class="custom-control custom-radio">
                <input type="radio" class="custom-control-input" id="500-Credit-Resort" name="credit-resort" value={1} checked={this.state.credit == 1 ? true : false } onChange={this.creditResort}/>
                <label class="custom-control-label" for="500-Credit-Resort"> $500 Credit Resort</label>
              </div>
              <div class="custom-control custom-radio">
                <input type="radio" class="custom-control-input" id="1000-Credit-Resort" name="credit-resort" value={2} checked={this.state.credit == 2 ? true : false } onChange={this.creditResort}/>
                <label class="custom-control-label" for="1000-Credit-Resort"> $1000 Credit Resort</label>
              </div>
              <div class="custom-control custom-radio">
                <input type="radio" class="custom-control-input" id="All-Credit-Resort" name="credit-resort" value={3} checked={this.state.credit == 3 ? true : false } onChange={this.creditResort}/>
                <label class="custom-control-label" for="All-Credit-Resort"> All Credit Resort</label>
              </div>
            </div>
            <br/><br/>
          </div>
        </div>
        <div className="row">
          <div className="col-md-12">
            <Table 
              data={this.state.passports.data} 
              responsive={true} 
              source={this.request.bind(this)} 
              headers={this.headers} 
              page={1} 
              rows={10} 
              placeholder={I18n.t('Search posts')} 
              search={true} 
            />
          </div>
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  console.log(state)
  return {
      bookings: state.bookings.data,
  }
}
export default connect(mapStateToProps, null)(roomUpgrade);
