import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import {I18n, Translate} from 'react-redux-i18n';

//Actions
import {
  loadUsersFA,
  loadAdmins,
  removeFa
} from '../actions/users';

//Components
import Table    from '../components/Table';
import AddForm from '../components/users/AddFormFA';

class Settings extends React.Component {
  static get contextTypes() {
    return {
        router: PropTypes.object.isRequired,
        store: PropTypes.object.isRequired
    }
  }
  constructor(props,context){
    super(props,context);
    this.context = context;
    this.store   = this.context.store;
    this.state   = this.store.getState();

    this.headers = [       
        {db:'name',label:I18n.t('Name'), icon:'ion-ios-body-outline', onclick: (o) => {
                window.Light = window.YagamiBox.init({
                title: I18n.t('Update user'),
                width:'70%',
                height:'600px',
                setDomContent : (container) => {
                    ReactDOM.render(<AddForm data={o} context={this.context} modal={true} />, container);
                }
            })
         }
        },
        {db:"email",label:I18n.t('Email'),width:'160px', type:'text' , align:'right'},
        {db:"role",label:I18n.t('Role'),width:'70px', badge:{default:'primary',root:'danger',operator:'success'}, type:'text' , align:'right',
        value:function(o){
            return o.role ? o.role.toUpperCase() : 'USER';
        }},
        {db:"thestatus",label:I18n.t('Status'),width:'100px',type:'text', icon:'ion-flag',badge:{default:'default',activo:'success',inactivo:'danger'}, align:'right',
        value:(o)=>{
            return o.thestatus.toUpperCase();
        }},
        {db:'updatedAt',label:I18n.t('Updated'), icon:'ion-calendar', badge:'link',type:'datetime',width:'150px', align:'center'},
        {db:'createdAt',label:' ', icon:'ion-backspace-outline', badge:'danger-link',width:'20px', align:'center',
        value:(o)=>{
            return 'Eliminar';
        },
        onclick:(o)=>{
            window.bootbox.confirm(I18n.t('Are you sure to delete this user?') ,(result)=>{
            if(result){
              console.log(o)
                this.store.dispatch(removeFa({id:o.id}));
            }
            })
        }}
    ]
  }
  componentWillUnmount() {
    this.unsuscribe();
  }
  componentDidMount(){
     this.unsuscribe = this.store.subscribe(() => {
        this.setState(this.store.getState());
     });
  }
  requestUsers(params){
    this.store.dispatch(loadUsersFA(params));
  }
  requestAdmins(params){
    this.store.dispatch(loadAdmins(params));
  }
  addForm(){
    window.Light = window.YagamiBox.init({
        title: I18n.t('Add user') ,
        width:'70%',
        height:'600px',
        setDomContent : function(container){
            ReactDOM.render(<AddForm  context={this.context} modal={true} />, container);
        }.bind(this)
    })
  }
  render() {
    return (
      <div className="page container"> 
        <hr className="divider" />  
        <div className="row">
          <div className="col-md-12">
              <button ref="adduser" onClick={this.addForm.bind(this) } className="btn btn-sm btn-success float-right"> <i className="ion-plus-circled" /> <Translate value="Add"/> </button>
            <h1><i className="ion-person-stalker"/> <Translate value="Dashboard FA"/></h1>
            <hr className="divider" />
          </div>
        </div>
        <hr className="divider" />
         <div className="row">
          <div className="col-md-12">
            <h5><Translate value="Users"/></h5>            
            <Table data={this.state.users.data} source={this.requestUsers.bind(this)} headers={this.headers} page={1} rows={10} placeholder={I18n.t('Search users')} search={true} />
          </div>
        </div>
      </div>
    )
  }
}
export default Settings;

