import React    from 'react';
import PropTypes from 'prop-types';
import {I18n, Translate} from 'react-redux-i18n';


//Actions
import {
  loadPassports, 
  remove
} from '../actions/passports';

//Components
import Table    from '../components/Table';

//Users routes
class Passports extends React.Component {

  static get contextTypes() {
    return {
        router: PropTypes.object.isRequired,
        store: PropTypes.object.isRequired
    }
  }
  constructor(props,context){
    super(props,context);
    this.context = context;
    this.store   = this.context.store;
    this.state   = this.store.getState();

     this.headers = [
        {db:'icontrol',label:I18n.t('#Control'), icon:'ion-edit', width:'150px',badge:'link', value:(o,i)=>{
          return o.icontrol ? o.icontrol : 'N/A';
        }},
        {db:"name",label:I18n.t('Name'), type:'text' , align:'left', width:'150px', value:(o,i)=>{
          return `${o.name} - ${o.email}`;
        }},    
        {db:"affiliated",label:I18n.t('Refer'), badge:{default:'default',null:''}, type:'text' , align:'center', width:'50px', value:(o,i)=>{
           return o.affiliated ? o.affiliated.code.toUpperCase() : 'PPASS';
        }},            
        {db:"status",label:I18n.t('Payment'),type:'text' , align:'center',width:'50px', badge:{default:'default',completed:'success',pending:'danger'}, value:(o,i)=>{
            return o.status === 1 ? 'Completed' : o.status === 3 ? 'pre Register' : 'Pending';
        }},            
        {db:"notify",label:I18n.t('Notified'),type:'text' , align:'center',width:'50px', badge:{default:'default',done:'success',pending:'danger'}, value:(o,i)=>{
            return o.notify === 1 ? 'Done' : 'Pending';
        }},        
        {db:"voucher",label:I18n.t('Voucher'),type:'text' , align:'center',width:'50px', badge:'primary', value:(o,i)=>{
          return o.voucher ? o.voucher : 'NO';
        }},
        {db:"typecredit",label:I18n.t('Type Credit'),type:'money' , align:'center',width:'100px', icon:'ion-calendar', badge:'primary'},
        // {db:"expiration",label:I18n.t('Expiration'),type:'date',align:'center',width:'100px', icon:'ion-calendar', badge:'primary' },
        {db:'createdAt',label:I18n.t('Created at'),type:'datetime',width:'100px', align:'center', icon:'ion-calendar', badge:'primary'},
    ]

  }
  componentWillUnmount() {
    this.unsuscribe();
  }
  componentDidMount(){
     this.unsuscribe = this.store.subscribe(() => {
        this.setState(this.store.getState());
     });
  }
  request(params){
    this.store.dispatch(loadPassports(params));
  }
  addForm(){
     window.location.href = '/posts/add'
  }
  render() {
    return (
      <div className="page container">
        <div className="row">
          <div className="col-md-12">
              <button ref="adduser" onClick={this.addForm.bind(this) } className="btn btn-sm btn-success float-right"> <i className="ion-plus-circled" /> <Translate value="Add"/> </button>
            <h1><i className="ion-compose"/> <Translate value="Passports"/></h1>
            <hr className="divider" />
          </div>
        </div>
        <div className="row">
          <div className="col-md-12">
            <Table data={this.state.passports.data} source={this.request.bind(this)} headers={this.headers} page={1} rows={10} placeholder={I18n.t('Search posts')} search={true} />
          </div>
        </div>
      </div>
    )
  }
}

export default Passports;
