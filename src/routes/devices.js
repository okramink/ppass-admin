import React      from 'react';
import PropTypes  from 'prop-types';
import ReactDOM   from 'react-dom';
import Table      from '../components/Table';
import DeviceForm from '../components/devices/Form';

import {I18n, Translate} from 'react-redux-i18n';

import {
  loadDevices
} from '../actions/devices';


class Devices extends React.Component {
  static get contextTypes() {
    return {
      router: PropTypes.object.isRequired,
      store: PropTypes.object.isRequired,
      socket: PropTypes.object.isRequired
    }
  }
  constructor(props,context){
    super(props,context);
    this.context   = context;
    this.store     = this.context.store;
    this.state     = this.store.getState();
    this.socket    = this.context.socket;
    
      this.DeviceHeaders = [        
        {db:'serial',label:I18n.t('Serial'), badge:'link', icon:'ion-edit', width:'100px', onclick: (o) => {                
                // this.socket.emit('device:hdd',o.id);
                window.Light = window.YagamiBox.init({
                title: I18n.t('Update device'),
                width:'70%',
                height:'600px',
                setDomContent : (container) => {
                    ReactDOM.render(<DeviceForm data={o} context={this.context} socket={this.socket} modal={true} />, container);
                }
              })
         }},
        {db:'location',label:I18n.t('Label'), icon:'ion-ios-location-outline', width:'100px',value:(o,i)=>{
          return  o.label;
        }},      
        {db:'updatedAt',label: I18n.t('Last Seen'), icon:'ion-calendar', badge:'link',type:'datetime',width:'150px', align:'center'},
        {db:'createdAt',label: I18n.t('Registerd'),icon:'ion-calendar', badge:'link', type:'datetime', width:'20px', align:'center'},
        {db:'status',label:I18n.t('Status'), width:'20px', align:'center',badge:{default:'default','active':'success','inactive':'danger'},value:(o,i)=>{
            return  o.status === 1 ? 'Active' : 'Inactive'
        }}
    ]

  }
  
  componentWillUnmount() {
    this.unsuscribe();
  }
  componentDidMount(){
    this.unsuscribe = this.store.subscribe(() => {
      this.setState(this.store.getState());
    });
    this.socket.on('device:registered',(data)=>{
      // console.log(this.paramsTable)
       this.requestDevices(this.paramsTable)
    });
  }
  addDevice(){
    window.Light = window.YagamiBox.init({
        title  : I18n.t('Add device'),
        width  :'60%',
        height :'650px',
      setDomContent : (container) => {
          ReactDOM.render(<DeviceForm context={this.context} modal={true} />, container);
      }
    })
  }
   requestDevices(params){
    this.paramsTable = params;
    this.store.dispatch(loadDevices(params));
  }
  render() {
    return (
    	<div className="page container">
     	<div className="float-right menu-btns hide">
	        <button className="btn btn-sm btn-outline-success" onClick={this.addDevice.bind(this)}><i className="ion-ios-plus-outline" /> <Translate value="Add device"/> </button>
	    </div>
        <h1><i className="ion-ios-location-outline"/> <Translate value="Devices"/></h1>  
        <hr className="divider" />
        <div className="row">
          <div className="col-md-12">
            <Table data={this.state.devices.table} source={this.requestDevices.bind(this)} headers={this.DeviceHeaders} page={1} rows={10} placeholder={I18n.t('Search devices')} search={true} />
          </div>
        </div>
      </div>
    )
  }
}
export default Devices;


