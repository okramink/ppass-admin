/*eslint-disable*/
import React     from 'react';
import ReactDOM  from 'react-dom';
import PropTypes from 'prop-types';
// import moment    from 'moment';



//Actions
import {
  setCurrent,
  loadAffiliates,
  getAffiliated,
  loadAffiliatesPassports, 
  getAffiliatesPassports
} from '../../actions/affiliates';

//Components
import Table      from '../../components/Table';
import {I18n,Translate, Localize} from 'react-redux-i18n';

import AddForm from '../../components/affiliates/AddForm';
import AddUserForm from '../../components/affiliates/AddUserForm';
import AddUserFormFA from '../../components/affiliates/AddUserFormFA';


/*eslint-enable*/

class Affiliated extends React.Component {
  static get contextTypes() {
    return {
      router: PropTypes.object.isRequired,
      store: PropTypes.object.isRequired,
      socket: PropTypes.object.isRequired
    }
  }
  constructor(props,context){
    super(props,context);
    this.context   = context;
    this.store     = this.context.store;
    this.state     = this.store.getState();
     this.socket   = this.context.socket;

     this.state = {
        current :{
          name :''
        }
     }
     this.headers = [
        {db:'stoken',label:I18n.t('Update'), icon:'ion-edit', align:'center', width:'10px',badge:'link', onclick: (o) => {
               window.Light = window.YagamiBox.init({
                title: I18n.t('Update Affiliated'),
                width:'70%',
                height:'600px',
                setDomContent : (container) => {
                    ReactDOM.render(<AddForm data={o} context={this.context} modal={true} />, container);
                }
            })
        }},
        {db:"name",label:I18n.t('Name'), type:'text' , align:'left', width:'250px'},            
        {db:"code",label:I18n.t('Code'),type:'text' , align:'center',width:'50px', badge:'primary'},   
        {db:"users",label:I18n.t('Users'),type:'text' , align:'center',width:'50px', badge:'primary',value:(o,i)=>{
          return o.users.length;
        }},
        {db:'createdAt',label:I18n.t('Created at'),type:'datetime',width:'50px', align:'center', icon:'ion-calendar', badge:'primary'}
    ]

    this.passport_headers = [
        {db:'stoken',label:I18n.t('Token'), icon:'ion-edit', width:'150px',badge:'link', onclick: (o) => {
            // window.location.href = '/passports/update/'+o.id;
        }},
        {db:"name",label:I18n.t('User'), type:'text' , align:'left', width:'150px'},     
        {db:"affiliated",label:I18n.t('Refer'), badge:{default:'default',null:''}, type:'text' , align:'center', width:'50px', value:(o,i)=>{
          return o.affiliated ? o.affiliated.code.toUpperCase() : 'PPASS';
        }},     
        {db:"status",label:I18n.t('Status'),type:'text' , align:'center',width:'50px', badge:{default:'default',completed:'success',pending:'danger'}, value:(o,i)=>{
            return o.notify === 1 ? 'Completed' : 'Pending';
        }},            
        {db:"notify",label:I18n.t('Notified'),type:'text' , align:'center',width:'50px', badge:{default:'default',done:'success',pending:'danger'}, value:(o,i)=>{
            return o.notify === 1 ? 'Done' : 'Pending';
        }},  
        {db:"people",label:I18n.t('People'),type:'money' , align:'center',width:'50px', badge:'primary',value:(o,i)=>{
            const a = []
            if(typeof(o.people) == 'string'){
              let people = JSON.parse(o.people);
                  a.push(people)
              return  a.length; 
            }else{
              o.people.forEach((item,i)=>{
                if(item.name !== ''){
                  a.push(item)
                }
              })
              return  a.length;
            }
        }},      
        {db:"total",label:I18n.t('Payment'),type:'money' , align:'center',width:'100px', badge:'primary',value:(o,i)=>{
            return isNaN(Number(o.total)) ? '0.00' : o.total; 
        }},
        {db:'createdAt',label:I18n.t('Created at'),type:'datetime',width:'100px', align:'center', icon:'ion-calendar', badge:'primary'}
    ]   

  }
  componentWillUnmount(){
    this.unsuscribe();
   
  }
  componentDidMount(){
     this.unsuscribe = this.store.subscribe(() => {
        this.setState(this.store.getState());
     });
  }

  componentWillMount(){
     if( this.props.computedMatch.params.id ){
      this.store.dispatch(getAffiliated({id:this.props.computedMatch.params.id}))
      //console.log( this.props )
      this.store.dispatch(getAffiliatesPassports({id:this.props.computedMatch.params.id}));
    }
  }

  requestPassports(params){

    params.affiliated = this.props.computedMatch.params.id;
    this.store.dispatch(loadAffiliatesPassports(params));
  }
  addUserForm(){
    window.Light = window.YagamiBox.init({
        title: I18n.t('Create passport') ,
        width:'70%',
        height:'600px',
        setDomContent : function(container){
            ReactDOM.render(<AddUserForm data={this.state.affiliates.current} owner={this.state.auth.user.id} context={this.context} modal={true} />, container);
        }.bind(this)
    })
  }
  addUserFormFA(){
    window.Light = window.YagamiBox.init({
        title: I18n.t('Create passport-A_F-activities') ,
        width:'70%',
        height:'600px',
        setDomContent : function(container){
            ReactDOM.render(<AddUserFormFA posts={this.state.affiliates.showPosts} data={this.state.affiliates.current} owner={this.state.auth.user.id} context={this.context} modal={true} />, container);
        }.bind(this)
    })
  }
  addForm(){
    window.Light = window.YagamiBox.init({
        title: I18n.t('Update affiliated'),
        width:'70%',
        height:'600px',
        setDomContent : (container) => {
          ReactDOM.render(<AddForm data={this.state.affiliates.current} context={this.context} modal={true} />, container);
        }
    })
  }
  back(){
     this.context.router.history.push('/affiliates');
  }
  render(){
    
    if( this.state.affiliates ){
      return (
        <div className="page container">
            <h1>  
            
            <button type="button" className="btn btn-outline-dark btn-sm float-left" onClick={this.back.bind(this) }> 
            <i className="ion-android-arrow-back" /> 
            </button> 

            &nbsp;  {this.state.affiliates.current.name} &nbsp;  
            <button type="button" className="btn btn-success btn-sm float-right btn-space-left" onClick={this.addUserForm.bind(this) }> 
            <i className="ion-plus-circled" /> Create passport
            </button>

            <button type="button" className="btn btn-success btn-sm float-right btn-space-left" onClick={this.addUserFormFA.bind(this) }> 
            <i className="ion-plus-circled" /> Create passport-FA-activities
            </button> 

            <button type="button" className="btn btn-outline-primary btn-sm float-right btn-space-left" onClick={this.addForm.bind(this) }> 
              <i className="ion-edit" />
            </button> 
            </h1>
            <hr className="divider" />
            <div className="row  ">
              <div className="col-md-12">
                <div className="row">
                    <div className="card-body">
                        <Table data={this.state.affiliates.passports} source={this.requestPassports.bind(this)} headers={this.passport_headers} page={1} rows={5} placeholder={I18n.t('Search Passport')} search={true} />
                    </div>
                  </div>
              </div>
            </div>
        </div>
      )
    }else{
      return null;
    }
  }
}

export default Affiliated;
