import React              from 'react';
// import ReactDOM             from 'react-dom';
import PropTypes            from 'prop-types';
// import { Prompt }           from 'react-router';
import {Translate}          from 'react-redux-i18n';
// import  FroalaEditor        from 'react-froala-editor';
// import Select               from 'react-select';
import ReactQuill           from 'react-quill'; // ES6
import Globals              from '../../globals';

import TimeWidget          from '../../components/posts/time'
import TagsWidget          from '../../components/posts/tags'
import CategoriesWidget    from '../../components/posts/categories'
import FeaturedImageWidget from '../../components/posts/featured'

//import calendar restiction
import DayPicker, { DateUtils } from 'react-day-picker';
import 'react-day-picker/lib/style.css';

import moment from 'moment';

//Actions
import {
  saveFa,
  setTags,
  setTime,
  setCategories,
  setHTMLContent,
  setImageFeature,
  getPostFa,
  setAlert
} from '../../actions/posts';


class Alert extends React.Component {
  static contextTypes = {
    router: PropTypes.object.isRequired,
    store:  PropTypes.object.isRequired
  };

  constructor(props,context){
    super(props,context);
    this.context = context;
    this.store   = this.context.store;
    this.state   = this.store.getState();
  }

  close() {
    this.context.store.dispatch(setAlert(false,null,null))
  }

  render(){
    if(this.props.status){
      return(
        <div className={`alert alert-${this.props.mode}`} role="alert">
          <button type="button" className="close" onClick={this.close.bind(this)}>
            <span aria-hidden="true">&times;</span>
          </button>
          {this.props.msg}
        </div>
      )
    }else{
      return null;
    }
  }
}


//posts  routes
class ArticlesAdd extends React.Component {
  static get contextTypes() {
    return {
        router: PropTypes.object.isRequired,
        store: PropTypes.object.isRequired
    }
  }

  constructor(props,context){
    super(props,context);
    this.context = context;
    this.store   = this.context.store;
    this.state   = this.store.getState();
    
    this.handleDayClick = this.handleDayClick.bind(this);
    
    this.state.selectedDays = [];
    this.toolbarOptions = [
      ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
      ['blockquote', 'code-block'],
      [{ 'list': 'ordered'}, { 'list': 'bullet' }],
      [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
      [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
      [{ 'direction': 'rtl' }],                         // text direction

      [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
      [{ 'align': [] }],

      ['clean','image','video']                                         // remove formatting button
    ];

    this.selectPromo = this.selectPromo.bind(this);
  }

  componentWillMount(){
    if(this.props.computedMatch.params.id){
      this.store.dispatch(getPostFa({id:this.props.computedMatch.params.id})).then((data) => {
        //console.log(data)
        this.refs.title.value               = data.title ? data.title : '';
        this.refs.description.value         = data.description ? data.description : '';
        this.refs.notification.value        = data.notification ? data.notification : '';
        this.refs.emailenglish.value        = data.emailenglish ? data.emailenglish : '';
        this.refs.emailspanish.value        = data.emailspanish ? data.emailspanish : '';
        this.refs.name.value                = data.name ? data.name : '';
        this.refs.address.value             = data.address ? data.address : '';
        this.refs.priority.value            = data.priority ? data.priority : 7;
        this.refs.cant_veces.value          = data.cant_veces ? data.cant_veces : 7;
        this.refs.cant_veces_al_dia.value   = data.cant_veces_al_dia ? data.cant_veces_al_dia : 7;
        this.refs.location.value            = data.location ? data.location : '';
        this.refs.favc.checked              = data.favc ? data.favc : false;
        this.refs.flabc.checked             = data.flabc ? data.flabc : false;
        this.refs.fgcbc.checked             = data.fgcbc ? data.fgcbc : false;
        this.refs.fvc.checked               = data.fvc ? data.fvc : false;
        this.refs.fcc.checked               = data.fcc ? data.fcc : false;
        this.refs.flabp.checked             = data.flabp ? data.flabp : false;
        this.refs.faic.checked              = data.faic ? data.faic : false;
        this.refs.ftec.checked              = data.ftec ? data.ftec : false;
        this.refs.lat.value                 = data.lat ? data.lat : '';
        this.refs.lng.value                 = data.lng ? data.lng : '';
        this.refs.price.value               = data.price ? parseFloat(data.price).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,') : '';
        this.refs.priceC.value              = data.priceC ? parseFloat(data.priceC).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,') : '';
        this.refs.discPrice.value           = data.discPrice ? parseFloat(data.discPrice).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,') : '';
        this.refs.discCPrice.value          = data.discCPrice ? parseFloat(data.discCPrice).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,') : '';
        this.refs.pricemxn.value            = data.pricemxn ? parseFloat(data.pricemxn).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,') : '';
        this.refs.priceCmxn.value           = data.priceCmxn ? parseFloat(data.priceCmxn).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,') : '';
        this.refs.discPricemxn.value        = data.discPricemxn ? parseFloat(data.discPricemxn).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,') : '';
        this.refs.discCPricemxn.value       = data.discCPricemxn ? parseFloat(data.discCPricemxn).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,') : '';
        this.refs.promoPrice.value          = data.promoPrice ? parseFloat(data.promoPrice).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,') : '';
        this.refs.promoCPrice.value         = data.promoCPrice ? parseFloat(data.promoCPrice).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,') : '';
        this.refs.sluglink.innerHTML        = Globals.domain+'/articles/'+data.name;
        this.refs.sluglink.href             = Globals.domain+'/articles/'+data.name;

        let daterange = []
        if(data.calendar_restriction){
          data.calendar_restriction.map((res, key) => {
            //console.log(res)
            daterange.push(moment(res, 'YYYY-MM-DD').toDate())
          })
        }

        //console.log(daterange)
        this.setState({selectedDays: daterange})

        if( data.content ){
          this.store.dispatch(setHTMLContent(data.content));
        }

        this.store.dispatch(setImageFeature(data.featured));
        if (data.promoType) {
          this.setState({promoTypeSelect: data.promoType}, () => {
            this.refs.promoType.value = data.promoType || '';
            if (data.promoType === 'NxM') {
              const perData        = data.promoContent.split('x');
              const perDatamxn        = data.promoContentmxn.split('x');
              this.refs.nxm1.value = perData[0];
              this.refs.nxm2.value = perData[1];
            } else if (data.promoType === 'Percentage') {
              this.refs.percentageValue.value = data.promoContent;
              this.refs.persValuemxn.value = data.promoContentmxn ? data.promoContentmxn : null;
            } else if (data.promoType === 'Personalized') {
              this.refs.persValue.value = data.promoContent;
              this.refs.persValuemxn.value = data.promoContentmxn ? data.promoContentmxn : null;
            }
          });
        }

        if (data.bgImage) {
          this.setState({bgImage:data.bgImage});
        }

        if (data.galleryImg) {
          let galleryImg = null;
          if (typeof data.galleryImg === 'string') {
            galleryImg = JSON.parse(data.galleryImg)
          } else {
            galleryImg = data.galleryImg;
          }
          this.setState({galleryImg});
        }

        let  time = [];
        if(data.time != undefined){
          for(let x=0; x < data.time.length; x++) {
            if( data.time[x] !== ''){
              time.push({label:data.time[x]})
            }
          }
        }

        let  tags = [];
        console.log(data)
        for(let x=0; x < data.tags.length; x++) {
          if( data.tags[x] !== ''){
            tags.push({label:data.tags[x]})
          }
        }

         let  categories = [];
        for(let x=0; x < data.categories.length; x++){
          if( data.categories[x] !== ''){
            categories.push({label:data.categories[x]})
          }
        }

        this.store.dispatch(setCategories(categories))
        this.store.dispatch(setTags(tags))
        
        this.store.dispatch(setTime(time))

      }).catch((e)=>{
        alert(`[ERROR] => ${JSON.stringify(e)}`)
        this.context.router.history.push('/postsfa')
      })
    }
  }
  componentWillUnmount() {
    this.unsuscribe();
  }

  componentDidMount(){
    this.unsuscribe = this.store.subscribe(() => {
      this.setState(this.store.getState());
    });
  }

  slugify(str){
    str = str.replace(/^\s+|\s+$/g, ''); // trim
    str = str.toLowerCase();

    // remove accents, swap ñ for n, etc
    var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
    var to   = "aaaaeeeeiiiioooouuuunc------";

    for (var i=0, l=from.length ; i<l ; i++)
      str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));

    str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
             .replace(/\s+/g, '-') // collapse whitespace and replace by -
             .replace(/-+/g, '-'); // collapse dashes

    return str;
  }

  onSubmit(e){

    e.preventDefault();

    let time = [];
    for(let x=0; x < this.state.posts.timeSelect.value.length; x++) {
      time.push(this.state.posts.timeSelect.value[x].label)
    }

    let tags = [];
    for(let x=0; x < this.state.posts.tagsSelect.value.length; x++) {
      tags.push(this.state.posts.tagsSelect.value[x].label)
    }

    let  categories = [];
    for (let x=0; x < this.state.posts.categoriesSelect.value.length; x++) {
      categories.push(this.state.posts.categoriesSelect.value[x].label)
    }
    let postID = this.state.posts.post ? this.state.posts.post.id : '';
        postID = !postID ? '' : postID;

    const { nxm1, nxm2, promoType, percentageValue, persValue, persValuemxn, lat, lng } = this.refs;
    const gallery = this.state.galleryImg ? JSON.stringify(this.state.galleryImg) : null;

    const data = {
      id            : postID,
      name          : this.refs.name.value,
      title         : this.refs.title.value,
      content       : this.state.posts.content,
      description   : this.refs.description.value,
      notification  : this.refs.notification.value,
      emailenglish  : this.refs.emailenglish.value,
      emailspanish  : this.refs.emailspanish.value,
      time          : time.join(','),
      tags          : tags.join(','),
      categories    : categories.join(','),
      price         : this.refs.price.value || 0,
      priceC        : this.refs.priceC.value || 0,
      discPrice     : this.refs.discPrice.value || 0,
      discCPrice    : this.refs.discCPrice.value || 0,
      pricemxn      : this.refs.pricemxn.value || 0,
      priceCmxn     : this.refs.priceCmxn.value || 0,
      discPricemxn  : this.refs.discPricemxn.value || 0,
      discCPricemxn : this.refs.discCPricemxn.value || 0,
      promoPrice    : this.refs.promoPrice.value || 0,
      promoCPrice   : this.refs.promoCPrice.value || 0,
      featured      : this.state.posts.featured,
      promoType     : this.refs.promoType.value || '',
      bgImage       : this.state.bgImage || null,
      galleryImg    : gallery,
      address       : this.refs.address.value,
      location      : this.refs.location.value,
      priority      : parseInt(this.refs.priority.value),
      cant_veces    : parseInt(this.refs.cant_veces.value),
      cant_veces_al_dia    : parseInt(this.refs.cant_veces_al_dia.value),
      favc          : this.refs.favc.checked,
      flabc         : this.refs.flabc.checked,
      fgcbc         : this.refs.fgcbc.checked,
      fvc           : this.refs.fvc.checked,
      fcc           : this.refs.fcc.checked,
      flabp         : this.refs.flabp.checked,
      faic          : this.refs.faic.checked,
      ftec          : this.refs.ftec.checked,
      calendar_restriction : this.state.selectedDays,
    }

    if (promoType.value === 'NxM') {
      data.promoType    = this.refs.promoType.value;
      data.promoContent = nxm1.value+'x'+nxm2.value;
    }

    if (this.refs.promoType.value === 'Percentage') {
      data.promoType    = this.refs.promoType.value;
      data.promoContent = percentageValue.value;
    }

    if (this.refs.promoType.value === 'Personalized') {
      data.promoType    = this.refs.promoType.value;
      data.promoContent = persValue.value;
      data.promoContentmxn = persValuemxn.value;
    }
    if (lat.value.trim().length > 0 || lng.value.trim().length > 0) {
        data.lat = lat.value;
        data.lng = lng.value;
    }
    console.log('FORM DATA')
    console.log(this.state.selectedDays);
    // console.log(data.favc)
    this.store.dispatch(saveFa(data));
  };

  onKeyUp(e) {
    const s = this.slugify(e.target.value);
    this.refs.sluglink.innerHTML = Globals.domain+'/articles/'+s;
    this.refs.sluglink.href      = Globals.domain+'/articles/'+s;
    this.refs.name.value = s;
    this.onChangeTitle();
  }

  onChangeTitle(){
    if( this.refs.title.value.length === 0 ){
      this.setState({noLeave:true})
    }else{
      this.setState({noLeave:false})
    }
  }

  selectTagsChange(value) {
    this.store.dispatch(setTags(value))
  }

  
  selectTimeChange(value) {
    this.store.dispatch(setTime(value))
  }

  selectCategoriesChange(value) {
    this.store.dispatch(setCategories(value))
  }

  handleChangeQuill(value) {
    this.store.dispatch(setHTMLContent(value))
  }

  setBgImage = (data) => {
    this.setState({bgImage:data});
  };

  removeImage = (name) => {
    let change   = {};
    change[name] = null;
    this.setState(change);
  };

  removeSingle = (i) =>{
    let { galleryImg } = this.state;
    galleryImg.splice(i,1);
    this.setState({galleryImg});
  };

  setGalleryImage = (data) => {
    if (this.state.galleryImg) {
      data = this.state.galleryImg.concat(data);
    }
    this.setState({galleryImg:data})
  };

  formatPrice(e) {
    if( !isNaN( parseFloat(e.target.value) )){
      if( e.target.value.length > 0 ){
        e.target.value = parseFloat(e.target.value).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')
      }
    }else{
      e.target.value = '';
    }
  }

  selectPromo(promoTypeSelect) {
    if (promoTypeSelect !== '') {
      this.setState({promoTypeSelect});
    } else {
      this.setState({promoTypeSelect: null});
    }
  }

  postFormSubmit(){
    this.refs.postForm.submit()
  }
  
  cancelPaste(ev){
    console.log('cancelando')
    return false;
    ev.preventDefault();
  }

  handleDayClick(day, { selected }) {
    const { selectedDays } = this.state;
    if (selected) {
      const selectedIndex = selectedDays.findIndex(selectedDay =>
        DateUtils.isSameDay(selectedDay, day)
      );
      selectedDays.splice(selectedIndex, 1);
    } else {
      selectedDays.push(day);
    }
    this.setState({ selectedDays });
  }

  render() {
    console.log(this.state.selectedDays)
    return (
      <div className="page container">
        <div className="row">
          <div className="col-md-12">
            {this.props.computedMatch.params.id !== undefined ? (
              <h1><i className="ion-compose"/> Update Offer </h1>
            ) : (<h1><i className="ion-compose"/> Add New Offer </h1>)}

             <Alert status={this.state.posts.alert_status} msg={this.state.posts.alert_msg} mode={this.state.posts.alert_mode} />

            <hr />
          </div>
        </div>

        <div className="row">
          <div className="col-md-8">
            <form onSubmit={this.onSubmit.bind(this)} ref="postForm">
              <div className="row">
               <div className="col-sm-12">
                  <div className="form-group">
                    <input className="form-control" ref="title" placeholder="Title" onKeyUp={this.onKeyUp.bind(this)} required />
                    <input type="hidden" ref="name" />
                    <small className="form-text text-slugfy"> <a ref="sluglink" to="#" target="_blank" rel="noopener noreferrer" >{Globals.domain}/articles/</a> <i className="ion-link" /> </small>
                  </div>

                  <div className="form-group">
                    <label>Text Preview</label>
                     <textarea className="form-control"  rows="6" ref="description" required ></textarea>
                  </div>
                  
                  <div className="form-group">
                    <label>Notification In the post</label>
                     <textarea className="form-control"  rows="6" ref="notification" required ></textarea>
                  </div>
                </div>
              </div>

              <div className="row">
                <div className="col-sm-12">
                  <div className="form-group form-group-quill-container" >
                    <label><Translate value="Content"/></label>
                    <ReactQuill theme="snow" value={this.state.posts.content} onChange={this.handleChangeQuill.bind(this)} placeholder=" " className="form-group-quill-editor"
                      modules={{
                        toolbar: this.toolbarOptions
                      }}
                    />
                  </div>
                </div>
              </div>
              <div className="card form-group ">
                <div className="card-header">Address </div>
                <div className="card-body">
                    <div className="row">
                      <div className="col-md-12 form-group">
                          <textarea className="form-control" rows="3" ref="address"></textarea>
                      </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-6">
                            <small className="form-help">Latitude: </small>
                            <input type="text" ref="lat"
                              className="form-control"
                              placeholder="Enter GoogleMaps Latitude"
                            />
                        </div>
                        <div className="col-sm-6">
                            <small className="form-help">Longitude: </small>
                            <input type="text" ref="lng"
                              className="form-control"
                              placeholder="Enter GoogleMaps Longitude"
                            />
                        </div>
                    </div>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12">
                  <div className="card form-group ">
                  <div className="card-header"> Price <small> *If not child price, system will take adult price</small></div>

                    <div className="card-body">
                      <label>Full Price</label>
                      <div className="row">
                        <div className="col-sm-6">
                          <div className="col-sm-12">
                            <small className="form-help">Adult</small>
                            <div className="input-group">
                              <span className="input-group-addon" style={{marginRight:`5px`}}>$USD </span>
                              <input type="text" ref="price"
                                onBlur={this.formatPrice.bind(this)}
                                className="form-control form-control-money"
                                aria-label="Amount (to the nearest dollar)"
                                placeholder="0.00"
                                defaultValue="0.00"
                                style={{textAlign:'left'}}
                                required
                                />
                            </div>
                          </div>
                          <div className="col-sm-12">
                            <small className="form-help">Adult</small>
                            <div className="input-group">
                              <span className="input-group-addon" style={{marginRight:`5px`}}>$MXN</span>
                              <input type="text" ref="pricemxn"
                                onBlur={this.formatPrice.bind(this)}
                                className="form-control form-control-money"
                                aria-label="Amount (to the nearest dollar)"
                                placeholder="0.00"
                                defaultValue="0.00"
                                style={{textAlign:'left'}}
                                required
                                />
                            </div>
                          </div>
                        </div>
                        <div className="col-sm-6">
                          <div className="col-sm-12">
                            <small className="form-help">Child</small>
                            <div className="input-group">
                              <span className="input-group-addon">$USD</span>
                              <input type="text" ref="priceC"
                                onBlur={this.formatPrice.bind(this)}
                                className="form-control form-control-money"
                                aria-label="Amount (to the nearest dollar)"
                                placeholder="0.00"
                                defaultValue="0.00"
                                style={{textAlign:'left'}}
                                required
                                />
                            </div>
                          </div>
                          <div className="col-sm-12">
                            <small className="form-help">Child</small>
                            <div className="input-group">
                              <span className="input-group-addon">$MXN</span>
                              <input type="text" ref="priceCmxn"
                                onBlur={this.formatPrice.bind(this)}
                                className="form-control form-control-money"
                                aria-label="Amount (to the nearest dollar)"
                                placeholder="0.00"
                                defaultValue="0.00"
                                style={{textAlign:'left'}}
                                required
                                />
                            </div>
                          </div>
                        </div>
                      </div>
                      <br/>
                      <label>Discount Price</label>
                      <div className="row">
                        <div className="col-sm-6">
                          <div className="col-sm-12">
                            <small className="form-help">Adult</small>
                            <div className="input-group">
                              <span className="input-group-addon">$USD</span>
                              <input type="text" ref="discPrice"
                                onBlur={this.formatPrice.bind(this)}
                                className="form-control form-control-money"
                                aria-label="Amount (to the nearest dollar)"
                                placeholder="0.00"
                                defaultValue="0.00"
                                style={{textAlign:'left'}}
                              />
                            </div>
                          </div>
                          <div className="col-sm-12">
                            <small className="form-help">Adult</small>
                            <div className="input-group">
                              <span className="input-group-addon">$MXN</span>
                              <input type="text" ref="discPricemxn"
                                onBlur={this.formatPrice.bind(this)}
                                className="form-control form-control-money"
                                aria-label="Amount (to the nearest dollar)"
                                placeholder="0.00"
                                defaultValue="0.00"
                                style={{textAlign:'left'}}
                              />
                            </div>
                          </div>
                        </div>
                        <div className="col-sm-6">
                          <div className="col-sm-12">
                            <small className="form-help">Child</small>
                            <div className="input-group">
                              <span className="input-group-addon">$USD</span>
                              <input type="text" ref="discCPrice"
                                onBlur={this.formatPrice.bind(this)}
                                className="form-control form-control-money"
                                aria-label="Amount (to the nearest dollar)"
                                placeholder="0.00"
                                defaultValue="0.00"
                                style={{textAlign:'left'}}
                              />
                            </div>
                          </div>
                          <div className="col-sm-12">
                            <small className="form-help">Child</small>
                            <div className="input-group">
                              <span className="input-group-addon">$MXN</span>
                              <input type="text" ref="discCPricemxn"
                                onBlur={this.formatPrice.bind(this)}
                                className="form-control form-control-money"
                                aria-label="Amount (to the nearest dollar)"
                                placeholder="0.00"
                                defaultValue="0.00"
                                style={{textAlign:'left'}}
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="card form-group ">
                <div className="card-header">PromoPrice <small> *If not child price, system will take adult price</small></div>
                <div className="card-body">
                  <div className="row">
                    <div className="col-sm-6">
                      <small className="form-help">Adult</small>
                      <div className="input-group">
                        <span className="input-group-addon">$USD</span>
                        <input type="text" ref="promoPrice"
                          onBlur={this.formatPrice.bind(this)}
                          className="form-control form-control-money"
                          aria-label="Amount (to the nearest dollar)"
                          placeholder="0.00"
                          defaultValue="0.00"
                          style={{textAlign:'left'}}
                        />
                      </div>
                    </div>

                    <div className="col-sm-6">
                      <small className="form-help">Child</small>
                      <div className="input-group">
                        <span className="input-group-addon">$MXN</span>
                        <input type="text" ref="promoCPrice"
                          onBlur={this.formatPrice.bind(this)}
                          className="form-control form-control-money"
                          aria-label="Amount (to the nearest dollar)"
                          placeholder="0.00"
                          defaultValue="0.00"
                          style={{textAlign:'left'}}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-sm-12">
                  <FeaturedImageWidget
                    multi={true}
                    title="Gallery"
                    getData={(data) => this.setGalleryImage(data)}
                    removeSelected={() => this.removeImage('galleryImg')}
                    selectedImage={this.state.galleryImg}
                    removeImage={(i) => this.removeSingle(i)}
                    gallery={true}
                  />
                </div>
              </div>
              <hr/><br/><br/><br/>
              <div className="row">
                <div className="col-sm-6">
                  <div className="form-group">
                    <label><b>Note:</b> this only works in emails with PDFs - English (max. characters #300)</label>
                     <textarea  maxLength="300" className="form-control"  rows="6" ref="emailenglish" required ></textarea>
                  </div>
                </div>
                <div className="col-sm-6">
                  <div className="form-group">
                    <label><b>Note:</b> this only works in emails with PDFs - Spanish (max. characters #350)</label>
                     <textarea className="form-control"  rows="6"  maxLength="350" ref="emailspanish" required ></textarea>
                  </div>
                </div>
              </div>
              <hr/>
              </form>
            </div>

            <div className="col-md-4 posts-side">
              <div className="card form-group ">
                <div className="card-body">
                    <button className="btn btn-sm btn-success btn-block" onClick={this.onSubmit.bind(this) }  ><i className="ion-edit" /> Save post </button>
                </div>
              </div>
              <div className="card form-group">
                <div className="card-header">Calendar Restictions - (Select Available[s] Day[s])</div>
                <div className="card-body">
                  <div className="form-check">
                    <DayPicker 
                      selectedDays={this.state.selectedDays}
                      onDayClick={this.handleDayClick}
                    />
                  </div>
                </div>
              </div>
              <div className="card form-group ">
                <div className="card-header">Affiliations: </div>
                <div className="card-body">
                  <div className="form-check">
                    <input type="checkbox" id="exampleCheck1" name="favc" ref="favc" />
                    <label className="form-check-label" htmlFor="exampleCheck1">Fiesta Americana</label>
                  </div>
                  <div className="form-check">
                    <input type="checkbox" id="exampleCheck2" name="flabc" ref="flabc" />
                    <label className="form-check-label" htmlFor="exampleCheck2">FAVC LIVE AQUA BEACH CANCUN</label>
                  </div>
                  <div className="form-check">
                    <input type="checkbox" id="exampleCheck3" name="fgcbc" ref="fgcbc" />
                    <label className="form-check-label" htmlFor="exampleCheck3">FAVC GRAND CORAL BEACH CANCUN</label>
                  </div>
                  <div className="form-check">
                    <input type="checkbox" id="exampleCheck4" name="fvc" ref="fvc" />
                    <label className="form-check-label" htmlFor="exampleCheck4">FAVC VILLAS CANCUN</label>
                  </div>
                  <div className="form-check">
                    <input type="checkbox" id="exampleCheck5" name="fcc" ref="fcc" />
                    <label className="form-check-label" htmlFor="exampleCheck5"> FAVC CONDESA CANCUN</label>
                  </div>
                  <div className="form-check">
                    <input type="checkbox" id="exampleCheck6" name="flabp" ref="flabp" />
                    <label className="form-check-label" htmlFor="exampleCheck6">FAVC LIVE AQUA BOUTIQUE PLAYA</label>
                  </div>
                  <div className="form-check">
                    <input type="checkbox" id="exampleCheck8" name="faic" ref="faic" />
                    <label className="form-check-label" htmlFor="exampleCheck8">FAVC ALL INCLUSIVE COZUMEL</label>
                  </div>
                  <div className="form-check">
                    <input type="checkbox" id="exampleCheck7" name="ftec" ref="ftec" />
                    <label className="form-check-label" htmlFor="exampleCheck7"> FAVC THE EXPLOREAN COZUMEL</label>
                  </div>
                </div>
              </div>
              
              <div className="card form-group ">
                <div className="card-header">How much the user can use the post (default 7, is required*): </div>
                <div className="card-body">
                  <div className="form-group">
                    <select
                      className="form-control"
                      name="cant_veces"
                      defaultValue={1}
                      ref="cant_veces"
                      required
                    >
                      <option value={1}>1</option>
                      <option value={2}>2</option>
                      <option value={3}>3</option>
                      <option value={4}>4</option>
                      <option value={5}>5</option>
                      <option value={6}>6</option>
                      <option value={7}>7</option>
                    </select>
                  </div>
                </div>
              </div>
              <div className="card form-group ">
                <div className="card-header">How much the user can use this post at day (default 7, is required*): </div>
                <div className="card-body">
                  <div className="form-group">
                    <select
                      className="form-control"
                      name="cant_veces_al_dia"
                      defaultValue={1}
                      ref="cant_veces_al_dia"
                      required>
                      <option value={1}>1</option>
                      <option value={2}>2</option>
                      <option value={3}>3</option>
                      <option value={4}>4</option>
                      <option value={5}>5</option>
                      <option value={6}>6</option>
                      <option value={7}>7</option>
                    </select>
                  </div>
                </div>
              </div>

              <div className="card form-group ">
                <div className="card-header">Post priority (default 7): </div>
                <div className="card-body">
                  <div className="form-group">
                    <select
                      className="form-control"
                      name="priority"
                      defaultValue={6}
                      ref="priority"
                    >
                      <option value={0}>0</option>
                      <option value={1}>1</option>
                      <option value={2}>2</option>
                      <option value={3}>3</option>
                      <option value={4}>4</option>
                      <option value={5}>5</option>
                      <option value={6}>6</option>
                      <option value={7}>7</option>
                    </select>
                  </div>
                </div>
              </div>

              <div className="card form-group ">
                <div className="card-header">Location: </div>
                <div className="card-body">
                  <div className="form-group">
                    <select
                      className="form-control"
                      name="location"
                      defaultValue={6}
                      ref="location"
                    >
                      <option value="" disabled selected>Select Location</option>
                      <option value={"cancun"}>Cancun</option>
                      <option value={"puerto-morelos"}>Puerto Morelos</option>
                      <option value={"playa-del-carmen"}>Playa del Carmen</option>
                      <option value={"puerto-aventuras"}>Puerto Aventuras</option>
                      <option value={"tulum"}>Tulum</option>
                      <option value={"cozumel"}>Cozumel</option>
                      <option value={"isla-mujeres"}>Isla Mujeres</option>
                    </select>
                  </div>
                </div>
              </div>

              <FeaturedImageWidget
                onChange = {this.selectCategoriesChange.bind(this)}
                multi={false}
                title="Featured Image"
                featureImage={true}
                placeholder={true}
              />

              <FeaturedImageWidget
                multi={false}
                title="Background Image"
                getData={(data) => this.setBgImage(data)}
                removeSelected={() => this.removeImage('bgImage')}
                selectedImage={this.state.bgImage}
              />

              <TimeWidget
                disabled = {this.state.posts.timeSelect.disabled}
                value    = {this.state.posts.timeSelect.value}
                onChange = {this.selectTimeChange.bind(this)}
              />
              <TagsWidget
                disabled = {this.state.posts.tagsSelect.disabled}
                value    = {this.state.posts.tagsSelect.value}
                onChange = {this.selectTagsChange.bind(this)}
              />

              <CategoriesWidget
                disabled = {this.state.posts.categoriesSelect.disabled}
                value    = {this.state.posts.categoriesSelect.value}
                onChange = {this.selectCategoriesChange.bind(this)}
              />
              <div className="card form-group ">
                <div className="card-header">Other Promotions </div>
                <div className="card-body">
                  <div className="form-group">
                    <select
                      className="form-control"
                      name="promoType"
                      defaultValue=""
                      ref="promoType"
                      onChange={(e) => this.selectPromo(e.target.value)}
                    >
                      <option value="">Select Promo Type</option>
                      <option value="NxM">2x1 (NxM)</option>
                      <option value="Percentage">Percentage (25%, 30%, etc)</option>
                      <option value="Personalized">Personalized (Ej. Free Frijoles Charros)</option>
                    </select>
                  </div>
                  { this.state.promoTypeSelect === 'NxM' &&
                    <div className="form-group">
                      <div className="row">
                        <div className="col-sm-5">
                          <label>Promo number</label>
                          <input type="text" ref="nxm1"
                            className="form-control"
                            placeholder="2"
                            defaultValue="2"
                          />
                        </div>
                        <div className="col-sm-2">
                          <label> - </label>
                          <span className="nxmX" style={{display:'block',lineHeight:'28px',textAlign:'center',fontSize:'22px',fontWeight:'400'}}>x</span>
                        </div>
                        <div className="col-sm-5">
                          <label>Promo number</label>
                          <input type="text" ref="nxm2"
                            className="form-control"
                            placeholder="1"
                            defaultValue="1"
                          />
                        </div>
                      </div>
                    </div>
                  }

                  { this.state.promoTypeSelect === 'Percentage' &&
                    <div>
                      <label>Percentage</label>
                      <div className="input-group">
                        <input type="text" ref="percentageValue"
                          className="form-control"
                          placeholder="10"
                          defaultValue="10"
                          style={{textAlign:'right'}}
                        />
                        <span className="input-group-addon">%</span>
                      </div>
                      <small className="help-text text-muted">Please Enter number WITHOUT %</small>
                    </div>
                  }

                  { this.state.promoTypeSelect === 'Personalized' &&
                    <div>
                      <label>Personalized Promo</label>
                      <div className="form-group">
                        <input type="text" ref="persValue"
                          className="form-control"
                          placeholder="Ej. Free Frijoles Charros"
                          style={{textAlign:'right'}}
                        />
                      </div>
                      <label>Promo Personalizado</label>
                      <div className="form-group">
                        <input type="text" ref="persValuemxn"
                          className="form-control"
                          placeholder="Ej. Frijoles Charros Gratis"
                          style={{textAlign:'right'}}
                        />
                      </div>
                    </div>
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
    )
  }
}

export default ArticlesAdd;
