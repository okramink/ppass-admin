/*eslint-disable*/
import React     from 'react';
import ReactDOM  from 'react-dom';
import PropTypes from 'prop-types';
// import moment    from 'moment';



//Actions
import {
  loadAffiliatesFa,
  remove
} from '../actions/affiliates';

//Components
import Table      from '../components/Table';
import {I18n,Translate, Localize} from 'react-redux-i18n';

import AddForm from '../components/affiliates/AddForm';


/*eslint-enable*/

class Dashboard extends React.Component {
  static get contextTypes() {
    return {
      router: PropTypes.object.isRequired,
      store: PropTypes.object.isRequired,
      socket: PropTypes.object.isRequired
    }
  }
  constructor(props,context){
    super(props,context);
    this.context   = context;
    this.store     = this.context.store;
    this.state     = this.store.getState();
     this.socket   = this.context.socket;

     this.headers = [
        {db:'stoken',label:I18n.t('Update'), icon:'ion-edit', align:'center', width:'10px',badge:'link', onclick: (o) => {          
            window.location.href = '/affiliates/view/'+o.id;
        }},
        {db:"name",label:I18n.t('Name'), type:'text' , align:'left', width:'250px'},            
        {db:"users",label:I18n.t('Refer Users'),type:'text' , align:'center',width:'100px', badge:'primary',value:(o,i)=>{
          return String( o.users.length ) ;
        }},
        {db:"passports",label:I18n.t('Refer Passports'),type:'text' , align:'center',width:'100px', badge:'primary',value:(o,i)=>{
          return String( o.passports.length ) ;
        }},
        {db:"code",label:I18n.t('Code'),type:'text' , align:'center',width:'50px', badge:'primary',value:(o,i)=>{
          return o.code.toUpperCase()
        }},           
        {db:'createdAt',label:I18n.t('Created at'),type:'datetime',width:'50px', align:'center', icon:'ion-calendar', badge:'primary'}
    ]
  }
  componentWillUnmount() {
    this.unsuscribe();
  }
  componentDidMount(){
     this.unsuscribe = this.store.subscribe(() => {
        this.setState(this.store.getState());
     });
  }
  request(params){
    this.store.dispatch(loadAffiliatesFa(params));
  }
  addForm(){
    window.Light = window.YagamiBox.init({
        title: I18n.t('Add affiliated') ,
        width:'70%',
        height:'600px',
        setDomContent : function(container){
            ReactDOM.render(<AddForm  context={this.context} modal={true} />, container);
        }.bind(this)
    })
  }
  render(){
    return (
      <div className="page container">
         <div className="float-right menu-btns hide">
            
         </div>
          <h1> <i className="ion-ios-people"/> <Translate value="Affiliates"/> <button type="button" className="btn btn-success btn-sm float-right hide" onClick={this.addForm.bind(this) }> <i className="ion-plus-circled" /> Add </button> </h1>
          <hr className="divider" />
          <div className="row">
            <div className="col-md-12">
              <div className="row">
                  <div className="card-body">
                      <Table data={this.state.affiliates.data} source={this.request.bind(this)} headers={this.headers} page={1} rows={5} placeholder={I18n.t('Search affiliates')} search={true} />
                  </div>
                </div>
            </div>
          </div>
          <hr className="divider-30" />

          <hr className="divider-30" />
      </div>
    )
  }
}

export default Dashboard;