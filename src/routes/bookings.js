import React    from 'react';
import PropTypes from 'prop-types';
import {I18n, Translate} from 'react-redux-i18n';
import ReactDOM  from 'react-dom';

import ReactPDF, { Font, BlobProvider, Page, PDFDownloadLink, Text, View, Document, StyleSheet, PDFViewer } from '@react-pdf/renderer';
import styled from '@react-pdf/styled-components';

import moment from 'moment';

import AddForm from '../components/bookings/bookingUpdate';

//Actions
import {
    loadBookings,
    cancel,
    print,
    printConfirmation,
    savebook,
    saveAprovalBooking
} from '../actions/bookings';

//Components
import Table    from '../components/Table';

class Note extends React.Component {
  static get contextTypes() {
    return {
        router: PropTypes.object.isRequired,
        store: PropTypes.object.isRequired
    }
  }
  constructor(props,context){
    super(props,context);
    this.context = context;
    this.store   = this.context.store;
    this.state   = {
        notebook: ''
    }
    //console.log(this.props)
  }

  componentDidMount(){
       this.setState({notebook: this.props.data.booknote});
  }

  notebookchange(e){
    //console.log(this.refs.booknote.value)
    this.setState({notebook: this.refs.booknote.value})
  }  
  saveNote() {
    this.props.context.store.dispatch(savebook({booknote: this.refs.booknote.value, id: this.props.data.id, code: 'cbcfa'}));
  }
  render() {
    return (
      <div className="container">
        <div className="form-group"><br/>
          <label for="notebook">Add your NOTE:</label><br/>
          <textarea className="form-control" id="notebook" rows="7" ref="booknote" value={this.state.notebook} onChange={(e) => {this.notebookchange()}}></textarea>
        </div>
        <button type="button" className="btn btn-info" style={{float: `right`}} onClick={(e) => {this.saveNote()}}>Save</button>
      </div>
    )
  }
}

//Users routes
class Bookings extends React.Component {

  static get contextTypes() {
    return {
        router: PropTypes.object.isRequired,
        store: PropTypes.object.isRequired
    }
  }
  constructor(props,context){
    super(props,context);
    this.context = context;
    this.store   = this.context.store;
    this.state   = this.store.getState();

    this.creditResort = this.creditResort.bind(this)
    //console.log(this.props)
     this.headers = [
        {db:'passport',label:I18n.t('#Control'), icon:'ion-edit', width:'150px',badge:'link', value: (o) => {
          //console.log(o)  
          if(o.passport == undefined){
            return 'Not Found'
          }else{
            return o.passport.icontrol;
          }
        }},
        {db:'initial_credit',label:I18n.t('#Credit'),type:'money' , icon:'ion-edit', width:'150px',badge:'link', value: (o) => {
          //console.log(o)  
          return o.passport ? o.passport.initial_credit : 'Not Found' ;
        }},
        {db:"guest",label:I18n.t('Guest'), type:'text' , align:'left', width:'150px', value:(o,i)=>{
          let name = o.passport ? o.passport.lastname ? o.name  +' '+ o.passport.lastname : o.name : o.name
          console.log(o)
          return `${name} - ${o.email}`;
        }},    
        /*{db:"affiliated",label:I18n.t('Refer'), badge:{default:'default',null:''}, type:'text' , align:'center', width:'50px', value:(o,i)=>{
           return o.affiliated ? o.affiliated.code.toUpperCase() : 'PPASS';
        }},*/            
        {db:"nameOffer",label:I18n.t('Offer'),type:'text' , align:'center',width:'120px', value:(o,i)=>{
            return o.offerData.title ? o.offerData.title.toUpperCase() : 'N/A';
        }},            
        {db:"payment",label:I18n.t('Payment'),type:'text' , align:'center',width:'50px', value:(o,i)=>{
            return o.discPrice ? '$'+o.discPrice : 'N/A';
        }},  
        {db:"credit",label:I18n.t('Remaining'),type:'text' , align:'center',width:'50px', value:(o,i)=>{
          return o.passport ? '$'+Number(o.passport.credit)+'USD' : 'N/A';
        }},       
        /*{db:"voucher",label:I18n.t('Voucher'),type:'text' , align:'center',width:'50px', badge:'primary', value:(o,i)=>{
          return o.voucher ? o.voucher : 'NO';
        }},*/
        // {db:"start",label:I18n.t('Start'),type:'date' , align:'center',width:'100px', icon:'ion-calendar', badge:'primary'},
        // {db:"expiration",label:I18n.t('Expiration'),type:'date',align:'center',width:'100px', icon:'ion-calendar', badge:'primary' },
        /*{db:'arrival',label:I18n.t('Activity date'),type:'datetime',width:'100px', align:'center', icon:'ion-calendar', badge:'primary', value:(o)=>{
          return moment(o.arrival).format('L')
        }},*/
        {db:"checkin",label:I18n.t('Check In'),type:'date',align:'center',width:'100px', icon:'ion-calendar', badge:'primary' , value:(o)=>{
          return o.passport ? moment(o.passport.checkIn).format('L') : 'N/A (this version do not have this data)'
        }},
        {db:"checkout",label:I18n.t('Check Out'),type:'date',align:'center',width:'100px', icon:'ion-calendar', badge:'primary' , value:(o)=>{
          return o.passport ? moment(o.passport.checkOut).format('L') : 'N/A (this version do not have this data)'
        }},
        /*{db:'scheduleHour',label:I18n.t('Confirmed Schedule'),type:'time',width:'100px', align:'center', icon:'ion-time', badge:'primary', value:(o)=>{
          if(o.scheduleHour !== undefined) { 
              return o.scheduleHour + ' hrs';
          }else {
              return 'N/A';
          }
        }},*/
        {db:'cancel',label:'Cancel', icon:'ion-backspace-outline', badge:'danger-link',width:'20px', align:'center',
        value:(o)=>{
            if(!o.bookingCanceled) { 
                return 'Cancel';
            }else {
                return 'Canceled';
            }
        },onclick:(o)=>{
            if(!o.bookingCanceled) {
                window.bootbox.confirm(I18n.t('Are you sure to cancel this booking?'), (result)=>{
                if(result){
                    this.store.dispatch(cancel({code: 'cbcfa',id: o.id}));
                }
                })
            }else {
                window.bootbox.confirm(I18n.t('This booking is allready canceled!'), (result)=>{
                    if(result){
                        return true;
                    }
                })
            }
        }},
        {db:'send',label:'Notification', icon:'ion-backspace-outline', badge:'primary-link',width:'20px', align:'center',
        value:(o)=>{
          if(!o.bookingCanceled) { 
            return 'Send';
          }else{
            return 'Canceled';
          }
        },onclick:(o)=>{
          if(!o.bookingCanceled) { 
            window.bootbox.confirm(I18n.t('Are you sure to send this booking?'), (result)=>{
              if(result){
                  this.store.dispatch(print({id: o.id}));
              }
            })
          }else{
            window.bootbox.confirm(I18n.t('This booking was canceled!'), (result)=>{
                if(result){
                    return true;
                }
            })
          }
        }},
        /*{db:'scheduleHour',label:I18n.t('Schedule'), icon:'ion-edit', align:'center', width:'10px',badge:'link',
        value:(o)=>{
          //console.log(o)
          if(!o.bookingCanceled) {
            if(o.reservation != 'N/A'){
              if(o.scheduleHour){
                return 'PDF Copy';
              }else{
                return 'send';
              }
            }else {
              return 'N/A';
            }
          }else{
            return 'Canceled';
          }
        }, onclick: (o) => {
            ////console.log(this.context)
            if(o.scheduleHour == 'N/A' || o.scheduleHour == undefined) {
              if(o.reservation != 'N/A' && o.bookingCanceled != true){
                window.Light = window.YagamiBox.init({
                  title: I18n.t('Add reservation time'),
                  width:'70%',
                  height:'600px',
                  setDomContent : (container) => {
                    ////console.log('print')
                      ReactDOM.render(<AddForm data={o} context={this.context} modal={true} />, container);
                  }
                })
              }else{
                return false
              }
            }else {
              window.bootbox.confirm(I18n.t('Are you sure to send this booking?'), (result)=>{
                if(result){
                    this.store.dispatch(printConfirmation({id: o.id}));
                }
              })
            }
        }},*/
        {db:"booknote",label:I18n.t('note'), badge:{default:'danger-link', null:''}, type:'link' , align:'center', width:'50px', value:(o,i)=>{
          return 'note';
        }, onclick: (o) => {
          window.Light = window.YagamiBox.init({
            title: I18n.t('NOTE'),
            width:'60%',
            height:'300px',
            setDomContent : (container) => {
                ReactDOM.render(<Note data={o} context={this.context} modal={true} />, container);
            }
          })
        }
      }, 
      {db:'pdf',label:'pdf-BETA-print', icon:'ion-backspace-outline', badge:'primary-link',width:'20px', align:'center',
      value:(o)=>{
        return 'print-pdf'
      },
      onclick:(o)=>{
        window.Light = window.YagamiBox.init({
          title: I18n.t('PDF report local'),
          width:'60%',
          height:'600px',
          setDomContent : (container) => {
            ReactDOM.render(<PrintDirectPDF reportData={o} />, container)
          }
        })
      }},
      {db:'printed',label:'printed',type:'check', icon:'ion-backspace-outline', badge:'primary-link',width:'20px', align:'center',
      oncheck: (o, data, pages) => {
        //console.log(pages)
        //let page = this.state.affiliates.passports.pages.current_page
        this.store.dispatch(saveAprovalBooking({aproved: o.target.checked, id: data.id, code: data.affiliateCode, page: pages.current_page, rows: 5}))
      }},
      {db:'printedAgain',label:'printed_again',type:'check2', icon:'ion-backspace-outline', badge:'primary-link',width:'20px', align:'center',
      oncheck: (o, data, pages) => {
        //console.log(o.target)
        //let page = this.state.affiliates.passports.pages.current_page
        this.store.dispatch(saveAprovalBooking({aprovedAgain: o.target.checked, id: data.id, code: data.affiliateCode, page: pages.current_page, rows: 5}))
      }},
    ]

  }
  componentWillUnmount() {
    this.unsuscribe();
  }
  componentDidMount(){
    this.setState({credit: 3})
     this.unsuscribe = this.store.subscribe(() => {
        this.setState(this.store.getState());
     });
  }
  request(params){
    console.log(this.state.credit)
    params.code = 'cbcfa'
    if(this.state.credit == 1){
      params.credit = '500'
    }else if(this.state.credit == 2){
      params.credit = '1000'
    }else if(this.state.credit == 3){
      params.credit = 'all'
    }else{
      params.credit = 'all'
    }
    this.store.dispatch(loadBookings(params));
  }
  addForm(){
     window.location.href = '/posts/add'
  }
  creditResort(data){
    let params={}
    console.log(data.target.value)
    this.setState({credit: data.target.value})
    if(data.target.value == 1){
      params.credit = '500'
    }else if(data.target.value == 2){
      params.credit = '1000'
    }else if(data.target.value == 3){
      params.credit = 'all'
    }
    params.code = 'cbcfa'
    this.store.dispatch(loadBookings(params));
    this.request.bind(this)
  }
  render() {
    return (
      <div className="page" style={{margin: '0 15px'}}>
        <div className="row">
          <div className="col-md-12">
            <button ref="adduser" onClick={this.addForm.bind(this) } className="btn btn-sm btn-success float-right sr-only"> <i className="ion-plus-circled" /> <Translate value="Add"/> </button>
            <h1><i className="ion-compose"/> <Translate value="Bookings"/></h1>
            <hr className="divider" />
            <h3>Filter:</h3>
            <div className="col-md-6">
              <div class="custom-control custom-radio">
                <input type="radio" class="custom-control-input" id="500-Credit-Resort" name="credit-resort" value={1} checked={this.state.credit == 1 ? true : false } onChange={this.creditResort}/>
                <label class="custom-control-label" for="500-Credit-Resort"> $500 Credit Resort</label>
              </div>
              <div class="custom-control custom-radio">
                <input type="radio" class="custom-control-input" id="1000-Credit-Resort" name="credit-resort" value={2} checked={this.state.credit == 2 ? true : false } onChange={this.creditResort}/>
                <label class="custom-control-label" for="1000-Credit-Resort"> $1000 Credit Resort</label>
              </div>
              <div class="custom-control custom-radio">
                <input type="radio" class="custom-control-input" id="All-Credit-Resort" name="credit-resort" value={3} checked={this.state.credit == 3 ? true : false } onChange={this.creditResort}/>
                <label class="custom-control-label" for="All-Credit-Resort"> All Credit Resort</label>
              </div>
            </div>
            <br/><br/>
          </div>
        </div>
        <div className="row">
          <div className="col-md-12">
            <Table data={this.state.bookings.data} responsive={true} source={this.request.bind(this)} headers={this.headers} page={1} rows={5} placeholder={I18n.t('Search books with name - email -or- offer')} search={true} />
          </div>
        </div>
      </div>
    )
  }
}

Font.register({
  family: 'Oswald',
  src: 'https://fonts.googleapis.com/css?family=Vesper+Libre&display=swap'
});

// Create styles for pdf
const styles = StyleSheet.create({
  page: {
    flexDirection: 'column',
    backgroundColor: '#FFFFFF',
    display: 'flex'
  },
  section: {
    marginTop: 38.52,
    width: '100%',
    fontStyle: 'italic'
  },
  sectionBody: {
    flexDirection: 'row',
    marginRight: 79.60,
    marginLeft: 79.60
  },
  info: {
    marginTop: 38.52,
    marginRight: 79.60,
    marginLeft: 79.60
  },
  title: {
    fontSize:".8rem"
  }
});

// Create Document Component
export class MyDocuments extends React.Component {
  constructor(props){
    super(props)
  }
  render(){
    return(
      <Document>
        <Page size="LETTER" style={{flexDirection: 'column',display:'flex'}}>
          <View>
            <Text style={{position:'absolute', textAlign: 'center',width:'100%', fontSize: "1.1rem"}}>Booking Details <br/> Detalles de reservación</Text><br/><br/><br/>
            <Text style={{position:'absolute', float: 'left', fontSize: ".9rem"}}>Activity / Actividad: <b><i style={styles.title}>{this.props.reportData.offerData.title}</i></b><br/>Value / Valor: <b>${this.props.reportData.discPrice} USD</b></Text>
          </View>
          <View>
            <Text style={{float: 'right', fontSize: ".9rem"}}>Date / Fecha: <b><i style={styles.title}>{moment(this.props.reportData.arrival).format('L')}</i></b><br/>
            Guest / Huésped: <b>{this.props.reportData.passport.name} {this.props.reportData.passport.lastname}</b> <br/>
            {this.props.reportData.reservation != 'N/A' ? `CONFIRMATION TIME / HORA DE CONFIRMACIÓN: <b>${this.props.reportData.scheduleHour} hrs.</b><br/>` : ``}
            # control: <b>{this.props.reportData.passport.icontrol}</b></Text>
          </View>
          <View><br/><br/><br/>
            <Text style={{fontSize: ".6rem", textAlign:'center', position:'absolute',}}>{this.props.reportData.offerData.emailenglish ? this.props.reportData.offerData.emailenglish : `NEED HELP? / ¿NECESITAS AYUDA? please contact Guest Service office / Por favor contacte la oficina de atención al Huésped.`} <br/>{this.props.reportData.offerData.emailspanish ? this.props.reportData.offerData.emailspanish : ``}</Text>
          </View>
        </Page>
      </Document>
  )}
}

const Titletext = styled.Text`
  font-family: 'Times-Roman';
  text-align: center; 
  font-size: 13pt;
  font-weight: 900;
`;

const Offertext = styled.Text`
  font-family: 'Times-Roman';
  font-weight: bold;
  font-size: 11pt;
`;

export class MyDocument extends React.Component {
  constructor(props){
    super(props)

  }
  render(){
    ////console.log(this.props)
    
    /*<View style={styles.sectionBody}>
    <Text style={{ textAlign: 'right', fontSize: 9, top:'10px', width: '100%', paddingLeft:'10px', display:'none' }}>{this.props.reportData.reservation != 'N/A' ? `CONFIRMATION TIME / HORA DE CONFIRMACIÓN: ${this.props.reportData.scheduleHour} hrs.` : ``}</Text>
  </View>*/
    return (<Document>
      <Page size="LETTER" style={styles.page}>
        <View style={styles.section} >
          <Titletext>Booking Details</Titletext>
          <Text style={{textAlign: 'center', fontSize: 10}}>Detalles de reservación</Text>  
        </View>
        <View style={styles.sectionBody}>
          <Text style={{ textAlign: 'left', fontSize: 9, top:'10px', width: '50%', paddingRight:'10px'}}>Activity / Actividad: <Offertext>{this.props.reportData.offerData.title}</Offertext></Text>
          <Text style={{ textAlign: 'right', fontSize: 9, top:'10px', width: '50%', paddingLeft:'10px' }}>Valid / Valido  From / Del: {moment(this.props.reportData.passport.checkIn).format('L')} - To/ Hasta: {moment(this.props.reportData.passport.checkOut).format('L')} </Text>
        </View>
        <View style={styles.sectionBody}>
          <Text style={{ textAlign: 'left', fontSize: 9, top:'10px', width: '50%', paddingRight:'10px' }}>Value / Valor: <Offertext>${this.props.reportData.discPrice} USD</Offertext></Text>
          <Text style={{ textAlign: 'right', fontSize: 9, top:'10px', width: '50%', paddingLeft:'10px' }}>Guest / Huésped: {this.props.reportData.passport.name} {this.props.reportData.passport.lastname}</Text>
        </View>
        <View style={styles.sectionBody}>
          <Text style={{ textAlign: 'right', fontSize: 9, top:'10px', width: '100%', paddingLeft:'10px' }}># control: {this.props.reportData.passport.icontrol}</Text>
        </View>
        <View style={styles.info} >
          <Text style={{textAlign: 'center', fontSize: 8}}>{this.props.reportData.offerData.emailenglish ? this.props.reportData.offerData.emailenglish : `NEED HELP? / ¿NECESITAS AYUDA? ${'\n'} please contact Guest Service office / Por favor contacte la oficina de atención al Huésped.`} {'\n'}  {this.props.reportData.offerData.emailspanish ? this.props.reportData.offerData.emailspanish : ``}</Text> 
        </View>
      </Page>
    </Document>)
  }
}

const PrintPDF = props => (
  <div>
    <MyDocuments reportData={props.reportData}/><br/>
    <BlobProvider document={<MyDocument reportData={props.reportData} />} fileName={'pdfdocument.pdf'}>
        {({ blob, url, loading, error }) => {return <a type='button' className='btn btn-primary' style={{position:'absolute', top:'55%',left:'25px'}} href={url}>Download PDF</a>}}
    </BlobProvider  >
  </div>
)

const Checkbox = props => (
  <input type="checkbox" className="custom-control-input" />
)

const PrintDirectPDF = props => (
  <div>
    <div className="custom-control custom-checkbox sr-only" style={{marginLeft: `15px`}}>
      <label>
        <Checkbox />
        <span className="custom-control-label">Already Printed</span>
      </label>
    </div>
    <BlobProvider document={<MyDocument reportData={props.reportData} />} fileName={'pdfdocument.pdf'}>
        {({ blob, url, loading, error }) => {return <embed src={url} width="100%" height="500px" type="application/pdf"></embed>}}
    </BlobProvider  >
  </div>
)

export default Bookings;
