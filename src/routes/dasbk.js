/*eslint-disable*/
import React     from 'react';
import ReactDOM  from 'react-dom';
import PropTypes from 'prop-types';
// import moment    from 'moment';



//Actions
import {
  loadPassports,
  remove
} from '../actions/passports'; '../actions/devices';

//Components
import Table      from '../components/Table';
import {I18n,Translate, Localize} from 'react-redux-i18n';

/*eslint-enable*/

const chartLine = ( ) =>{

   var randomScalingFactor = function() {
            return Math.round(Math.random() * 100);
        };

        var datapoints = [0, 20, 20, 60, 60, 120, NaN, 180, 120, 125, 105, 110, 170];
    var config = {
            type: 'line',
            data: {
                labels: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
                datasets: [{
                    label: "Cubic interpolation (monotone)",
                    data: datapoints,
                    borderColor: window.chartColors.red,
          backgroundColor: 'rgba(0, 0, 0, 0)',
                    fill: false,
          cubicInterpolationMode: 'monotone'
                }, {
                    label: "Cubic interpolation (default)",
                    data: datapoints,
                    borderColor: window.chartColors.blue,
          backgroundColor: 'rgba(0, 0, 0, 0)',
                    fill: false,
                }, {
                    label: "Linear interpolation",
                    data: datapoints,
                    borderColor: window.chartColors.green,
          backgroundColor: 'rgba(0, 0, 0, 0)',
                    fill: false,
          lineTension: 0
                }]
            },
            options: {
                responsive: true,
                title:{
                    display:true,
                    text:'Chart.js Line Chart - Cubic interpolation mode'
                },
                tooltips: {
                    mode: 'index'
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Value'
                        },
                        ticks: {
                            suggestedMin: -10,
                            suggestedMax: 200,
                        }
                    }]
                }
            }
        };



     var ctx = document.getElementById("chart-line").getContext("2d");
    window.myLine = new window.Chart(ctx, config);

}
class Dashboard extends React.Component {
  static get contextTypes() {
    return {
      router: PropTypes.object.isRequired,
      store: PropTypes.object.isRequired,
      socket: PropTypes.object.isRequired
    }
  }
  constructor(props,context){
    super(props,context);
    this.context   = context;
    this.store     = this.context.store;
    this.state     = this.store.getState();
     this.socket   = this.context.socket;

     this.headers = [
        {db:'stoken',label:I18n.t('Token'), icon:'ion-edit', width:'150px',badge:'link', onclick: (o) => {
            // window.location.href = '/passports/update/'+o.id;
        }},
        {db:"name",label:I18n.t('Name'), type:'text' , align:'left', width:'150px', value:(o,i)=>{
          return `${o.name} - ${o.email}`;
        }},     
        {db:"affiliated",label:I18n.t('Affiliated'), badge:{default:'default',null:''}, type:'text' , align:'left', width:'150px', value:(o,i)=>{
          return o.affiliated ? o.affiliated : 'PPASS';
        }},     
        {db:"status",label:I18n.t('Payment'),type:'text' , align:'center',width:'50px', badge:{default:'default',completed:'success',pending:'danger'}, value:(o,i)=>{
            return o.notify === 1 ? 'Completed' : 'Pending';
        }},            
        {db:"notify",label:I18n.t('Notified'),type:'text' , align:'center',width:'50px', badge:{default:'default',done:'success',pending:'danger'}, value:(o,i)=>{
            return o.notify === 1 ? 'Done' : 'Pending';
        }},        
        {db:"voucher",label:I18n.t('Voucher'),type:'text' , align:'center',width:'50px', badge:'primary', value:(o,i)=>{
          return o.voucher ? o.voucher : 'NO';
        }},
        {db:"start",label:I18n.t('Start'),type:'date' , align:'center',width:'100px', icon:'ion-calendar', badge:'primary'},
        {db:"expiration",label:I18n.t('Expiration'),type:'date',align:'center',width:'100px', icon:'ion-calendar', badge:'primary' },
        {db:'createdAt',label:I18n.t('Created at'),type:'datetime',width:'100px', align:'center', icon:'ion-calendar', badge:'primary'}
    ]

     this.chart = {

        type:'dougnut',
        doughnutHoleSize:0.9,
        data :{
          "Classical music": 10,
          "Alternative rock": 14,
          "Pop": 2,
          "Jazz": 12
        }
    }
  }

  componentWillUnmount() {
    this.unsuscribe();
  }
  componentDidMount(){
     this.unsuscribe = this.store.subscribe(() => {
        this.setState(this.store.getState());
     });

     this.chartPie();

     chartLine();
  }

  chartPie(){

  window.Chart.pluginService.register({
  beforeDraw: function(chart) {
    var width = chart.chart.width,
        height = chart.chart.height,
        ctx = chart.chart.ctx,
        type = chart.config.type;

    if (type === 'doughnut'){
      var percent = Math.round((chart.config.data.datasets[0].data[1] * 100) /
                    (chart.config.data.datasets[0].data[0] +
                    chart.config.data.datasets[0].data[1]));
      var oldFill = ctx.fillStyle;
     var fontSize = ((height - chart.chartArea.top) / 80).toFixed(2);

      ctx.restore();
      ctx.font =   fontSize+"em 'Bungee Inline', cursive";
      ctx.textBaseline = "middle"

      var text = percent + "%",
          textX = Math.round((width - ctx.measureText(text).width) / 2),
          textY = (height + chart.chartArea.top) / 2;

      ctx.fillStyle = chart.config.data.datasets[0].backgroundColor[1]
      ctx.fillText(text, textX, textY);
      ctx.fillStyle = oldFill;
      ctx.save();
    }
  }
});


    window.chartColors = {
    red: 'rgb(221,44,0)',
    orange: 'rgb(255, 159, 64)',
    yellow: 'rgb(255, 205, 86)',
    green: 'rgb(0,150,136)',
    blue: 'rgb(54, 162, 235)',
    purple: 'rgb(153, 102, 255)',
    grey: 'rgb(201, 203, 207)'
  };

    var randomScalingFactor = function() {
        return Math.round(Math.random() * 100);
    };

    var config = {
        type: 'doughnut',
        data: {
            datasets: [{
                data: [
                    randomScalingFactor(),
                    randomScalingFactor()
                ],
                backgroundColor: [
                    window.chartColors.red,
                    window.chartColors.green,
                ],
                label: 'Dataset 1'
            }],
            labels: [
                I18n.t('inactive'),
                I18n.t('active')
            ]
        },
        options: {
          cutoutPercentage:80,
          responsive: true
        }
    };

    var ctx = this.refs['chart-area'].getContext("2d");
    window.myPie = new window.Chart(ctx, config);
  }

  setMap(){
    let uluru = {lat:19.432608, lng:-99.133209};
    let maps   = new window.google.maps.Map(this.refs['map'], {
      zoom  :11,
      center:uluru,
      styles:[{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#1166ca"},{"visibility":"on"}]}]
    });

    new window.google.maps.Marker({
      position: uluru,
      map: maps
    });
  }
 
  request(params){
    this.store.dispatch(loadPassports(params));
  }
  render(){
    return (
      <div className="page container">
         <div className="float-right menu-btns hide">
            
         </div>
          <h1> <i className="ion-clock"/> <Translate value="Dashboard"/> </h1>
          <hr className="divider" />

          <div className="row hide">
              <div className="col-md-4">
                <div className="card">
                  <div className="card-header">
                    <Translate value="Passports activity"/>
                  </div>
                  <div className="card-body">
                    <canvas ref="chart-area" id="chart-area" />
                  </div>
                </div>
              </div>
              <div className="col-md-8">

                <div className="card">
                  <div className="card-header">
                    <Translate value="Activity "/>
                  </div>
                  <div className="card-body ">
                    <canvas ref="chart-line" id="chart-line" />
                  </div>
                </div>
              </div>
          </div>
          <hr className="divider-30" />
          <div className="row">
            <div className="col-md-12">
              <div className="row">
                  <div className="card-body">
                      <Table data={this.state.passports.data} source={this.request.bind(this)} headers={this.headers} page={1} rows={5} placeholder={I18n.t('Search Passport')} search={true} />
                  </div>
                </div>
            </div>
          </div>
          <hr className="divider-30" />

          <hr className="divider-30" />
      </div>
    )
  }
}

export default Dashboard;
