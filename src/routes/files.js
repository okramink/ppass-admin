import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import {I18n,Translate} from 'react-redux-i18n';

//Actions
import {
  loadFiles,
  remove
} from '../actions/files';

//Components
import Table     from '../components/Table';
import AddForm   from '../components/files/Form';
import ViewMedia from '../components/files/View';

class Files extends React.Component {
  static get contextTypes() {
    return {
        router: PropTypes.object.isRequired,
        store: PropTypes.object.isRequired
    }
  }
  constructor(props,context){
    super(props,context);
    this.context = context;
    this.store   = this.context.store;
    this.state   = this.store.getState();

    this.headers = [        
        {db:'id',label:I18n.t('Preview'), width:'60px', align:'center', type:'media', value:(o,i)=>{        
          return '<div class="thumb-preview"><img src="'+o.thumb+'"/></item>';
        },
        onclick: (o) => {
            window.Light = window.YagamiBox.init({
                title: I18n.t('Add file'),
                width:'55%',
                height:'auto',
                showTitle:false,
                background:'#000',
                setDomContent : (container) => {                    
                  ReactDOM.render(<ViewMedia data={o} context={this.context} modal={true} src={o.src} type={o.type} />, container);
                }
            })
         }
        }, 
        {db:"name",label:I18n.t('Name'),width:'160px', type:'text' , align:'left',value:(o,i)=>{

          return o.name ? o.name : o.filename;
        }},       
        {db:"type",label:I18n.t('Type'),width:'160px', type:'text' , align:'left'},
        {db:"size",label:I18n.t('Size'),width:'160px', type:'text' , align:'center',value:(o,i)=>{
        	return this.size(o.size);
        }},                
        {db:'createdAt',label:I18n.t('Created'), icon:'ion-calendar', badge:'link',type:'datetime',width:'150px', align:'center'},
        {db:'updatedAt',label:' ', icon:'ion-backspace-outline', badge:'danger-link',width:'20px', align:'center',
        value:(o)=>{
            return ' Eliminar';
        },
        onclick:(o)=>{
            window.bootbox.confirm(I18n.t('Are you sure to delete this user?') ,(result)=>{
	            if(result){
	                this.store.dispatch(remove({id:o.id}));
	            }
            })
        }}
    ]
  }
  size(bytes){
     
   if(bytes === 0) return '0 Bytes';
   var k = 1000,
       dm = 2,
       sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
       i = Math.floor(Math.log(bytes) / Math.log(k));
      return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
  }
  componentWillUnmount() {
    this.unsuscribe();
  }
  componentDidMount(){
     this.unsuscribe = this.store.subscribe(() => {
        this.setState(this.store.getState());
     });
  }
  request(data){
    this.tableFilesData = data;
    //this.store.dispatch(loadFiles(data));
  }
  addFile(){
  	window.Light = window.YagamiBox.init({
        title: I18n.t('Add file'),
        width:'70%',
        height:'600px',
        setDomContent : (container) => {
            ReactDOM.render(<AddForm context={this.context} modal={true} filesData={this.tableFilesData} />, container);
        }
    })
  }
  render() {
    return (
       <div className="page container">
        <div className="row">
          <div className="col-md-12">
            <button ref="addFile" onClick={this.addFile.bind(this) } className="btn btn-sm btn-success float-right"> <i className="ion-plus-circled" /> <Translate value="Add"/> </button>
            <h1><i className="ion-navicon-round"/> <Translate value="Files"/></h1>
           <hr className="divider" />
            
          </div>
        </div>
        <div className="row">
          <div className="col-md-12">
            <Table data={this.state.files.table} source={this.request.bind(this)} headers={this.headers} page={1} rows={20} placeholder={I18n.t('Search files')} search={true} />
          </div>
          
        </div>
      </div>
    )
  }
}
export default Files;

