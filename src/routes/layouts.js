import React    from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import {I18n, Translate} from 'react-redux-i18n';
import {NavLink}  from 'react-router-dom';

//Actions

//Components
import AddForm from '../components/users/AddForm';

class Components extends React.Component {

  render(){
    return(
      <div className="container-fluid">

        <div className="row">
          
          <div className="col-md-6">
            <div className="card">
            <div className="card-body">
              <h4 className="card-title">Special title treatment</h4>
              <p className="card-text">With supporting text below as a natural lead-in to additional content.</p>
              
            </div>
          </div>
          </div>
          <div className="col-md-6">
            <div className="card">
              <div className="card-body">
                <h4 className="card-title">Special title treatment</h4>
                <p className="card-text">With supporting text below as a natural lead-in to additional content.</p>
                
              </div>
            </div>
          </div>
        </div>        
      </div>
    )
  }
}


class Tools extends React.Component {

  onZoomChange(e){
    e.preventDefault()
    this.refs.label_zoom.value = e.target.value + '%';
    this.props.setZoom(e.target.value);
  }
  render(){
    return(
       <ul className="nav navbar-light bg-light nav-tools">
        <li className="nav-item">
          <NavLink className="nav-link active" to="#"><i className="ion-ios-plus" /> New</NavLink>
        </li>                
        <li className="nav-item">
          <NavLink className="nav-link" to="#" onClick={this.props.setFullScreen }><i className="ion-ios-monitor-outline" /> Fullscreen</NavLink>
        </li>
        <li className="nav-item">
          <NavLink className="nav-link" to="#"><i className="ion-ios-upload" /> Save</NavLink>
        </li>
         <li className="nav-item">          
            <form onInput={this.onZoomChange.bind(this)}>
              <input type="range" ref="input_zoom" defaultValue="50" /> 
              <output ref="label_zoom">50%</output>
            </form>
        </li>
      </ul>
    )
  }
}



class Workspace extends React.Component {
  constructor(props) {
    super(props);
    
    this.state = {
      zoom : 50
    }
  }

  componentDidMount() {    
    
    this.refs.content.style.width = ( this.props.wwidth - ( this.props.wmenu + 1 )) + 'px';

    this.refs.workspace.style.width  = '1920px';
    this.refs.workspace.style.height = '1080px';
    this.refs.workspace.style.zoom   = '50%';

    document.addEventListener('drop', function(e) {
      e.preventDefault();
      e.stopPropagation();
    });
    document.addEventListener('dragover', function(e) {
      e.preventDefault();
      e.stopPropagation();
    });

    document.addEventListener('webkitfullscreenchange', (e) => {
      if( !document.webkitIsFullScreen ){
        this.refs.workspace.style.zoom   = '50%';
      }else{           
        this.refs.workspace.style.zoom   = '100%';
      }

    }, false);    

  }
  dragLeave(e){
  }
  dragStart(e){
    this.target = e.target;
    e.dataTransfer.setData("application/json", JSON.stringify([(e.offsetX || e.clientX - e.target.offsetLeft),(e.offsetY || e.clientY - e.target.offsetTop)]));
  }
  dragEnd(e){

  }
  dragOver(e){
    e.preventDefault();

  }
  drop(e){    
    e.preventDefault();
    if( e.target.classList.contains('workspace') ){
      var data = JSON.parse(e.dataTransfer.getData("application/json"));
      e.target.appendChild(this.target);
      this.target.style.left = e.clientX  - data[0] + 'px';
      this.target.style.top  = e.clientY  - data[1] + 'px';
    }
    return false;
  }
  ondblclick(){
    alert('ok')
  }
  setFullScreen(){
    const element = this.refs.workspace;
    element.webkitRequestFullscreen()
  }
  setZoom(value){
    this.refs.workspace.style.zoom   = value + '%';
  }
  render(){
    return(
      <div className="content " ref="content">
        <Tools setZoom={this.setZoom.bind(this)}/>
        <div className="workspace" ref="workspace" onDragOver={this.dragOver.bind(this)}  onDrop={this.drop.bind(this)} >       
            <div draggable="true" className="ui-component" onDragStart={this.dragStart.bind(this)}  onDoubleClick={this.ondblclick.bind(this)} > content 1 </div>
            <div draggable="true" className="ui-component" onDragStart={this.dragStart.bind(this)}  onDoubleClick={this.ondblclick.bind(this)} > content 2 </div>          
        </div>
      </div>
    )
  }
}

//Users routes
class Layouts extends React.Component {

  static get contextTypes() {
    return {
        router: PropTypes.object.isRequired,
        store: PropTypes.object.isRequired
    }
  }
  constructor(props,context){
    super(props,context);
    this.context = context;
    this.store   = this.context.store;
    this.state   = this.store.getState();
  }
  componentWillUnmount() {
    this.unsuscribe();    
  }
  componentWillMount() {
    this.windowSize();
  }
  componentDidMount(){
     this.unsuscribe = this.store.subscribe(() => {
        this.setState(this.store.getState());
     });    
     this.resizeLayout();
     window.addEventListener("resize", this.resizeLayout.bind(this), false);
  }  
  windowSize(){
    this.wwidth  = window.innerWidth;
    this.wheight = window.innerHeight;
    this.wmenu = 300;
  }
  resizeLayout(){    
    this.refs.page.style.width  = ( this.wwidth  ) + 'px';
    this.refs.page.style.height = ( this.wheight  ) + 'px';
    this.refs.menu.style.width    = ( this.wmenu ) + 'px';
  }

  addForm(){
    window.Light = window.YagamiBox.init({
        title: I18n.t('Add user') ,
        width:'70%',
        height:'600px',
        setDomContent : function(container){
            ReactDOM.render(<AddForm  context={this.context} modal={true} />, container);
        }.bind(this)
    })
  }

  render() {
    return (
      <div className="page container-fluid " ref="page">        
        <div className="row fh">
          <div className="full-menu fh" ref="menu">
            <Translate value="Layouts"/>
            <Components />
          </div>
          <div className="full-content fh" ref="content"> 
              <Workspace wwidth={this.wwidth} wheight={this.wheight} wmenu={this.wmenu} />
          </div>
        </div>
      </div>
    )
  }
}

export default Layouts;