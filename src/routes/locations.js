import React from 'react';
import {Translate} from 'react-redux-i18n';

class Locations extends React.Component {
  render() {
    return (
      <div className="page container">
            <h1><i className="ion-ios-location-outline"/> <Translate value="Locations"/></h1>       
      </div>
    )
  }
}
export default Locations;

