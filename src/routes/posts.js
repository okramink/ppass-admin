import React, { Component } from 'react';
// import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import {I18n, Translate} from 'react-redux-i18n';

//Actions
import { loadPosts, remove } from '../actions/posts';

//Components
import Table from '../components/Table';


class Posts extends Component {
  static contextTypes = {
    router: PropTypes.object.isRequired,
    store:  PropTypes.object.isRequired,
  };

  constructor(props,context){
    super(props,context);
    this.context = context;
    this.store   = this.context.store;
    this.state   = this.store.getState();

    this.headers = [
        {db:'title',label:I18n.t('Title'), icon:'ion-edit', onclick: (o) => {
            window.location.href = '/posts/update/'+o.id;
        }},
        {db:"author",label:I18n.t('Author'),width:'130px', type:'text' , align:'right',
          value:(o) => {
            if (o.author) return o.author.name;
            return 'PPass Admin';
        }},
        {db:"thestatus",label:I18n.t('Status'),width:'100px',type:'text', icon:'ion-flag',badge:{default:'default',activo:'success',inactivo:'danger'}, align:'right',
        value:(o) => {
            return o.thestatus.toUpperCase() || '';
        }},
        {db:'updatedAt',label:I18n.t('Updated'), icon:'ion-calendar', badge:'link',type:'datetime',width:'150px', align:'center'},
        {db:'createdAt',label:' ', icon:'ion-backspace-outline', badge:'danger-link',width:'20px', align:'center',
        value:(o) => {
            return 'Eliminar';
        },
        onclick:(o) => {
            window.bootbox.confirm(I18n.t('Are you sure to delete this post?') ,(result)=>{
                if (result) this.store.dispatch(remove({id:o.id}));
            });
        }}
    ];
  }

  componentWillUnmount() {
    this.unsuscribe();
  }

  componentDidMount(){
    this.unsuscribe = this.store.subscribe(() => {
        this.setState(this.store.getState());
    });
  }

  request(params){
    this.store.dispatch(loadPosts(params));
  }

  addForm(){
     this.context.router.history.push('/posts/add')
  }

  render() {
    return (
      <div className="page container">
        <div className="row">
          <div className="col-md-12">
              <button ref="adduser" onClick={this.addForm.bind(this) } className="btn btn-sm btn-success float-right"> <i className="ion-plus-circled" /> <Translate value="Add"/> </button>
            <h1><i className="ion-compose"/> <Translate value="Posts"/></h1>
            <hr className="divider" />
          </div>
        </div>
        <div className="row">
          <div className="col-md-12">
            <Table data={this.state.posts.data} source={this.request.bind(this)} headers={this.headers} page={1} rows={10} placeholder={I18n.t('Search posts')} search={true} />
          </div>
        </div>
      </div>
    );
  }
}

export default Posts;

