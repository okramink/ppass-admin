import React from 'react';
//import Table from '../components/Table';
import {Translate} from 'react-redux-i18n';
class Profile extends React.Component {
  render() {
    return (
       <div className="page container">
        <div className="row">
          <div className="col-md-12">
            <h1><Translate value="Profile"/></h1>
            <hr />
          </div>
        </div>
        <div className="row">          
          <div className="col-md-4">
            <div className="list-group">            
              <a to="#" role="button" className="list-group-item list-group-item-action active"> <Translate value="General info"/> </a>
              <a to="#" role="button" className="list-group-item list-group-item-action "> <Translate value="Update password"/>  </a>
              <a to="#" role="button" className="list-group-item list-group-item-action "> <Translate value="Image profile"/>  </a>
            </div>
          </div>           
          <div className="offset-md-1 col-md-7">
           <div className="card">
              <div className="card-header">
                <Translate value="General Information"/>
              </div>
              <div className="card-block">
                 <form >
                  <div className="row">
                    <div className="col-md-12">
                      <div className="form-group">
                        <label><Translate value="Language"/> :</label>
                        <select className="form-control" type="text" ref="language" >
                         <option value="es">Spanish</option>
                         <option value="en">English</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-12">
                      <div className="form-group">
                        <label><Translate value="Name"/> :</label>
                        <input className="form-control" type="text" ref="name" required/>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-12">
                      <div className="form-group">
                        <label><Translate value="Email"/>:</label>
                        <input className="form-control" type="email" ref="email" required/>
                      </div>
                    </div>
                  </div>
                 
                   <div className="row">
                    <div className="col-md-12">
                      <div className="form-group">
                        <label><Translate value="Phone"/>:</label>
                        <input className="form-control" type="text" ref="phone" />
                      </div>
                    </div>
                  </div>
                
                 <div className="row">
                    <div className="col-md-12">
                      <div className="form-group">
                        <label><Translate value="Description"/>:</label>                        
                        <textarea className="form-control" rows="7" />
                      </div>
                    </div>
                  </div>

                </form>

              </div>
            </div>

          </div>

        </div>
      </div>
    )
  }
}
export default Profile;

