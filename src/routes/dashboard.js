/*eslint-disable*/
import React     from 'react';
import ReactDOM  from 'react-dom';
import PropTypes from 'prop-types';

//Actions
import {
  loadPassports,
  remove
} from '../actions/passports'; '../actions/devices';

//Components
import Table      from '../components/Table';
import {I18n,Translate, Localize} from 'react-redux-i18n';

/*eslint-enable*/

class Dashboard extends React.Component {
  static get contextTypes() {
    return {
      router: PropTypes.object.isRequired,
      store: PropTypes.object.isRequired,
      socket: PropTypes.object.isRequired
    }
  }
  constructor(props,context){
    super(props,context);
    this.context   = context;
    this.store     = this.context.store;
    this.state     = this.store.getState();
     this.socket   = this.context.socket;
     this.headers = [
        {db:'stoken', label:I18n.t('Token'), icon:'ion-edit', width:'150px',badge:'link', onclick: (o) => {
            // window.location.href = '/passports/update/'+o.id;
        }},
        {db:"name",label:I18n.t('Name'), type:'text' , align:'left', width:'150px', value:(o,i)=>{
          return `${o.name} - ${o.email}`;
        }},     
        {db:"affiliated",label:I18n.t('Refer'), badge:{default:'default',null:''}, type:'text' , align:'center', width:'50px', value:(o,i)=>{
          return o.affiliated ? o.affiliated.code.toUpperCase() : 'PPASS';
        }},     
        {db:"status",label:I18n.t('Payment'),type:'text' , align:'center',width:'50px', badge:{default:'default',completed:'success',pending:'danger'}, value:(o,i)=>{
            return o.notify === 1 ? 'Completed' : 'Pending';
        }},            
        {db:"notify",label:I18n.t('Notified'),type:'text' , align:'center',width:'50px', badge:{default:'default',done:'success',pending:'danger'}, value:(o,i)=>{
            return o.notify === 1 ? 'Done' : 'Pending';
        }},        
        {db:"voucher",label:I18n.t('Voucher'),type:'text' , align:'center',width:'50px', badge:'primary', value:(o,i)=>{
          return o.voucher ? o.voucher : 'NO';
        }},
        {db:'createdAt',label:I18n.t('Created at'),type:'datetime',width:'100px', align:'center', icon:'ion-calendar', badge:'primary'}
    ]   
  }
  componentWillUnmount() {
    this.unsuscribe();
  }
  componentDidMount(){
     this.unsuscribe = this.store.subscribe(() => {
        this.setState(this.store.getState());
     });
  }
  request(params){
    this.store.dispatch(loadPassports(params));
  }
  render(){
    return (
      <div className="page container">
        
          <h1> <i className="ion-clock"/> <Translate value="Dashboard"/> </h1>
          <hr className="divider" />
          <hr className="divider-30" />
          <div className="row">
            <div className="col-md-12">
              <div className="row">
                  <div className="card-body">
                      <Table data={this.state.passports.data} source={this.request.bind(this)} headers={this.headers} page={1} rows={5} placeholder={I18n.t('Search Passport')} search={true} />
                  </div>
                </div>
            </div>
          </div>
          <hr className="divider-30" />

          <hr className="divider-30" />
      </div>
    )
  }
}

export default Dashboard;
