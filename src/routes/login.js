import React from 'react';
import PropTypes from 'prop-types';
import jwtDecode from 'jwt-decode';

import {
  start,
  set
} from '../actions/auth';

import {I18n, setLocale, Translate} from 'react-redux-i18n';



class Form extends React.Component{
  static get contextTypes() {
      return {
        router: PropTypes.object.isRequired,
        store: PropTypes.object.isRequired
      }
  }
  constructor(props){
    super(props);
    this.context = this.props.context;
    this.store   = this.context.store;
  }
  componentDidMount(){
    sessionStorage.removeItem('isAuthenticated');
    sessionStorage.removeItem('appUser');
    sessionStorage.removeItem('response_user')
    window.localStorage.removeItem('appToken');

  }
  displayError(){
    this.refs.msgError.style.display = 'block';
    this.refs.msgError.className     = 'alert alert-danger';
    this.refs.msgErrorText.innerHTML = 'Algo ha salido mal intenta de nuevo';
    this.refs.email.disabled         = false;
    this.refs.password.disabled      = false;
    this.refs.btnSubmit.disabled     = false;
  }
  sendForm(e){
    e.preventDefault();

     this.refs.email.disabled         = true;
     this.refs.password.disabled      = true;
     this.refs.btnSubmit.disabled     = true;

     this.store.dispatch(start(
       { email    :this.refs.email.value,
         password :this.refs.password.value
     }))
    .then(response => {
      console.log(response)
      sessionStorage.setItem('response_user', response.data.role);
        if( response.code === 0){

          const user = jwtDecode(response.token);
          const lang = user.lang || ( window.localStorage.getItem('lang') || 'en' ) ;
          this.store.dispatch(set(user));
          this.refs.msgError.style.display = 'block';
          this.refs.msgError.className     = 'alert alert-success';
          this.refs.msgErrorText.innerHTML = 'Well done, Inicio de sessión correcto, redireccionando!';
          window.localStorage.setItem('appToken',  (response.token));
          window.localStorage.setItem('lang', lang);
          sessionStorage.setItem('appToken', (response.token));
          sessionStorage.setItem('appUser', JSON.stringify(user));
          sessionStorage.setItem('isAuthenticated',true);
          this.store.dispatch(setLocale(lang))

          setTimeout(() => {
            this.context.router.history.push('/');
          },1000);
        }
        if(response.error){
          this.displayError();
        }
    })
    .catch(e =>{
      this.displayError();
    });
  }
  render(){
    return(
       <div className="fh form-container">
          <div className="logo-container">
            <img className="logo" src="../assets/imgs/PPLogo-circle_white.svg" alt="Preference Pass Admin" />
            <h2 className="form-signin-heading">  Preference Pass admin </h2>
          </div>          

          <form className="form-signin" onSubmit={this.sendForm.bind(this)}>            
            
            <div className="form-group">
              <label  className="sr-only">Email address</label>
              <input type="email" ref="email" className="form-control" placeholder={I18n.t('Email address')} required autoFocus />
            </div>
            <div className="form-group">
              <label className="sr-only">Password</label>
              <input type="password" ref="password" className="form-control" placeholder={I18n.t('Password')} required />
            </div>
            <div className="checkbox hide">
              <label>
                <input type="checkbox" value="remember-me" /> Remember me
              </label>
            </div>
            <button className="btn  btn-primary btn-block btn-register" type="submit" ref="btnSubmit">  <Translate value="Start"/> <i className="ion-log-in" /> </button>
          <div className="col-md-12 ">
            <div className="hide" role="alert" ref="msgError">
                <span ref="msgErrorText">Change a few things up and try submitting again.</span>
            </div>
          </div>
          </form>
      </div>      
    )
  }
}

class Login extends React.Component {
  static get contextTypes() {
      return {
        router: PropTypes.object.isRequired,
        store: PropTypes.object.isRequired
      }
  }
  constructor(props,context){
    super(props,context);
    this.context = context;
  }
 
  render() {
    return (
      <Form context={this.context}  />
    )
  }
}

export default Login;