import React from 'react';

class Empty extends React.Component {
  render() {
    return (
      <div>
        <nav className="navbar navbar-toggleable-md navbar-light bg-faded  fixed-top">
            <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <a className="navbar-brand" href="/">
               <img src="/assets/imgs/bootstrap-solid.svg" width="30" height="30" className="d-inline-block align-top" alt=""/> Evpay
            </a>
              <div className="collapse navbar-collapse justify-content-end" id="navbarCollapse">
                <ul className="navbar-nav mr-auto">
                </ul>
            </div>
          </nav>

          <div className="container">
            <this.props.component {...this.props} />
        </div>
      </div>
    )
  }
}
export default Empty;

