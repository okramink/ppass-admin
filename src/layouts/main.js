import React      from 'react';
import PropTypes  from 'prop-types';
import {NavLink}  from 'react-router-dom';


//Components
import {Translate, setLocale} from 'react-redux-i18n';

class Main extends React.Component {
 static get contextTypes() {
    return {
      router: PropTypes.object.isRequired,
      store: PropTypes.object.isRequired
    }
  }
 constructor(props, context) {
    super(props, context);
    this.isAuthenticated = sessionStorage.getItem('isAuthenticated');
    this.store  = context.store;
    this.state  = this.store.getState();
  }
  componentDidMount(){
    document.body.className = "";
     this.unsuscribe = this.store.subscribe(() => {
        this.setState(this.store.getState());
    });
  }
  componentWillUnmount() {
    this.unsuscribe();
  }
  onChangeLang = (e) => {
    this.store.dispatch(setLocale(e.target.dataset.lang))
    window.localStorage.setItem('lang', e.target.dataset.lang);
  }
  render(){
    //console.log(this.state)
    if( this.state.auth.user && this.state.auth.user.role == 'operator-adds' ) {
      return (
        <div className="layout">

        <nav className="navbar navbar-expand-lg navbar-inverse navbar-dark bg-fade navbar-theme fixed-top">
          <div className="container">
            <NavLink exact={true} className="navbar-brand " to="#">
              <img className="logo" src=" /assets/imgs/PPLogo-circle_white.svg" alt="PPaSS" /> Preference Pass admin
            </NavLink>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="navbarSupportedContent">
              <ul className="navbar-nav mr-auto">
                <li className="nav-item">
                <NavLink className="nav-link" to="/affiliates"><i className="ion-gear-b"/> <Translate value="Affiliates"/></NavLink>
                </li> 
              </ul>
              <ul className="navbar-nav ">

              <li className="nav-item lang-item">
                <a to="#" role="button" className="nav-link" data-lang="en" onClick={this.onChangeLang.bind(this)}  >EN</a>
                <a to="#" role="button" className="nav-link" data-lang="es" onClick={this.onChangeLang.bind(this)}  >ES</a>
              </li>
              <li className="nav-item dropdown">
                  <a className="nav-link dropdown-toggle" href="/profile" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    <img alt="user avatar" src={ this.state.auth.user.avatar } className="rounded-circle avatar" width="25" /> { this.state.auth.user.name }
                  </a>
                  <div className="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                    <NavLink className="dropdown-item" to="/profile"><i className="ion-person"/> <Translate value="Profile"/> </NavLink>
                    <NavLink className="dropdown-item" to="/login"> <i className="ion-log-out"/> <Translate value="Logout"/></NavLink>
                  </div>
                </li>
            </ul>
              </div>
            </div>
          </nav>
            <this.props.component {...this.props} />
        </div>
      )
    }else if( this.state.auth.user && this.state.auth.user.role != 'operator'){
      return (
        <div className="layout">

        <nav className="navbar navbar-expand-lg navbar-inverse navbar-dark bg-fade navbar-theme fixed-top">
          <div className="container">
            <NavLink exact={true} className="navbar-brand " to="#">
              <img className="logo" src=" /assets/imgs/PPLogo-circle_white.svg" alt="PPaSS" /> Preference Pass admin
            </NavLink>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="navbarSupportedContent">
             <ul className="navbar-nav mr-auto">
                <li className="nav-item">
                  <NavLink className="nav-link" exact={true} to="/" ><i className="ion-clock"/> <Translate value="Dashboard"/> </NavLink>
                </li>
                {
                  this.state.auth.user.email == 'gv@preferencepass.com' &&
                  <li className="nav-item ">
                    <NavLink className="nav-link" to="/bookings"><i className="ion-ios-folder-outline"/> <Translate value="Bookings"/> </NavLink>
                  </li>
                }
                {
                  this.state.auth.user.email == 'gv@preferencepass.com' &&
                  <li className="nav-item ">
                    <NavLink className="nav-link" to="/usersfa"><i className="ion-person-stalker"/> <Translate value="UsersFa"/> </NavLink>
                  </li>
                }
                {
                  this.state.auth.user.email == 'gv@preferencepass.com' &&
                  <li className="nav-item ">
                    <NavLink className="nav-link" to="/roomUpgrade"><i className="ion-person-stalker"/> <Translate value="Room Upgrade"/> </NavLink>
                  </li>
                }
                <li className="nav-item ">
                  <NavLink className="nav-link" to="/passports"><i className="ion-ios-folder-outline"/> <Translate value="Passports"/> </NavLink>
                </li>
                 <li className="nav-item ">
                  <NavLink className="nav-link" to="/posts"><i className="ion-ios-location-outline"/> <Translate value="Posts"/> </NavLink>
                </li>
                {
                this.state.auth.user.email == 'gv@preferencepass.com' &&
                  <li className="nav-item ">
                    <NavLink className="nav-link" to="/postsfa"><i className="ion-ios-location-outline"/> <Translate value="Postsfa"/> </NavLink>
                  </li>
                }               
                <li className="nav-item ">
                  <NavLink className="nav-link" to="/affiliates"><i className="ion-ios-people"/> <Translate value="Affiliates"/> </NavLink>
                </li>
                <li className="nav-item ">
                  <NavLink className="nav-link" to="/users"><i className="ion-person-stalker"/> <Translate value="Users"/> </NavLink>
                </li>
                {
                  this.state.auth.user.email == 'gv@preferencepass.com' &&
                  <li className="nav-item hide">
                    <NavLink className="nav-link" to="/notification"><i className="ion-ios-folder-outline"/> <Translate value="Notification"/> </NavLink>
                  </li>
                }
                <li className="nav-item hide">
                <NavLink className="nav-link" to="/settings"><i className="ion-gear-b"/> <Translate value="Settings"/></NavLink>
                </li>
              </ul>
              <ul className="navbar-nav ">

              <li className="nav-item lang-item hide ">
                <a to="#" role="button" className="nav-link" data-lang="en" onClick={this.onChangeLang.bind(this)}  >EN</a>
                <a to="#" role="button" className="nav-link" data-lang="es" onClick={this.onChangeLang.bind(this)}  >ES</a>
              </li>
              <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" href="/profile" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                  <img alt="user avatar" src={ this.state.auth.user.avatar } className="rounded-circle avatar" width="25" /> { this.state.auth.user.name }
                </a>
                <div className="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                  <NavLink className="dropdown-item" to="/profile"><i className="ion-person"/> <Translate value="Profile"/> </NavLink>
                  <NavLink className="dropdown-item" to="/login"> <i className="ion-log-out"/> <Translate value="Logout"/></NavLink>
                </div>
              </li>
            </ul>
              </div>
            </div>
          </nav>
            <this.props.component {...this.props} />
        </div>
      )
    }else{
      return (
        <div className="layout">

        <nav className="navbar navbar-expand-lg navbar-inverse navbar-dark bg-fade navbar-theme fixed-top">
          <div className="container">
            <NavLink exact={true} className="navbar-brand " to="#">
              <img className="logo" src=" /assets/imgs/PPLogo-circle_white.svg" alt="PPaSS" /> Preference Pass admin
            </NavLink>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="navbarSupportedContent">
              <ul className="navbar-nav mr-auto">
                <li className="nav-item">
                <NavLink className="nav-link" to="/affiliates"><i className="ion-gear-b"/> <Translate value="Affiliates"/></NavLink>
                </li>
                <li className="nav-item ">
                  <NavLink className="nav-link" to="/bookings"><i className="ion-ios-folder-outline"/> <Translate value="Bookings"/> </NavLink>
                </li>
                <li className="nav-item ">
                  <NavLink className="nav-link" to="/roomUpgrade"><i className="ion-person-stalker"/> <Translate value="Room Upgrade"/> </NavLink>
                </li>
                <li className="nav-item ">
                  <NavLink className="nav-link" to="/postsfa"><i className="ion-ios-location-outline"/> <Translate value="Postsfa"/> </NavLink>
                </li>  
              </ul>
              <ul className="navbar-nav ">

              <li className="nav-item lang-item">
                <a to="#" role="button" className="nav-link" data-lang="en" onClick={this.onChangeLang.bind(this)}  >EN</a>
                <a to="#" role="button" className="nav-link" data-lang="es" onClick={this.onChangeLang.bind(this)}  >ES</a>
              </li>
              <li className="nav-item dropdown">
                  <a className="nav-link dropdown-toggle" href="/profile" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    <img alt="user avatar" src={ this.state.auth.user.avatar } className="rounded-circle avatar" width="25" /> { this.state.auth.user.name }
                  </a>
                  <div className="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                    <NavLink className="dropdown-item" to="/profile"><i className="ion-person"/> <Translate value="Profile"/> </NavLink>
                    <NavLink className="dropdown-item" to="/login"> <i className="ion-log-out"/> <Translate value="Logout"/></NavLink>
                  </div>
                </li>
            </ul>
              </div>
            </div>
          </nav>
            <this.props.component {...this.props} />
        </div>
      )
    }
  }
}

export default Main;
