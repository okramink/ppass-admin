import React from 'react';

class Login extends React.Component {

  componentDidMount() {
    document.body.className = "login";
  }
  render() {
    return (
        <div className="row fh align-items-center">
          <div className="col-md-12 splash fh ">            
           <this.props.component {...this.props} />
        </div>
      </div>

    )
  }
}
export default Login;

