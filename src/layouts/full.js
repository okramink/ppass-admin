import React      from 'react';
import PropTypes  from 'prop-types';
import {NavLink}  from 'react-router-dom';


//Components
import {Translate, setLocale} from 'react-redux-i18n';

class Main extends React.Component {
 static get contextTypes() {
    return {
      router: PropTypes.object.isRequired,
      store: PropTypes.object.isRequired
    }
  }
 constructor(props, context) {
    super(props, context);
    this.isAuthenticated = sessionStorage.getItem('isAuthenticated');
    this.store  = context.store;
    this.state  = this.store.getState();
  }
  componentDidMount(){
    document.body.className = "full";
     this.unsuscribe = this.store.subscribe(() => {
        this.setState(this.store.getState());
    });
  }
  componentWillUnmount() {
    this.unsuscribe();
    document.body.className = "";
  }
  onChangeLang = (e) => {
    this.store.dispatch(setLocale(e.target.dataset.lang))
    window.localStorage.setItem('lang', e.target.dataset.lang);
  }
  render(){
    if( this.state.auth.user ){
      return (
        <div className="">
          <nav className="navbar navbar-toggleable-md navbar-inverse navbar-theme bg-faded fixed-top">
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                  <span className="navbar-toggler-icon"></span>
                </button>
                <NavLink exact={true} className="navbar-brand " to="#">
                  <i className="ion-log-in" /> PreferencePass
                </NavLink>
                  <div className="collapse navbar-collapse " id="navbarCollapse">
                    <ul className="navbar-nav mr-auto">
                      <li className="nav-item">
                        <NavLink className="nav-link" exact={true} to="/" > <i className="ion-clock"/> <Translate value="Dashboard"/> </NavLink>
                      </li>
                       <li className="nav-item hide">
                        <NavLink className="nav-link" to="/locations"> <i className="ion-ios-location-outline"/> <Translate value="Locations"/> </NavLink>
                      </li>
                       <li className="nav-item ">
                        <NavLink className="nav-link" to="/devices"> <i className="ion-ios-monitor-outline"/> <Translate value="Devices"/> </NavLink>
                      </li>
                      <li className="nav-item ">
                        <NavLink className="nav-link" to="/files"> <i className="ion-ios-folder-outline"/> <Translate value="Files"/> </NavLink>
                      </li>
                      <li className="nav-item ">
                        <NavLink className="nav-link" to="/playlists"> <i className="ion-ios-albums-outline"/> <Translate value="Playlists"/> </NavLink>
                      </li>
                      <li className="nav-item ">
                        <NavLink className="nav-link" to="/layouts"> <i className="ion-ios-grid-view"/> <Translate value="Layouts"/> </NavLink>
                      </li>
                      <li className="nav-item ">
                        <NavLink className="nav-link" to="/users"> <i className="ion-person-stalker"/> <Translate value="Users"/> </NavLink>
                      </li>
                      <li className="nav-item hide">
                      <NavLink className="nav-link" to="/settings"> <i className="ion-gear-b"/> <Translate value="Settings"/></NavLink>
                      </li>
                    </ul>
                    <ul className="navbar-nav ">

                    <li className="nav-item lang-item hide ">
                      <a to="#" role="button" className="nav-link" data-lang="en" onClick={this.onChangeLang.bind(this)}  >EN</a>
                      <a to="#" role="button" className="nav-link" data-lang="es" onClick={this.onChangeLang.bind(this)}  >ES</a>
                    </li>
                    <li className="nav-item dropdown">
                        <a className="nav-link dropdown-toggle" href="/profile" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                          <img alt="user avatar" src={ this.state.auth.user.avatar } className="rounded-circle avatar" width="25" /> { this.state.auth.user.name }
                        </a>
                        <div className="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                          <NavLink className="dropdown-item" to="/profile"><i className="ion-person"/> <Translate value="Profile"/> </NavLink>
                          <NavLink className="dropdown-item" to="/login"> <i className="ion-log-out"/> <Translate value="Logout"/></NavLink>
                        </div>
                      </li>
                  </ul>
                 </div>

            </nav>
            <this.props.component {...this.props} />
        </div>
      )
    }else{
      return null;
    }
  }
}

export default Main;
