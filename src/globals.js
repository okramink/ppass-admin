

export const endpoints = {

	dev : {
    api:'http://localhost:1337',
    socket:'http://localhost:1337',
    domain:'http://localhost:1351',
    cdn:{
    	posts:'https://6c0786fc3e3b30f70d01-0963afee2fce7f91f53fe65892c25127.ssl.cf1.rackcdn.com'
	    }
	},
	prod  : {
	    api:'https://ws.preferencepass.com',
	    socket:'https://ws.preferencepass.com',
	    domain:'https://preferencepass.com',
	    cdn:{
	    	posts:'https://6c0786fc3e3b30f70d01-0963afee2fce7f91f53fe65892c25127.ssl.cf1.rackcdn.com'
	    }
	}

}



export default  {
    api    : process.env.REACT_APP_ENV ? endpoints[process.env.REACT_APP_ENV].api    : endpoints.prod.api,
    socket :process.env.REACT_APP_ENV  ? endpoints[process.env.REACT_APP_ENV].socket : endpoints.prod.socket,
    domain :process.env.REACT_APP_ENV  ? endpoints[process.env.REACT_APP_ENV].domain : endpoints.prod.domain,
    cdn:{
    	posts:'https://6c0786fc3e3b30f70d01-0963afee2fce7f91f53fe65892c25127.ssl.cf1.rackcdn.com'
    }
}
