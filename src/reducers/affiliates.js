import {
    SET_DATA,
    SET_CURRENT,
    REQUEST_AFFILIATES,
    SET_DATA_AFFILIATES,
    ADD_AFFILIATES_TAGS,
    ADD_AFFILIATES_CATEGORIES,
    SET_POST_HTML_CONTENT,
    SET_POST_IMAGE_FEATURED,
    REMOVE_POST_IMAGE_FEATURED,
    SET_DATA_PASSPORTS,
    SHOW_POSTS
} from '../actions/affiliates';

var init = {
    data       :null,
    error      :null,
    loader     : false,
    featured   :'',
    post       :null,
    showPosts  :null,
    passports  :null,
    tagsSelect :{
      value    : [],          
      disabled : false
    },
    categoriesSelect:{
      value : [],          
      disabled : false
    },
    content:'',
  };
export default function(state = init, action) {
  switch(action.type) {
    case SET_CURRENT:    
    return Object.assign({}, state, {         
        loader  : action.loader,
        current : action.data
    });
    case SHOW_POSTS:    
    return Object.assign({}, state, {          
      loader  : action.loader, 
      showPosts : action.data
    });
    case REQUEST_AFFILIATES:
    return Object.assign({}, state, {
        loader  : action.loader
      });
    case SET_DATA_AFFILIATES:
    return Object.assign({}, state, {
        loader  : action.loader,
        data    : action.data
      });
    case SET_DATA_PASSPORTS:
    return Object.assign({}, state, {
        loader      : action.loader,
        passports   : action.data
      });
    case ADD_AFFILIATES_TAGS:
    return Object.assign({}, state, {
        tagsSelect  : {
          value : action.value
        }
      });
    case ADD_AFFILIATES_CATEGORIES:
    return Object.assign({}, state, {
        categoriesSelect  : {
          value : action.value
        }
      });    
    case SET_POST_HTML_CONTENT:
    return Object.assign({}, state, {
        content : action.value
    });
    case SET_POST_IMAGE_FEATURED:
    return Object.assign({}, state, {
        featured : action.value
    });
    case REMOVE_POST_IMAGE_FEATURED:
    return Object.assign({}, state, {
        featured : ''
    });
    
    default:
    return state;
  }
}