/*eslint-disable*/
import {
    REQUEST_DEVICES,
    SET_DATA_DEVICES,
    HIDE_SEARCH_DEVICES,
    REQUEST_SEARCH_DEVICES,
    SET_SEARCH_DATA_DEVICES,
    SET_DATA_DEVICE,
    REQUEST_SAVE_DEVICE,
    SHOW_DEVICE_FORM,
    SHOW_PAYMENT_FORM,
    SET_UPDATE
} from '../actions/devices';

/*eslint-enable*/
var init = {
    table        :null,
    data         :null,
    error        :null,
    loader       :false,
    search       :null,
    cards        :null,
    customer     :null,
    saveLoader   :false,
    showDeviceForm :false
};
export default function(state = init, action) {
  switch(action.type) {
    case REQUEST_DEVICES:
    return Object.assign({}, state, {
        loader  : action.loader
      });
    case SET_DATA_DEVICES:
    return Object.assign({}, state, {
        loader     : action.loader,
        saveLoader : action.loader,
        table      : action.data
      });
    case HIDE_SEARCH_DEVICES:
    return Object.assign({}, state, {
        search:null
    });
    case REQUEST_SEARCH_DEVICES:
    return Object.assign({}, state, {
        searchLoader  : action.loader,
        search:null
    });
    case SET_SEARCH_DATA_DEVICES:
    return Object.assign({}, state, {
        searchLoader  : action.loader,
        search        : action.data
    });
    case REQUEST_SAVE_DEVICE:
    return Object.assign({}, state, {
        saveLoader  :action.loader,
        customer    :null
    });
    case SET_DATA_DEVICE:
    return Object.assign({}, state, {
        saveLoader  : action.loader,
        customer    : action.data
      });
    case SHOW_DEVICE_FORM:
    return Object.assign({}, state, {
        showCardForm  : action.status
    });
    case SET_UPDATE:
    return Object.assign({}, state, {
        
    });

    default:
    return state;
  }
}