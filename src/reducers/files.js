/*eslint-disable*/
import {
    REQUEST_FILES,
    SET_DATA_FILES,
    HIDE_SEARCH_FILES,
    REQUEST_SEARCH_FILES,
    SET_SEARCH_DATA_FILES,
    SET_DATA_FILE,
    REQUEST_SAVE_FILE,
    SHOW_FILE_FORM,
    SHOW_PAYMENT_FORM,
    SET_TEMPORAL_FILE,
    SET_TEMPORAL_FILE_PROGRESS,
    SET_TEMPORAL_FILE_UPLOADED,
    REMOVE_TEMPORAL_FILE,
    SET_DATA_LIST,
    SET_TEMPORAL_LIST,
    REMOVE_TEMPORAL_LIST,
    RESET_TEMPORAL_LIST,
    REMOVE_ITEM_LIST,
    SET_COMPLET_LIST
} from '../actions/files';

/*eslint-enable*/
var init = {
    table        :null,
    data         :null,
    error        :null,
    loader       :false,
    search       :null,
    cards        :null,
    customer     :null,
    temporal     :[],
    list         :[],
    listTable    :null,
    saveLoader   :false,
    showDeviceForm :false
};
export default function(state = init, action) {
  switch(action.type) {
    case REQUEST_FILES:
    return Object.assign({}, state, {
        loader  : action.loader
      });
    case SET_DATA_FILES:
    return Object.assign({}, state, {
        loader     : action.loader,
        saveLoader : action.loader,
        table      : action.data
      });
    case HIDE_SEARCH_FILES:
    return Object.assign({}, state, {
        search:null
    });
    case REQUEST_SEARCH_FILES:
    return Object.assign({}, state, {
        searchLoader  : action.loader,
        search:null
    });
    case SET_SEARCH_DATA_FILES:
    return Object.assign({}, state, {
        searchLoader  : action.loader,
        search        : action.data
    });
    case REQUEST_SAVE_FILE:
    return Object.assign({}, state, {
        saveLoader  :action.loader,
        customer    :null
    });
    case SET_DATA_FILE:
    return Object.assign({}, state, {
        saveLoader  : action.loader,
        customer    : action.data
      });
    case SHOW_FILE_FORM:
    return Object.assign({}, state, {
        showCardForm  : action.status
    });
    case SET_TEMPORAL_FILE:    
        state.temporal.push( action.data)
    return Object.assign({}, state, {}); 
    case SET_TEMPORAL_FILE_PROGRESS:    
        state.temporal[action.idx].percent  = action.percent;
        state.temporal[action.idx].uploaded = action.uploaded;
    return Object.assign({}, state, {});
    case SET_TEMPORAL_FILE_UPLOADED:                
    return Object.assign({}, state, {
        temporal:[]
    });
    case REMOVE_TEMPORAL_FILE:
        state.temporal.splice(action.idx,1);
    return Object.assign({}, state,{});    
    case SET_DATA_LIST:
    return Object.assign({}, state, {
        loader     : action.loader,
        saveLoader : action.loader,
        listTable  : action.data
      });
    case SET_TEMPORAL_LIST:    
        state.list.push( action.data)
    return Object.assign({}, state, {}); 
    case RESET_TEMPORAL_LIST:                
    return Object.assign({}, state, {
        list:[]
    });
    case REMOVE_ITEM_LIST:    
        state.list.splice(action.idx,1);
    return Object.assign({}, state, {});     
    case SET_COMPLET_LIST:
    return Object.assign({}, state, {
        list:action.data
    });
    default:
    return state;
  }
}