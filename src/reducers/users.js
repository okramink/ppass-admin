import {
    REQUEST_USERS,
    SET_DATA_USERS,
    SET_DATA_USERSFA,
    SET_DATA_ADMINS
} from '../actions/users';

var init = {
    data:null,
    error:null,
    admintable:null,
    loader: false};
export default function(state = init, action) {
  switch(action.type) {
    case REQUEST_USERS:
    return Object.assign({}, state, {
        loader  : action.loader
      });
    case SET_DATA_USERS:
    return Object.assign({}, state, {
        loader  : action.loader,
        data    : action.data
    });
    case SET_DATA_USERSFA:
    return Object.assign({}, state, {
        loader  : action.loader,
        datafa  : action.data
    });
    case SET_DATA_ADMINS:
    return Object.assign({}, state, {
        loader     : action.loader,
        admintable : action.data
    });
    default:
    return state;
  }
}