/*eslint-disable*/
import {
  
    SET_DATA_LIST,
    SET_TEMPORAL_LIST,
    REMOVE_TEMPORAL_LIST,
    RESET_TEMPORAL_LIST,
    REMOVE_ITEM_LIST,
    SET_COMPLET_LIST,
    SET_SEARCH_RESULT,
    SET_SELECTED_VALUE
} from '../actions/playlists';

/*eslint-enable*/
var init = {
    table        :null,
    data         :null,
    results      :[],
    error        :null,
    loader       :false,    
    temporal     :[],
    list         :[],
    list_value   :'',
    listTable    :null,
    saveLoader   :false,
    showDeviceForm :false,
    
};
export default function(state = init, action) {
  switch(action.type) {
  
    case SET_TEMPORAL_LIST:    
        state.list.push( action.data)
    return Object.assign({}, state, {}); 
    case RESET_TEMPORAL_LIST:                
    return Object.assign({}, state, {
        list:[]
    });
    case REMOVE_ITEM_LIST:    
        state.list.splice(action.idx,1);
    return Object.assign({}, state, {});     
    case SET_COMPLET_LIST:
    return Object.assign({}, state, {
        list:action.data
    });
    case SET_SEARCH_RESULT:    
    return Object.assign({}, state, {
        results:action.data
    });
    case SET_SELECTED_VALUE:    
    return Object.assign({}, state, {
        list_value:action.value
    });    
    default:
    return state;
  }
}