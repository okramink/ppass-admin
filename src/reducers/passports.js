import {
    SET_POST,
    REQUEST_PASSPORTS,
    SET_DATA_PASSPORTS,
    SET_DATA_PASSPORTSMaster,
    ADD_PASSPORTS_TAGS,
    ADD_PASSPORTS_CATEGORIES,
    SET_POST_HTML_CONTENT,
    SET_POST_IMAGE_FEATURED,
    REMOVE_POST_IMAGE_FEATURED
} from '../actions/passports';

var init = {
    data:null,
    error:null,
    loader: false,
    featured:'',
    post:null,
    tagsSelect:{
      value : [],          
      disabled : false
    },
    categoriesSelect:{
      value : [],          
      disabled : false
    },
    content:'',
  };
export default function(state = init, action) {
  switch(action.type) {
    case SET_POST:    
    return Object.assign({}, state, {         
      post    : action.post
    });
    case REQUEST_PASSPORTS:
    return Object.assign({}, state, {
        loader  : action.loader
      });
    case SET_DATA_PASSPORTSMaster:
    return Object.assign({}, state, {
        loader  : action.loader,
        data    : action.data
      });
    case SET_DATA_PASSPORTS:
    return Object.assign({}, state, {
        loader  : action.loader,
        data    : action.data
      });
    case ADD_PASSPORTS_TAGS:
    return Object.assign({}, state, {
        tagsSelect  : {
          value : action.value
        }
      });
    case ADD_PASSPORTS_CATEGORIES:
    return Object.assign({}, state, {
        categoriesSelect  : {
          value : action.value
        }
      });    
    case SET_POST_HTML_CONTENT:
    return Object.assign({}, state, {
        content : action.value
    });
    case SET_POST_IMAGE_FEATURED:
    return Object.assign({}, state, {
        featured : action.value
    });
    case REMOVE_POST_IMAGE_FEATURED:
    return Object.assign({}, state, {
        featured : ''
    });
    
    default:
    return state;
  }
}