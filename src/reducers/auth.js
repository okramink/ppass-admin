import {
    REQUEST_SESSION,
    SET_SESSION
} from '../actions/auth';

var init = {user:null, status:null, error:null, loader: false};
export default function(state = init, action) {
  switch(action.type) {
    case REQUEST_SESSION:
    return Object.assign({}, state, {
        loader  : action.loader
      });
    case SET_SESSION:
    return Object.assign({}, state, {
        loader  : action.loader,
        user    : action.user
      });
    default:
    return state;
  }
}