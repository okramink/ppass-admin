import {
    REQUEST_BOOKINGS,
} from '../actions/bookings';

var init = {
    data:null,
    error:null,
    loader: false,
    content:'',
  };
export default function(state = init, action) {
  switch(action.type) {
    case REQUEST_BOOKINGS:
    return Object.assign({}, state, {
        loader  : action.loader,
        data    : action.data
      });
    
    default:
    return state;
  }
}