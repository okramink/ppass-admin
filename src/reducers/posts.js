import {
    SET_POST,
    REQUEST_POSTS,
    SET_DATA_POSTS,
    ADD_POSTS_TAGS,
    ADD_POSTS_TIME,
    ADD_POSTS_CATEGORIES,
    SET_POST_HTML_CONTENT,
    SET_POST_IMAGE_FEATURED,
    REMOVE_POST_IMAGE_FEATURED,
    SET_ALERT
} from '../actions/posts';

var init = {
    data:null,
    error:null,
    loader: false,
    featured:'',
    post:null,
    tagsSelect:{
      value : [],
      disabled : false
    },
    timeSelect:{
      value : [],
      disabled : false
    },
    categoriesSelect:{
      value : [],
      disabled : false
    },
    content:'',
    alert_status:false,
    alert_msg   : '',
    alert_mode  : '',
    alert_icon  : ''

  };
export default function(state = init, action) {
  switch(action.type) {
    case SET_POST:
    return Object.assign({}, state, {
      post    : action.post
    });
    case REQUEST_POSTS:
    return Object.assign({}, state, {
        loader  : action.loader
      });
    case SET_DATA_POSTS:
    return Object.assign({}, state, {
        loader  : action.loader,
        data    : action.data
      });
    case ADD_POSTS_TAGS:
    return Object.assign({}, state, {
        tagsSelect  : {
          value : action.value
        }
      });
      case ADD_POSTS_TIME:
      return Object.assign({}, state, {
          timeSelect  : {
            value : action.value
          }
        });
    case ADD_POSTS_CATEGORIES:
    return Object.assign({}, state, {
        categoriesSelect  : {
          value : action.value
        }
      });
    case SET_POST_HTML_CONTENT:
    return Object.assign({}, state, {
        content : action.value
    });
    case SET_POST_IMAGE_FEATURED:
    return Object.assign({}, state, {
        featured : action.value
    });
    case REMOVE_POST_IMAGE_FEATURED:
    return Object.assign({}, state, {
        featured : ''
    });
    case SET_ALERT:
    return Object.assign({}, state, {
        alert_status : action.status,
        alert_msg    : action.msg,
        alert_icon   : action.icon,
        alert_mode   : action.mode,
    });
    default:
    return state;
  }
}
