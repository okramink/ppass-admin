import React           from 'react';
import ReactHtmlParser from 'react-html-parser';

import {Link}  from 'react-router-dom';

class Loader extends React.Component{
	render(){
		if(!this.props.data){
			return(
				<div className="loading"></div>
			);
		}else{
			return null
		}
	}
}

class HeadersCol extends React.Component{
	render(){
		var width   = this.props.item.width ? this.props.item.width : '' ;
		var align   = 'sortable' + this.props.item.align ? this.props.item.align : 'left' ;
		// var lastcol = '';
		//('sortable'+' '+ align +' '+ lastcol)
		//('sortable'+' '+ align +' '+ lastcol)
		return(<th width={width} className={ align } >{this.props.item.label}</th>);
	}
}

class Headers extends React.Component{
	render(){
		var items = [];
		if( this.props.headers ){
			this.props.headers.forEach((item, index) => {
			items.push(<HeadersCol key={'col-'+index} idx={index} item={item} />);
			});
			return(
				<thead className="">
				<tr>
				{items}
				</tr>
			</thead>
			)
		}else{
			return null
		}
	}
}

class Badge extends React.Component{
	createMarkup() {
		return {__html: this.props.label}
	}
	render(){
		let badge;
		if(this.props.badge){
			if( typeof(this.props.badge) === 'object'){
				 badge = this.props.badge.default;
				for(var x in this.props.badge ){
					if( x === this.props.label.toLowerCase() ){
						 badge = this.props.badge[x];
					}
				}
				//console.log(this.props)
				if(this.props.item.role == 'invited'){
					//console.log(this.props)
					return( <span className={'badge badge-pill badge-'+ badge }><i className={this.props.icon} /> {ReactHtmlParser(this.props.label)} </span>)
				}else{
					if(this.props.label == 'note'){
						//console.log(this.props.item.booknote)
						if(this.props.item.booknote == undefined || this.props.item.booknote == ''){
							return( <span className={'badge badge-pill badge-'+ badge}><i className={this.props.icon} /> {ReactHtmlParser(this.props.label)} </span>)	
						}else{
							return( <span className={'badge badge-pill badge-primary-link'}><i className={this.props.icon} /> {ReactHtmlParser(this.props.label)} </span>)
						}
					}else{
						return( <span className={'badge badge-pill badge-'+ badge }><i className={this.props.icon} /> {ReactHtmlParser(this.props.label)} </span>)
					}
				}
			}else{
				return(
					<span className={'badge badge-pill badge-'+ this.props.badge }><i className={this.props.icon} /> {ReactHtmlParser(this.props.label)}</span>
				)
			}
		}
		return(
			<span dangerouslySetInnerHTML={this.createMarkup()}></span>
		)
	}
}

class Label extends React.Component{
	render(){

		if(this.props.value ){
			var label = this.props.value( this.props.item );
			if( label === ''){
				return null
			}
			return(
				<Badge {...this.props} icon={this.props.col.icon} label={label} />
			)
		}

		return(
			<Badge {...this.props} icon={this.props.col.icon} label={this.props.label} />
		)
	}
}

class Actions extends React.Component{
	constructor(props){
		super(props)
		this.state = {
			checked: null
		}

		if(this.props.col.type == 'check'){
			//console.log(this.props)
		}
	}
	componentDidMount() {
		if(this.props.col.onhover){
			for(var i in this.refs){
				this.props.col.onhover(this.props.item,this.refs[i]);
			}
		}
	}
	onSaveAproval(ev, poPops){
		//console.log(ev)
		//this.props.col.oncheck.bind(ev.target.checked);
		let taref = ev.target.ref
		let isChecked = ev.target.checked
		this.setState(prevState => ({checked: isChecked}))
		this.props.col.oncheck(ev, this.props.item, this.props.page.pages)
	}
	setCheck(label){
		return label;
	}
	render(){
		if(this.props.col.onclick){

			return(
				<Link to="#" role="button" ref={this.props.idx} data-toggle="popover" onClick={this.props.col.onclick.bind(this,this.props.item)} >
					<Label {...this.props} />
				</Link>
			)
		}

		if(this.props.col.onhover){
			return(
				<Link top="#" role="button" ref={this.props.idx} data-toggle="popover" >
					<Label {...this.props} />
				</Link>
			)
		}

		if(this.props.col.type == 'check' || this.props.col.type == 'check2'){
			console.log(this.props)
			return (
				<input type="checkbox" ref={this.props.item.id} checked={this.setCheck(this.props.label)} onChange={(ev) => this.onSaveAproval(ev, this)} />
			)
		}

		return(<Label {...this.props} />)
	}
}

class Value extends React.Component{
	twoDigits(d) {
	    if(0 <= d && d < 10) return "0" + d.toString();
	    if(-10 < d && d < 0) return "-0" + (-1*d).toString();
	    return d.toString();
	}
	toDate (str){
		var monthNames = [ "Enero", "Febrero",  "Marzo", "Abril", "Mayo", "Junio","Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
		var date = new Date(str);
		return date.getFullYear() + "-" + monthNames[date.getMonth()] + "-" + this.twoDigits(date.getDate()) + " " ;
	}
	toDateTime (str){
		var date = new Date(str);
		return date.getFullYear() + "-" + this.twoDigits(1 + date.getMonth()) + "-" + this.twoDigits(date.getDate()) + " " + this.twoDigits(date.getHours()) + ":" + this.twoDigits(date.getUTCMinutes()) + ":" + this.twoDigits(date.getUTCSeconds());
	}
	getValue(){
		let label;
		//console.log(this.props.item)
		switch( this.props.col.type ){
			case 'date':
				 label = this.props.label != null ? this.toDate( this.props.label ) : 'No Date';
			break;
			case 'datetime':
				 label = this.props.label ? this.toDateTime( this.props.label ) : 'No Date' ;
			break;
			case 'money':
				 label = this.props.label ? '$'+ parseFloat(this.props.label).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,') : '0.00';
			break;
			case 'float':
				 label = this.props.label ? parseFloat(this.props.label).toFixed(2) : '0';
			break;
			case 'percent':
				 label = this.props.label ? parseFloat(this.props.label).toFixed(2) + '%' : '0%';
			break;
			case 'text':
				 label = this.props.label ? this.props.label : '';
			break;
			case 'check':
				 label = this.props.item.aproved == 'true' ? true : false;
			break;
			case 'check2':
				 label = this.props.item.aprovedAgain == 'true' ? true : false;
			break;
			default:
				 label = this.props.label ? this.props.label : '';
			break;
		}
		return label;
	}
	render(){
		//console.log(this.props)
		//if( this.props.label != 'null' && this.props.label != null ){
			return(<Actions {...this.props} label={ this.getValue() } />)
		// }else{
		// 	return null
		// }
	}
}

class Cell extends React.Component{
	render(){
		var align   = this.props.col.align ? this.props.col.align : 'left' ;
		var lastcol = '';
		var nogrid  = '';
		return(
			<td className={ align +' '+ lastcol + ' ' + nogrid }>
				<Value {...this.props} />
			</td>
		)
	}
}

class RowsCol extends React.Component{
	render(){
		var label = this.props.item[this.props.col.db];
		return(
			<Cell
			idx     = {this.props.idx}
			label   = {label}
			item    = {this.props.item}
			col     = {this.props.col}
			badge   = {this.props.col.badge}
			value   = {this.props.col.value}
			page    = {this.props.pages}
			/>
		);
	}
}

class Rows extends React.Component{
	render(){
		var items = [];
		//console.log(this.props)
		this.props.headers.forEach((item, index) => {
	      items.push(<RowsCol idx={this.props.idx +'-'+index} key={'coldata-'+index} col={item} {...this.props} pages={this.props.data} />);
	    });
		return(
			<tr>
			{items}
		    </tr>
		);
	}
}

class Body extends React.Component{
	render(){
		var items = [];
		console.log(this.props.data)
		if( this.props.data ){
			this.props.data.data.forEach((item, index) => {
		      items.push(<Rows key={'rowdata-'+index} idx={index} item={item} data={this.props.data.pages} {...this.props} />);
		    });
	    }
		return(
			<tbody>
		    {items}
		  </tbody>
		)
	}
}

class RowsPage extends React.Component{
	click(p,e){
		this.refs['navlink_'+p].className = 'page-item load';
		this.props.setPage(e);
	}
	render(){
		var active = this.props.current === this.props.page ? 'active' : '';
		return(
			<li ref={'navlink_'+this.props.page} className={'page-item '+ active}>
		      <Link className="page-link"  to="#" role="button" onClick={ this.click.bind(this,this.props.page) } data-page={this.props.page}>{this.props.page}</Link>
		    </li>
		);
	}
}

class PrevPage extends React.Component{
	click(e){
		this.props.prevPage(e);
	}
	render(){
		if( this.props.data.pages.current_page > 1){
			return(
				<li className="page-item">
			      <Link className="page-link" to="#" onClick={ this.click.bind(this) } ><i className="ion-chevron-left" /></Link>
			    </li>
			);
		}else{
			return(
				<li className="page-item disabled">
			      <Link className="page-link" to="#"><i className="ion-chevron-left" /></Link>
			    </li>
			);
		}
	}
}

class NextPage extends React.Component{
	click(e){
		this.props.nextPage(e);
	}
	render(){
		if( this.props.data.pages.current_page < this.props.data.pages.last_page ){
			return(
				<li className="page-item">
			      <Link className="page-link" to="#" onClick={ this.click.bind(this) }><i className="ion-chevron-right" /></Link>
			    </li>
			);
		}else{
			return (
				<li className="page-item disabled">
			      <Link className="page-link" to="#" ><i className="ion-chevron-right" /></Link>
			    </li>
			)
		}
	}
}

class Pagination extends React.Component{

	render(){
		var items = [];
		if( this.props.data && this.props.data.pages && this.props.data.pages.count > 1 ){
			var pages 		= this.props.data.pages;
			var pageview    = 10;
			var interval    = Math.floor ( (pageview/2) );
			var total 	    = pages.last_page;

			var current     = parseInt(pages.current_page,0);

			var aux 	    = ( Math.round(current) - pageview ) + interval;
			let initpage    = current < pageview ?  0 : ( aux ) ;
			let maxpages    = current === total  ? total : initpage + pageview ;
				maxpages    = maxpages > total ? total : maxpages;
			var aux2 	    = maxpages - initpage ;
			var ninitpage   = initpage - ( pageview - aux2 ) ;
				initpage    = aux2 < pageview && ( total > pageview ) ? ninitpage : initpage ;

			if(maxpages > 1){
				for(var i=initpage; i < maxpages; i++){
			      items.push(<RowsPage key={'pagedata-'+i} {...this.props} page={i+1} />);
			    }

				return(
					<nav >
						<ul className="pagination pagination-sm justify-content-center">
						   <PrevPage {...this.props} />
						    {items}
						   <NextPage {...this.props} />
					  	</ul>
					</nav>
				)
			}else{
				return null;
			}
		}else{
			return null
		}
	}
}

class Search extends React.Component{
	keyUp(e){
		this.props.onSearch(e.target)
	}
	render(){
		if(this.props.search){
			return(
			<div className="input-group input-group-md">
			  <input ref="keyword" type="text"  name="keyword" className="form-control" placeholder={this.props.placeholder} aria-describedby="basic-addon1" onKeyUp={this.keyUp.bind(this)} onKeyDown={this.props.goSearch} onBlur={this.props.goSearch} />
			  <span className="input-group-append " id="sizing-addon1"><span className="input-group-text"><i className="ion-flash-off" /></span></span>
			</div>
			)
		}else{
			return null
		}
	}
}

class Table extends React.Component{
	constructor(props) {
		super(props);
		this.request = this.props.request ? this.props.request : {};
		this.current = this.props.page    ? this.props.page   : 1;
		this.rows    = this.props.rows    ? this.props.rows   : 10;
		this.keyword = null;
	}
	load(){
		this.getRequest();
		this.props.source(this.request);
	}
	getRequest(){
		this.request 	  = this.props.request ? this.props.request : {};
		this.request.page = this.current;
		this.request.rows = this.rows;
		this.request.typeuser = this.props.typeuser;
		if(this.keyword){
			this.request.keyword = this.keyword;
		}
	}
	setPage(e){
		this.current = parseInt(e.target.dataset.page,0);
		this.load();
	}
	nextPage(){
		this.current = this.current + 1;
		this.load();
	}
	prevPage(){
		this.current = this.current === 1 ? 1 : this.current - 1;
		this.load();
	}
	onSearch(e){
	   clearTimeout(this.clearTimeout)
	   this.clearTimeout = setTimeout(function(){
	    	this.keyword  = e.value === '' ? null : e.value;
			this.load();
	    }.bind(this),1000)
  	}
  	goSearch(e){
  		if( e.key === 'Enter'){
			this.keyword  = e.target.value === '' ? null : e.target.value;
			this.load();
  		}

  		if(!e.key){
  			this.keyword  = e.target.value === '' ? null : e.target.value;
			this.load();
  		}
  	}
	componentDidMount() {
		this.load();
	}
	shouldComponentUpdate(nextProps, nextState) {
		if( !Object.is(this.props.data,nextProps.data)){
			return true;
		}else{
			return false;
		}
	}
	render(){
		return(
			<div className="ztable">
				<Search {...this.props} onSearch={this.onSearch.bind(this)} goSearch={this.goSearch.bind(this)} />
				<table className={`table table-stripe ${this.props.responsive && 'table-responsive'}`}>
					<Headers {...this.props} />
					<Body 	 {...this.props} />
				</table>
				<Loader {...this.props} />
				<Pagination {...this.props} current={this.current} setPage={this.setPage.bind(this)} nextPage={this.nextPage.bind(this)} prevPage={this.prevPage.bind(this)} />
			</div>

		)
	}
}

export default Table;
