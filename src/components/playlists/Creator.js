import React from 'react';
import BtnSubmitLoader from '../BtnSubmitLoader';
import {I18n} from 'react-redux-i18n';


//Actions
import {
  loadFiles,
  saveList,
  setTempList,
  resetList,
  removeItemList,
  setListUpdate
} from '../../actions/files';


class Item extends React.Component {
  
   dragStart(e){
    e.dataTransfer.setData('text/plain',null);
  }
  render(){

    return(
      <li id={'list-item-'+ this.props.idx} className="list-group-item " draggable="true" onDragStart={this.dragStart.bind(this)} data-id={this.props.idx}>
        <img src={this.props.item.thumb} className="img-thumbnail" alt={this.props.item.name}/>
        <span className="description" >{this.props.item.name} <br/> {this.props.item.timer }</span>
        <button type="button" className="btn btn-sm btn-success float-right ion-android-add-circle" title="add to playlist" onClick={this.props.addMedia.bind(this)} data-id={this.props.idx} > </button>
      </li>)
  }
}


class TemporalItem extends React.Component {  
   dragStart(e){
    e.dataTransfer.setData('text/plain',null);
  }
  render(){

    return(
      <li id={'list-item-'+ this.props.idx} className="list-group-item " data-id={this.props.idx}>
        <img src={this.props.item.thumb} className="img-thumbnail" alt={this.props.item.name}/>
        <span className="description" >{this.props.item.name} <br/> {this.props.item.timer } </span>
        <button type="button" className="btn btn-sm btn-danger float-right ion-trash-a" title="add to playlist" data-id={this.props.idx} onClick={this.props.removeMedia.bind(this)}> </button>
      </li>)
  }
}

class PlaylistCreator extends React.Component {
  constructor(props) {
    super(props);
    this.store = this.props.context.store;
    this.state = this.props.context.store.getState();
    this._fd   = {};
    this.index = 0;
    this.origin = null;
    this.timer  = new Date().getTime();

    this.title = I18n.t('List - ') + this.timer ;
    this.id    = null;
  

  }
  componentWillUnmount() {
    this.unsuscribe();

  }
  componentDidMount(){
    this.unsuscribe = this.store.subscribe(() => {
      this.setState(this.store.getState());
    });

    this.store.dispatch(resetList())
    if(this.props.data){
       this.title = this.props.data.title;
       this.store.dispatch(setListUpdate(this.props.data.content));
    }

    this.dragAndDrop();
    this.store.dispatch(loadFiles());

  }
  noRepeat(items,idx){
   let t = [];
   items.forEach((item,index )=>{    
      if( index === parseInt(idx,0)) {
        t.push(1);
      }
   })
   return t.length > 0 ? false : true ;
  }  
  close(){
    if( window.Light ){
      window.Light.close();    
    }
  }
  submit(e){
    e.preventDefault();        

     this._fd.id    =  this.id;
     this._fd.list  =  JSON.stringify(this.state.files.list);
     this._fd.title = this.refs.title.value !== '' ? this.refs.title.value : this.title;

     if( this._fd.list.length > 0 ){

      this.props.context.store.dispatch(saveList(this._fd,(device)=>{
          window.bootbox.alert('Los datos ha sido registrados correctamente');       
          this.close();
          this.store.dispatch(resetList() ) 
      }))

      }else{
        window.bootbox.alert('Playlist needs 1 media item at least.');       
      }
  }
  
  dragAndDrop(){

   var dragged;

    /* events fired on the draggable target */
    document.addEventListener("drag",  ( event ) => {
      // script
    }, false);
    document.addEventListener("dragstart", ( event ) => {
        // store a ref. on the dragged elem
        dragged = event.target;
        // make it half transparent
        event.target.style.opacity = .5;
    }, false);
    document.addEventListener("dragend",  ( event ) => {
        // reset the transparency
        event.target.style.opacity = "";
    }, false);
    /* events fired on the drop targets */
    document.addEventListener("dragover",  ( event ) => {
        // prevent default to allow drop
        event.preventDefault();
        event.dataTransfer.dropEffect = 'move';  
    }, false);
    document.addEventListener("dragenter",  ( event ) => {
        // highlight potential drop target when the draggable element enters it
        if ( event.target.classList.contains("dropzone")  ) {
          event.target.style.background = 'purple';
        }
    }, false);
    document.addEventListener("dragleave",  ( event ) => {
        // reset background of potential drop target when the draggable element leaves it
        if ( event.target.classList.contains("dropzone")  ) {
            event.target.style.background = "";
        }
    }, false);
    document.addEventListener("drop", ( event ) => {
        // prevent default action (open as link for some elements)
        event.preventDefault();
        // move dragged elem to the selected drop target
        if ( event.target.classList.contains("dropzone") ) {
            event.target.style.background = "";
            const id   = dragged.dataset.id;
            if( this.noRepeat(this.state.files.list,id)){            
              this.store.dispatch(setTempList( this.state.files.table.data[id] ) ) 
              event.target.scrollTop = event.target.scrollHeight;
            }
        }      
    }, false);
  }

 
  addMedia(e){
    
    const id = e.target.dataset.id;    
    if( this.noRepeat(this.state.files.list,id)){  
      this.store.dispatch(setTempList( this.state.files.table.data[id] ) ) 
      setTimeout(()=>{
        this.refs.destination.scrollTop = this.refs.destination.scrollHeight;
      },100)
    }
  }
  removeMedia(e){
    
    const id = e.target.dataset.id;        
    this.store.dispatch(removeItemList( id ) )   
    setTimeout(()=>{
      this.refs.destination.scrollTop = this.refs.destination.scrollHeight;
    },100)

  }
  
  render() {   
    var items = [];
    if( this.state.files.table){
      this.state.files.table.data.forEach((item, index) => {
        items.push(<Item key={'rowdata-'+index} idx={index} item={item} {...this.props} addMedia={this.addMedia.bind(this) }/>);
      });
    }

    var list = [];
    if( this.state.files.list){
      this.state.files.list.forEach((item, index) => {
        list.push(<TemporalItem key={'item-rowdata-'+index} idx={index} item={item} {...this.props} removeMedia={this.removeMedia.bind(this) } />);
      });
    }

    return (
      <div className="dragrow">

        <form onSubmit={this.submit.bind(this) } className="fh">
          <div className="row fh">
            <div className="col-md-6 fh">            
              <div className="row">
               <div className="col-md-12 mb">          
                <div className="input-group">
                    <input type="text" className="form-control search" placeholder={I18n.t('Search')} />
                      <span className="input-group-btn">
                        <button className="btn btn-secondary" type="button"><i className="ion-ios-search-strong"/></button>
                      </span>
                    </div>
                </div>
              </div>              
              <ul className="list-drag " ref="origin" id="origin" >
                {items}
              </ul>
            </div>     
            <div className="col-md-6 fh"> 

              <div className="row">
               <div className="col-md-12 mb">          
                  <div className="input-group">
                       <input type="text" className="form-control search" placeholder={this.title} ref="title" />    
                    </div>
                  </div>
              </div>          
              <ul className="list-drag dropzone" ref="destination" id="destination">
                {list}
              </ul>
            </div>
             <div className="clearfix" />
          </div>         
          
          <div className="row">
            <div className="col-md-12 ">
              <BtnSubmitLoader textBase={I18n.t('Save')} textProcess={I18n.t('Saving data')} icon="ion-plus-circled" className="btn-success btn-sm float-right" />   
            </div>
          </div>
        </form>
      </div>
    )
  }
}
export default PlaylistCreator;

