import React from 'react';
import BtnSubmitLoader from '../BtnSubmitLoader';
import {I18n} from 'react-redux-i18n';

import PropTypes from 'prop-types';

import Select from 'react-select';


//Actions
import {
  loadPassportsMasters
} from '../../actions/passports';
import { 
  saveAffiliatedPostUser
} from '../../actions/affiliates';

const peopleData = (name, label, price, extra ) => {
    return JSON.stringify({ name, label, price, extra});
}

class FormGroup extends React.Component {
  
  getLabel(){
    let string = this.props.label;
    return string.charAt(0).toUpperCase() + string.slice(1) + ' '+ (this.props.i+1)
  }
  render(){
      
      return(
       <div className="col-md-4">
        <div className="form-group">
          <label>{ this.getLabel() }:</label>
          <input className="form-control" type="text" data-index={this.props.index} data-label={this.props.label}  onChange={this.props.updatePeople.bind(this)} />
        </div>
      </div>
    )
  }
}
class AddForm extends React.Component{
    
  constructor(props,context) {
        super(props,context); 
        this.state = {        
          people :[],
          check: false,
          selectedOption: null,
          options: null
        }         
      this.adults = null;
      this.childs = null;
    
  }
  componentDidMount(){
    let options = []
    this.createPeople();
      this.props.context.store.dispatch(loadPassportsMasters(true)).then(res => {
      res.map((res) => {
        options.push({value:res.token,label:res.email+' createdAt: '+res.createdAt,email:res.email})
      })
      this.setState({options:options})})
  }
  handleChange = (selectedOption) => {
    this.setState({ selectedOption });
    console.log(`Option selected:`, selectedOption);
  }
  updateCheck(e){
    console.log(e.refs['master'].checked)
    if(e.refs['master'].checked){
      document.getElementById('masterToken').style.display = "block"
    }else{
      this.setState({selectedOption:null})
      document.getElementById('email').value = null
      document.getElementById('masterToken').style.display = "none"
    }
  }
  submit(e){
    e.preventDefault();
    var data = {};
    for( let x in this.refs ){
      if( this.refs[x].value !== '' ){
        data[x] = this.refs[x].value;
        this.refs[x].disabled = true;
      }     
    }

    let da = [];
    let checking = false;
    let checking2 = false;
    let post = ''
    let add = ''
    let cont = 0
    //let cadena = this.refs.name;
    //let hallado = cadena.match(expresion);
    
    for( let x in this.refs){
      let data = this.refs[x]
      post = x
      add = x
      if( this.refs[x].value !== '' ){
        //console.log(post)
      }
      if(data.name == 'add' && data.checked == true){
        console.log('add '+add)
        checking2 = true
      }else if(data.name == 'post' && checking2 === true){
        console.log('post '+data.checked)
        checking = data.checked
        //console.log('-check- '+checking +' -add- '+ checking2 +' -- ')
        da.push( {id:this.refs[x].value,bandera:checking,used:0} )
        //console.log(this.refs[x].value +' -- '+ this.refs[x].checked +' -- ')
      }else{
        checking2 = false
      }
    }
    data['postcheck'] = da
    if(this.state.selectedOption != null){
      data['masterToken'] = this.state.selectedOption.value;
    }
    
    if(data['masterToken'] == undefined){
      delete data['masterToken']
      delete data['master']
    }

    console.log(data)
    //console.log(this.refs)
    //data['people'] = this.state.people;
    this.props.context.store.dispatch(saveAffiliatedPostUser(data));
  }
  updateCupon(ev){
    console.log(ev.target.checked)
    if(ev.target.checked == true){
      for( let x in this.refs){
        console.log(this.refs[x])
        if(this.refs[x].name == 'post'){
          console.log(this.refs[x].disabled = true)
          this.refs[x].checked = false
        }
        if(this.refs[x].name == 'cuponPrice'){
          this.refs[x].disabled = false
        }
      }
    }else {
      for( let x in this.refs){
        console.log(this.refs[x])
        if(this.refs[x].name == 'post' || this.refs[x].name == 'add'){
          console.log(this.refs[x].disabled = false)
          this.refs[x].checked = false
        }
        if(this.refs[x].name == 'cuponPrice'){
          this.refs[x].disabled = true
          this.refs[x].value = null
        }
      }
    }
  }
  updateCuponPrice(ev){
    //console.log(ev.target.value.length)
    if(ev.target.value.length != 0){
      for( let x in this.refs){
        //console.log(this.refs[x])
        if(this.refs[x].name == 'add'){
         this.refs[x].disabled = true
          this.refs[x].checked = false
        }
      }
    }
    else if(ev.target.value.length == 0){
      for( let x in this.refs){
        //console.log(this.refs[x])
        if(this.refs[x].name == 'add'){
         this.refs[x].disabled = false
          this.refs[x].checked = false
        }
      }
    }
  }
  close(){
    if( window.Light ){
      window.Light.close();
    }
  }
  updatePeople(e){
    const idx = e.target.dataset.index;
    if( this.state.people[idx] ){
      this.state.people[idx].name = e.target.value;    
      this.setState({people:this.state.people});
    }
  }
  createPeople(){
    

    this.adults  = []
    this.childs  = []
    let people  = [];
    let adults = Array.apply(null, {length: Number(this.props.data.adults) }).map(Number.call, Number) 
    let achilds = Array.apply(null, {length: Number(this.props.data.childs) }).map(Number.call, Number) 

    let idx = 0;
    adults.forEach((item,i)=>{
      people.push({label:'adult',name:'',extra:0,index:idx})
      this.adults.push(<FormGroup   key={'input-'+idx} index={idx} i={i} item={item} label="adult" updatePeople={ this.updatePeople.bind(this) } />)
      idx++;
    })
    achilds.forEach((item,i)=>{      
      people.push({label:'child',name:'',extra:0,index:idx})
      this.childs.push(<FormGroup key={'input-'+idx} index={idx} i={i} item={item}  label="child" updatePeople={ this.updatePeople.bind(this) } />)
      idx++
    })

    this.setState({people:people});
  }

  render() {
    console.log(this.props.posts)
    let { posts } = this.props;
    return (
      <div className="padding-50">
          <form onSubmit={this.submit.bind(this)}>
              <div className="row">
                <div className="col-md-6">
                  <div className="form-group">
                    <label> Add a master passport to this new one :</label>
                    <input onChange={(e) => this.updateCheck(this)} type="checkbox" style={{marginLeft: `15px`}}  className="form-check-input" ref="master"/>
                  </div>
                </div>{console.log(this.state.selectedOption)}
                <div className="col-md-6">
                  <div className="formgroup"
                    id="masterToken" 
                    style={{display:`none`}}>
                  <Select
                    ref={'masterToken'} 
                    value={this.state.selectedOption}
                    onChange={this.handleChange}
                    options={this.state.options}
                  />
                  </div>
                </div>
                <div className="col-md-5">
                  <div className="form-group">
                    <label>(*) Name in the passport :</label>
                    <input className="form-control" type="hidden" ref="id" />
                    <input className="form-control" type="hidden" ref="affiliated" value={this.props.data.id}/>
                    <input className="form-control" type="hidden" ref="role" value="user" />
                    <input className="form-control" type="hidden" ref="owner" value={this.props.owner} /> 
                    <input className="form-control" type="hidden" ref="status" value="0" />
                    <input className="form-control" type="text" ref="name" required />
                  </div>
                </div>

                <div className="col-md-4">
                  <div className="form-group">
                    <label>(*) Email:</label>
                    {
                    this.state.selectedOption == null ?
                    <input className="form-control" id="email" type="email" ref="email" required/>
                    :
                    <input className="form-control" id="email" type="email" ref="email" value={this.state.selectedOption.email} required disabled/>
                    }
                  </div>
                </div>

                <div className="col-md-3">
                  <div className="form-group">
                    <label>Phone: (Optional) </label>
                    <input className="form-control" type="text" ref="phone" />
                  </div>
                </div>
                <div className="col-md-3">
                  <div className="form-group">
                    <label>(*) Room # </label>
                    <input className="form-control" type="text" ref="room" />
                  </div>
                </div>
                
                <div className="col-md-12">
                  <div className="form-group">
                    <label>Add the price of the Cupon </label>
                    <input className="form-control col-sm-4" type="text" name={'cuponPrice'} ref="cuponPrice" onChange={this.updateCuponPrice.bind(this)} disabled/>
                  </div>
                  <div className="form-check">
                    <input type="checkbox" className="form-check-input" name={'cupon'} defaultChecked={false} ref="cupon" onChange={this.updateCupon.bind(this)} />
                    <label className="form-check-label">Check this to send a cupon (Choose only one gift or add a price).</label>
                  </div>
                </div>
              </div>
              
             <div className="row">   
                <div className="col-md-12">
                  <label>Initial passport settings </label> 
                  <hr className="divider"/>
                </div>
                 <div className="col-md-12">
                  <label>Add Free Courtesy :</label>
                  <div className="row">
                    {
                    posts.data != null &&
                    posts.data.map((res, key) =>
                    <div className="col-sm-4" style={{paddingBottom:`15px`}} key={key}>
                      <div className="card">
                        <img className="card-img-top" src={res.featured} alt="Card image cap"/>
                        <div className="card-body">
                          <h6 className="card-title">{res.name}</h6>
                          <div className="form-check">
                            <input type="checkbox" className="form-check-input" name={'add'} defaultChecked={false} value={res.id} ref={'add-'+key}/>
                            <label className="form-check-label">Choose me to be Added :)</label>
                          </div>
                          <div className="form-check">
                            <input type="checkbox" className="form-check-input" name={'post'} defaultChecked={false} value={res.id} ref={'post-'+key}/>
                            <label className="form-check-label">Choose me to be FREE :)</label>
                          </div>
                        </div>
                      </div>
                    </div>
                    )
                    }
                  </div>
                </div>
                
              </div>

              <hr />
              <div className="row">
                <div className="col-md-12">
                  <div className="form-group form-group-button float-right">
                      <BtnSubmitLoader textBase={I18n.t('Save')} textProcess={I18n.t('Saving data')} icon="ion-edit" className="btn-success btn-sm float-right " />
                      <BtnSubmitLoader  textBase={I18n.t('Cancel')} textProcess={I18n.t('Updating info')} icon="ion-android-cancel"      className="btn-danger btn-sm hide" type="button" click={this.close.bind(this)}/>
                  </div>
                </div>
              </div>
          </form>
      </div>
    )
  }
}
export default AddForm;
