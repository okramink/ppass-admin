import React from 'react';
import BtnSubmitLoader from '../BtnSubmitLoader';
import {I18n} from 'react-redux-i18n';
import {
  save
} from '../../actions/affiliates';

class AddForm extends React.Component {
  constructor(props,context){
    super(props,context);
    this.context   = this.props.context;
    this.store     = this.context.store;
    this.state     = this.store.getState();

    this.setState({credit:1000})
  }
  componentDidMount(){
    this.setDefaultData();
  }
  setDefaultData(){
     if(  this.props.data ){
      for (let ref in this.refs) {
          this.refs[ref].value = this.props.data[ref] ? this.props.data[ref] : '';
      }
    }else{
         setTimeout(()=>{
          this.refs.email.value    = '';
          this.refs.credit.value    = '100';
        },60)
    }
  }
  submit(e){
    e.preventDefault();
    var data = {};
    for( let x in this.refs ){
      if( this.refs[x].value !== '' ){
        data[x] = this.refs[x].value;
        this.refs[x].disabled = true;
      }
    }
    this.props.context.store.dispatch(save(data));
  }
  close(){
    if( window.Light ){
      window.Light.close();
    }
  }
  getCode(e){

    if( e.target.value.length > 0 ){
      let str     = e.target.value;
      var matches = str.match(/\b(\w)/g);              // ['J','S','O','N']    
      this.refs.code.value =   matches.join('')
    }
  }
  credit(event){
    console.log(event.target.value)
    this.setState({credit: event.target.value})
  }
  render() {
    console.log(this.state)
    return (
      <div className="padding-50">
          <form onSubmit={this.submit.bind(this)}>
              <div className="row">
                <div className="col-md-12">
                  <div className="form-group">
                    <label>Nombre:</label>
                    <input className="form-control" type="hidden" ref="id" />
                    <input className="form-control" type="hidden" ref="status" value="1" />
                    <input className="form-control" type="hidden" ref="owner" value={this.state.auth.user.id} />
                    {
                    this.state.auth.user.role == 'operator' ?
                    <input className="form-control" type="text" ref="name" required onBlur={this.getCode.bind(this)} readOnly/>
                    :
                    <input className="form-control" type="text" ref="name" required onBlur={this.getCode.bind(this)}/>
                    }
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12">
                  <div className="form-group">
                    <label>Code:</label>
                    <input className="form-control" type="text" ref="code" required  readOnly/>
                  </div>
                </div>
              </div>
              <div className="row hide">
                <div className="col-md-12">
                  <div className="form-group">
                    <label>Email: (Optional) </label>
                    <input className="form-control" type="text" ref="email" />
                  </div>
                </div>
              </div>


              <div className="row">
                
                <div className="col-md-3">
                  <label>Credit :</label>
                  <div className="input-group input-group-sm">                                        
                    <select className="custom-select" ref="credit" onChange={ev => this.credit(ev)}>
                      <option value={1000}>1000</option>
                      <option value={500}>500</option>
                    </select>
                    <div className="input-group-append">
                      <span className="input-group-text">USD</span>
                    </div>
                  </div>
                </div>     
                
                <div className="col-md-3">
                  <label>Type credit :</label>
                  <div className="input-group input-group-sm">                                        
                    <select className="custom-select" ref="typecredit" value={this.state.credit} disabled>
                      <option value={1000}>1000</option>
                      <option value={500}>500</option>
                    </select>
                  </div>
                </div>  

                <div className="col-md-2">
                  <div className="form-group">
                    <label>Pass day(s):</label>
                    <input className="form-control" type="number" maxLength="2" ref="days" defaultValue="7" required/>
                  </div>
                </div>

                <div className="col-md-2 sr-only">
                  <div className="form-group">
                    <label>Adults :</label>
                    <input className="form-control" type="number" ref="adults" defaultValue="1" required/>
                  </div>
                </div>

                <div className="col-md-2 sr-only">
                  <div className="form-group">
                    <label>Children  :</label>
                    <input className="form-control" type="number" ref="childs" defaultValue="1" required/>
                  </div>
                </div>

                <div className="col-md-3 sr-only">
                  <label>Extra user Disc. :</label>
                  <div className="input-group input-group-sm">  
                    {
                    this.state.auth.user.role == 'operator' ?                                      
                    <input type="text" className="form-control" aria-label="Amount" ref="discount" defaultValue="50" readOnly/>
                    :
                    <input type="text" className="form-control" aria-label="Amount" ref="discount" defaultValue="50" />
                    }
                    <div className="input-group-append">
                      <span className="input-group-text">%</span>
                    </div>
                  </div>
                </div>   

              </div>
              
              <hr className="separator-30" />
              <div className="row">
                <div className="col-md-12">
                  <div className="form-group form-group-button float-right">
                      <BtnSubmitLoader textBase={I18n.t('Save')} textProcess={I18n.t('Saving data')} icon="ion-edit" className="btn-success btn-sm float-right" />
                      <BtnSubmitLoader  textBase={I18n.t('Cancel')} textProcess={I18n.t('Updating info')} icon="ion-android-cancel "      className="btn-danger btn-sm hide" type="button" click={this.close.bind(this)}/>
                  </div>
                </div>
              </div>
          </form>
      </div>
    )
  }
}
export default AddForm;
