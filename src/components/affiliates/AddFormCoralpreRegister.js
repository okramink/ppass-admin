import React from 'react';
import BtnSubmitLoader from '../BtnSubmitLoader';
import {I18n} from 'react-redux-i18n';
import PropTypes from 'prop-types';

import Select from 'react-select';

import {
  saveAffiliatedUserFa,
  saveAffiliatedUserFaActivated,
  saveAffiliatedUserFaActivated_edit
} from '../../actions/affiliates';

//Actions
import {
  loadPassportsMasters
} from '../../actions/passports';

const peopleData = (name, label, price, extra ) => {
    return JSON.stringify({ name, label, price, extra});
}

class FormGroup extends React.Component {

  getLabel(){
    let string = this.props.label;
    return string.charAt(0).toUpperCase() + string.slice(1) + ' '+ (this.props.i+1)
  }
  render(){
      return(
       <div className="col-md-4">
        <div className="form-group">
          <label>{ this.getLabel() }:</label>
          <input className="form-control" type="text" data-index={this.props.index} data-label={this.props.label}  onChange={this.props.updatePeople.bind(this)} />
        </div>
      </div>
    )
  }
}

class AddFormCoral extends React.Component{
  constructor(props,context) {
        super(props,context); 
        this.state = {        
          people :[],
          selectedOption: null,
          options: null
        }         
      this.adults = null;
      this.childs = null;
    
  }
  componentDidMount(){
      let options = []
      this.createPeople();
      this.props.context.store.dispatch(loadPassportsMasters(true)).then(res => {
        res.map((res) => {
          options.push({value:res.token,label:res.email+' createdAt: '+res.createdAt,email:res.email})
        })
        this.setState({options:options})})
    
  }
  submit(e){
    e.preventDefault();
    var data = {};
    for( let x in this.refs ){
      if( this.refs[x].value !== '' ){
        //console.log(this.refs[x].type)
          if(this.refs[x].type == 'checkbox'){
            data[x] = this.refs[x].checked;
          }else{
            data[x] = this.refs[x].value;
          }
          this.refs[x].disabled = true;
      }     
    }
    //console.log(data['roomupgrade'])
    if(this.state.selectedOption != null){
      data['masterToken'] = this.state.selectedOption.value;
    }
    if(data['masterToken'] == undefined){
      delete data['masterToken']
      delete data['master']
    }
    
    data['pages'] = this.props.page;
    if(this.props.data.status == 3) {
      if(this.props.params.type == 'update'){
        data['people'] = this.state.people;
        data['ownerdata'] = this.props.ownerdata.user;
        data['params'] = 'update';
        this.props.data.newinfo = JSON.stringify(data);
        //console.log(this.props.data)
        this.props.context.store.dispatch(saveAffiliatedUserFaActivated_edit(this.props.data));
      }else {
        this.props.data.newinfo = JSON.stringify(data);
        this.props.data['ownerdata'] = JSON.stringify(this.props.ownerdata.user);
        this.props.data.pages = this.props.page;
        if(this.props.data.owner.role != 'operator-adds') {
          this.props.data['affiliated'] = JSON.stringify(this.props.data.affiliated)
        }
        data['params'] = 'save';
        console.log(this.props.data)
        this.props.context.store.dispatch(saveAffiliatedUserFaActivated(this.props.data));
      }
    }else{
      console.log(data)
      data.people = this.state.people;
      data['ownerdata'] = this.props.ownerdata.user;
      this.props.context.store.dispatch(saveAffiliatedUserFa(data));
    }
  }
  close(){
    if( window.Light ){
      window.Light.close();
    }
  }
  updatePeople(e){
    const idx = e.target.dataset.index;
    if( this.state.people[idx] ){
      this.state.people[idx].name = e.target.value;    
      this.setState({people:this.state.people});
    }
  }
  handleChange = (selectedOption) => {
    this.setState({ selectedOption });
    //console.log(`Option selected:`, selectedOption);
  }
  updateCheck(e){
    //console.log(e.refs['master'].checked)
    if(e.refs['master'].checked){
      document.getElementById('masterToken').style.display = "block"
    }else{
      this.setState({selectedOption:null})
      document.getElementById('email').value = null
      document.getElementById('masterToken').style.display = "none"
    }
  }
  createPeople(){
    

    this.adults  = []
    this.childs  = []
    let people  = [];
    let aadults = Array.apply(null, {length: Number(this.props.data.adults) }).map(Number.call, Number) 
    let achilds = Array.apply(null, {length: Number(this.props.data.childs) }).map(Number.call, Number) 

    let idx = 0;
    aadults.forEach((item,i)=>{
      people.push({label:'adult',name:'',extra:0,index:idx})
      this.adults.push(<FormGroup   key={'input-'+idx} index={idx} i={i} item={item} label="adult" updatePeople={ this.updatePeople.bind(this) } />)
      idx++;
    })
    achilds.forEach((item,i)=>{      
      people.push({label:'child',name:'',extra:0,index:idx})
      this.childs.push(<FormGroup key={'input-'+idx} index={idx} i={i} item={item}  label="child" updatePeople={ this.updatePeople.bind(this) } />)
      idx++
    })

    this.setState({people:people});
  }
  render() {
    const data = this.props;
    console.log(data)
    return (
      <div className="padding-50">
          <form onSubmit={this.submit.bind(this)}>
              <div className="row">
                <div className="col-md-6">
                  <div className="form-group sr-only">
                    <label> Add a master passport to this new :</label>
                    <input onChange={(e) => this.updateCheck(this)} type="checkbox" style={{marginLeft: `15px`}}  className="form-check-input" ref="master"/>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="formgroup"
                    id="masterToken" 
                    style={{display:`none`}}>
                  <Select
                    ref={'masterToken'} 
                    value={this.state.selectedOption}
                    onChange={this.handleChange}
                    options={this.state.options}
                  />
                  </div>
                </div>
                <div className="col-md-12">
                  <div className="form-group">
                    <label> Active Option (Room Upgrade) :</label>
                    <input type="checkbox" style={{marginLeft: `15px`}}  className="form-check-input" ref="roomupgrade" defaultChecked={data.data && data.data.roomupgrade == "true" ? true : false} disabled={data.ownerdata.user.role != 'operator-adds'  ? false : true}/>
                  </div>
                </div>
                <div className="col-md-5">
                  <div className="form-group">
                    <label>(*) Name in the passport :</label>
                    <input className="form-control" type="hidden" ref="id" />
                    <input className="form-control" type="hidden" ref="affiliated" value={this.props.data.id}/>
                    <input className="form-control" type="hidden" ref="role" value="user" />
                    <input className="form-control" type="hidden" ref="owner" value={this.props.owner ? this.props.owner : data.ownerdata.user} /> 
                    <input className="form-control" type="text" ref="name" defaultValue={data.params.type != 'create' ? data.data.name : ''} required disabled={data.ownerdata.user.role != 'operator-adds'  ? false : data.params && data.data.name ? true : false}/>
                  </div>
                </div>

                <div className="col-md-4">
                  <div className="form-group">
                    <label>{data.params.type == 'save' && '(*)'} Email:</label>
                    {
                    this.state.selectedOption == null ?
                    <input className="form-control" id="email" type="email" ref="email" defaultValue={data.params && data.data.email} required={data.params.type == 'save' ? true : false} disabled={data.ownerdata.user.role != 'operator-adds'  ? false : false}/>
                    :
                    <input className="form-control" id="email" type="email" ref="email" value={this.state.selectedOption.email} required disabled/>
                    }
                  </div>
                </div>

                <div className="col-md-3 sr-only">
                  <div className="form-group">
                    <label>Phone: (Optional) </label>
                    <input className="form-control" type="text" ref="phone" />
                  </div>
                </div>
                <div className="col-md-3">
                  <div className="form-group">
                    <label>Room # </label>
                    <input className="form-control" type="text" ref="room" defaultValue={data.params && data.data.room} disabled={data.ownerdata.user.role != 'operator-adds' ? false : data.params && data.data.room ? true : false}/>
                  </div>
                </div>
                <div className="col-md-3">
                  <div className="form-group">
                    <label>(*) #Intern Control </label>
                    <input className="form-control" type="text" ref="icontrol" defaultValue={data.params && data.data.icontrol} required={data.params ? true : false} disabled={data.ownerdata.user.role != 'operator-adds'  ? false : data.params && data.data.icontrol ? true : false}/>
                  </div>
                </div>
                
                <div className="col-md-3">
                  <div className="form-group">
                    <label>(*) TypeCredit </label> 
                    <input type="text" className="form-control" aria-label="typecredit" ref="typecredit" defaultValue={this.props.data.typecredit} readOnly />
                  </div>
                </div>
              </div>
              
              
              <div className="row sr-only">
                
                {this.adults}

                {this.childs}


              </div>




              
             <div className="row">   
                <div className="col-md-12">
                  <label>Initial passport settings </label> 
                  <hr className="divider"/>
                </div>
                 <div className="col-md-3">
                  <label>Credit :</label>
                  <div className="input-group input-group-sm">                                        
                    <input type="text" className="form-control" aria-label="credit" ref="credit" defaultValue={this.props.data.credit} readOnly />
                    <div className="input-group-append">
                      <span className="input-group-text">USD</span>
                    </div>
                  </div>
                </div>
                <div className="col-md-2">
                  <div className="form-group">
                    <label>Pass Days:</label>
                    <input className="form-control" type="number" maxLength="2" ref="days" defaultValue={this.props.data.days} required readOnly />
                  </div>
                </div>

                <div className="col-md-2 sr-only">
                  <div className="form-group">
                    <label>Adults :</label>
                    <input className="form-control" type="number" ref="adults" defaultValue={this.props.data.adults} required readOnly />
                  </div>
                </div>

                <div className="col-md-2 sr-only">
                  <div className="form-group">
                    <label>Children  :</label>
                    <input className="form-control" type="number" ref="childs" defaultValue={this.props.data.childs} required readOnly />
                  </div>
                </div>

                <div className="col-md-3 sr-only">
                  <label>Extra user Disc. :</label>
                  <div className="input-group input-group-sm">                                        
                    <input type="text" className="form-control" aria-label="Amount" ref="discount" defaultValue={this.props.data.discount} readOnly/>
                    <div className="input-group-append">
                      <span className="input-group-text">%</span>
                    </div>
                  </div>
                </div>
              </div>
               

              <hr />
              <div className="row">
                <div className="col-md-12">
                  <div className="form-group form-group-button float-right">
                      {
                      data.params.type != 'create' ?
                      data.params.type == 'update' ? 
                        data.aproved == 'true' ?
                        <BtnSubmitLoader textBase={I18n.t('Edit')} textProcess={I18n.t('Saving data')} icon="ion-edit" className="btn-success btn-sm float-right " />
                        :
                        data.aproved == undefined ?
                          <BtnSubmitLoader textBase={I18n.t('Edit')} textProcess={I18n.t('Saving data')} icon="ion-edit" className="btn-success btn-sm float-right " />
                          :
                          <BtnSubmitLoader textBase={I18n.t('Save')} textProcess={I18n.t('Saving data')} icon="ion-edit" className="btn-success btn-sm float-right " />
                      :
                      <BtnSubmitLoader textBase={I18n.t('Save')} textProcess={I18n.t('Saving data')} icon="ion-edit" className="btn-success btn-sm float-right " />
                      :
                      <BtnSubmitLoader textBase={I18n.t('Save')} textProcess={I18n.t('Saving data')} icon="ion-edit" className="btn-success btn-sm float-right " />
                      }
                      <BtnSubmitLoader  textBase={I18n.t('Cancel')} textProcess={I18n.t('Updating info')} icon="ion-android-cancel"      className="btn-danger btn-sm hide" type="button" click={this.close.bind(this)}/>
                  </div>
                </div>
              </div>
          </form>
      </div>
    )
  }
}
export default AddFormCoral;
