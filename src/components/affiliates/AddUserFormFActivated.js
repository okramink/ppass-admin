import React from 'react';
import BtnSubmitLoader from '../BtnSubmitLoader';
import {I18n} from 'react-redux-i18n';
import PropTypes from 'prop-types';
import moment from 'moment';

import Select from 'react-select';

import {
  saveAffiliatedUserFActivated
} from '../../actions/affiliates';

//Actions
import {
  loadPassportsMasters
} from '../../actions/passports';

import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import { isNull } from 'util';

const peopleData = (name, label, price, extra ) => {
    return JSON.stringify({ name, label, price, extra});
}

class FormGroup extends React.Component {

  getLabel(){
    let string = this.props.label;
    return string.charAt(0).toUpperCase() + string.slice(1) + ' '+ (this.props.i+1)
  }
  render(){
      return(
       <div className="col-md-4">
        <div className="form-group">
          <label>{ this.getLabel() }:</label>
          <input className="form-control" type="text" data-index={this.props.index} data-label={this.props.label}  onChange={this.props.updatePeople.bind(this)} />
        </div>
      </div>
    )
  }
}

class AddUserFormFActivated extends React.Component{
  constructor(props,context) {
        super(props,context); 
        this.handleDayClick = this.handleDayClick.bind(this);
        this.handleDayClickOut = this.handleDayClickOut.bind(this);
        this.state = {        
          people :[],
          selectedOption: null,
          options: null,
          addEmail: '',
          icontrol: '',
          Lastname: '',
          selectedDaysIn: undefined,
          selectedDaysOut: undefined,
          alertIn: false,
          alertOut: false,
          countdays: null,
          typeCredit: null
        }         
      this.adults = null;
      this.childs = null;
    
  }
  componentDidMount(){
      let options = []
      this.createPeople();
      this.props.context.store.dispatch(loadPassportsMasters(false, 'cbcfa')).then(res => {
        console.log(res)
        res.map((res) => {
          options.push({value:res.token,label:res.email+' createdAt: '+res.createdAt,email:res.email})
        })
        this.setState({options:options})})
    
  }
  submit(e){
    e.preventDefault();
    var data = {};
    for( let x in this.refs ){
      if( this.refs[x].value !== '' ){
        //console.log(this.refs[x].type)
          if(this.refs[x].type == 'checkbox'){
            data[x] = this.refs[x].checked;
          }else{
            if(this.refs[x].value == undefined){
              data[x] = this.refs[x].value;
            }else {
              data[x] = this.refs[x].value.toUpperCase();
            }
          }
          this.refs[x].disabled = true;
      }     
    }
    //console.log(data['roomupgrade'])
    if(this.state.selectedOption != null){
      data['masterToken'] = this.state.selectedOption.value;
    }
    if(data['masterToken'] == undefined){
      delete data['masterToken']
      delete data['master']
    }
    //console.log(data)
    if(this.state.selectedDaysIn && this.state.selectedDaysOut){
      data['people'] = this.state.people;
      this.setState({alertIn: false, alertOut: false})
      data['checkIn'] = this.state.selectedDaysIn.toString()
      data['checkOut'] = this.state.selectedDaysOut.toString()
      this.props.context.store.dispatch(saveAffiliatedUserFActivated(data));
      //console.log(this.state.selectedDaysIn)
    }else{
      if(this.state.selectedDaysIn == undefined){
        this.setState({alertIn: true})
      }else {
        this.setState({alertOut: true})
      }
    }
  }
  close(){
    if( window.Light ){
      window.Light.close();
    }
  }
  updatePeople(e){
    const idx = e.target.dataset.index;
    if( this.state.people[idx] ){
      this.state.people[idx].name = e.target.value;    
      this.setState({people:this.state.people});
    }
  }
  handleChange = (selectedOption) => {
    this.setState({ selectedOption });
    //console.log(`Option selected:`, selectedOption);
  }
  updateCheck(e){
    //console.log(e.refs['master'].checked)
    if(e.refs['master'].checked){
      document.getElementById('masterToken').style.display = "block"
    }else{
      this.setState({selectedOption:null})
      document.getElementById('email').value = null
      document.getElementById('masterToken').style.display = "none"
    }
  }
  createPeople(){
    this.adults  = []
    this.childs  = []
    let people  = [];
    let aadults = Array.apply(null, {length: Number(this.props.data.adults) }).map(Number.call, Number) 
    let achilds = Array.apply(null, {length: Number(this.props.data.childs) }).map(Number.call, Number) 

    let idx = 0;
    aadults.forEach((item,i)=>{
      people.push({label:'adult',name:'',extra:0,index:idx})
      this.adults.push(<FormGroup   key={'input-'+idx} index={idx} i={i} item={item} label="adult" updatePeople={ this.updatePeople.bind(this) } />)
      idx++;
    })
    achilds.forEach((item,i)=>{      
      people.push({label:'child',name:'',extra:0,index:idx})
      this.childs.push(<FormGroup key={'input-'+idx} index={idx} i={i} item={item}  label="child" updatePeople={ this.updatePeople.bind(this) } />)
      idx++
    })

    this.setState({people:people});
  }
  createEmail(ev){
    //console.log(ev.target.value)
    let email = ev.target.value.replace(/ /g,'')
    this.setState({addEmail:email + '@preferencepass.com'})
  }
  emptyValue(ev){
    //console.log(ev.target.value)
    let email = ev.target.value.replace(/ /g,'')
    this.setState({icontrol: email})
  }
  emptyLastname(ev){
    //console.log(ev.target.value)
    let lastname = ev.target.value.replace(/ /g,'')
    this.setState({lastname: lastname})
  }
  handleDayClick(day) {
    console.log(moment(day).date())
    this.setState({selectedDaysIn: day, selectedDaysOut: null, countdays: null});
  }
  handleDayClickOut(day) {
    let deff = moment(day).diff(this.state.selectedDaysIn, 'days')
    console.log(deff)
    if(deff > 0){
      if(deff < 5){
        this.setState({selectedDaysOut: day, countdays: deff, typeCredit: 500});
      }else{
        this.setState({selectedDaysOut: day, countdays: deff, typeCredit: 1000});
      }
    }else{
      this.setState({selectedDaysOut: null, countdays: '', typeCredit: ''});
    }
  }
  render() {
    //const { selectedOption } = this.state.selectedOption;
    //console.log(this.state.options)
    let daysOfWeek = [0, 1, 2, 3, 4, 5, 6]
    let before = new Date()
    let disabled = true
    if(this.state.selectedDaysIn){
      daysOfWeek = []
      disabled = false
      before = this.state.selectedDaysIn
    }
    return (
      <div className="padding-50">
          <form onSubmit={this.submit.bind(this)}>
              <div className="row">
                <div className="col-md-12">
                  <div className="form-group">
                    <label> Active Option (Room Upgrade) :</label>
                    <input type="checkbox" style={{marginLeft: `15px`}}  className="form-check-input" ref="roomupgrade"/>
                  </div>
                </div>
                <div className="col-md-3">
                  <div className="form-group">
                    <label>(*) Name in the passport :</label>
                    <input className="form-control" type="hidden" ref="id" />
                    <input className="form-control" type="hidden" ref="affiliated" value={this.props.data.id}/>
                    <input className="form-control" type="hidden" ref="role" value="user" />
                    <input className="form-control" type="hidden" ref="owner" value={this.props.owner} /> 
                    <input className="form-control" type="hidden" ref="status" value="0" />
                    <input style={{textTransform: `uppercase`}} className="form-control" type="text" ref="name" required />
                  </div>
                </div>

                <div className="col-md-3">
                  <div className="form-group">
                    <label>(*) LastName in the passport :</label>
                    <input value={this.state.lastname} onChange={e => this.emptyLastname(e)} style={{textTransform: `uppercase`}} className="form-control" type="text" ref="lastname" required />
                  </div>
                </div>

                <div className="col-md-3">
                  <div className="form-group">
                    <label>(*) Email:</label>
                    {
                    this.state.selectedOption == null ?
                    <input className="form-control" id="email" type="email" ref="email" value={this.state.addEmail} required readOnly/>
                    :
                    <input className="form-control" id="email" type="email" ref="email" value={this.state.selectedOption.email} required disabled/>
                    }
                  </div>
                </div>

                <div className="col-md-3 sr-only">
                  <div className="form-group">
                    <label>Phone: (Optional) </label>
                    <input className="form-control" type="text" ref="phone" />
                  </div>
                </div>
                <div className="col-md-3">
                  <div className="form-group">
                    <label>Room # </label>
                    <input className="form-control" type="text" ref="room" />
                  </div>
                </div>
                <div className="col-md-3">
                  <div className="form-group">
                    <label>(*) #Intern Control </label>
                    <input style={{textTransform: `uppercase`}} value={this.state.icontrol} onChange={e => this.emptyValue(e)} className="form-control" type="text" ref="icontrol" onKeyUp={ev => this.createEmail(ev)} required />
                  </div>
                </div>
                
                <div className="col-md-3">
                  <div className="form-group">
                    <label>(*) TypeCredit </label> 
                    <input type="text" className="form-control" aria-label="typecredit" ref="typecredit" value={this.state.typeCredit && this.state.typeCredit} readOnly />
                  </div>
                </div>
                <div className="col-md-3">
                  <div className="form-group">
                    <label>(*) Check In</label><br/>
                    <DayPickerInput
                      dayPickerProps={{
                        month: new Date(),
                        showWeekNumbers: true,
                        todayButton: 'Today',
                      }}
                      dayPickerProps={{disabledDays:{before: new Date()}}}
                      onDayChange={this.handleDayClick}
                    required/>
                    <label style={{color: 'red', fontSize: '9pt'}}>{this.state.alertIn && 'select a date'}</label>
                  </div>
                </div>
                <div className="col-md-3">
                  <div className="form-group">
                    <label>(*) Check Out</label><br/>
                    <DayPickerInput
                      inputProps={{ disabled: disabled }}
                      dayPickerProps={{
                        month: new Date(),
                        showWeekNumbers: true,
                        todayButton: 'Today',
                        disabledDays: {
                          daysOfWeek: daysOfWeek,
                        },
                      }}
                      dayPickerProps={{disabledDays:{before: before}}}
                      onDayChange={this.handleDayClickOut}
                      />
                    <label style={{color: 'red', fontSize: '9pt'}}>{this.state.alertOut && 'select a different date'}</label>
                  </div>
                </div>
                <div className="col-md-4">
                  <div className="form-group">
                    <label> Add a master passport to this new :</label>
                    <input onChange={(e) => this.updateCheck(this)} type="checkbox" style={{marginLeft: `15px`}}  className="form-check-input" ref="master"/>
                  </div>
                </div>
                <div className="col-md-5">
                  <div className="formgroup"
                    id="masterToken" 
                    style={{display:`none`}}>
                  <Select
                    ref={'masterToken'} 
                    value={this.state.selectedOption}
                    onChange={this.handleChange}
                    options={this.state.options}
                  />
                  </div>
                </div>
              </div>
              
              
              <div className="row sr-only">
                
                {this.adults}

                {this.childs}


              </div>




              
             <div className="row">   
                <div className="col-md-12">
                  <label>Initial passport settings </label> 
                  <hr className="divider"/>
                </div>
                 <div className="col-md-3">
                  <label>Credit :</label>
                  <div className="input-group input-group-sm">                                        
                    <input type="text" className="form-control" aria-label="credit" ref="credit" value={this.state.typeCredit && this.state.typeCredit} readOnly />
                    <div className="input-group-append">
                      <span className="input-group-text">USD</span>
                    </div>
                  </div>
                </div>
                <div className="col-md-2">
                  <div className="form-group">
                    <label>Pass Days:</label>
                    <input className="form-control" type="number" maxLength="2" ref="days" value={isNull(this.state.countdays) ? '' : this.state.countdays} required readOnly />
                  </div>
                </div>

                <div className="col-md-2 sr-only">
                  <div className="form-group">
                    <label>Adults :</label>
                    <input className="form-control" type="number" ref="adults" defaultValue={this.props.data.adults} required readOnly />
                  </div>
                </div>

                <div className="col-md-2 sr-only">
                  <div className="form-group">
                    <label>Children  :</label>
                    <input className="form-control" type="number" ref="childs" defaultValue={this.props.data.childs} required readOnly />
                  </div>
                </div>

                <div className="col-md-3 sr-only">
                  <label>Extra user Disc. :</label>
                  <div className="input-group input-group-sm">                                        
                    <input type="text" className="form-control" aria-label="Amount" ref="discount" defaultValue={this.props.data.discount} readOnly/>
                    <div className="input-group-append">
                      <span className="input-group-text">%</span>
                    </div>
                  </div>
                </div>
              </div>
               

              <hr />
              <div className="row">
                <div className="col-md-12">
                  <div className="form-group form-group-button float-right">
                      <BtnSubmitLoader textBase={I18n.t('Save')} textProcess={I18n.t('Saving data')} icon="ion-edit" className="btn-success btn-sm float-right " />
                      <BtnSubmitLoader  textBase={I18n.t('Cancel')} textProcess={I18n.t('Updating info')} icon="ion-android-cancel"      className="btn-danger btn-sm hide" type="button" click={this.close.bind(this)}/>
                  </div>
                </div>
              </div>
          </form>
      </div>
    )
  }
}
export default AddUserFormFActivated;
