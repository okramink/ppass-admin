import React from 'react';

export default class BtnSubmitLoader extends React.Component{
  render(){

    this.type = this.props.type ? this.props.type : "submit";
    if(this.props.state){
      return(
         <button type="submit" className="btn btn-primary btn-sm" ref="bt_send" disabled>
          <div className="sk-circle">
            <div className="sk-circle1 sk-child"></div>
            <div className="sk-circle2 sk-child"></div>
            <div className="sk-circle3 sk-child"></div>
            <div className="sk-circle4 sk-child"></div>
            <div className="sk-circle5 sk-child"></div>
            <div className="sk-circle6 sk-child"></div>
            <div className="sk-circle7 sk-child"></div>
            <div className="sk-circle8 sk-child"></div>
            <div className="sk-circle9 sk-child"></div>
            <div className="sk-circle10 sk-child"></div>
            <div className="sk-circle11 sk-child"></div>
            <div className="sk-circle12 sk-child"></div>
          </div>
          <i className={this.props.icon}/> {this.props.textProcess} &nbsp;
        </button>
        )
    }else{
      return (<button type={this.type} className={'btn btn-submit btn-primary ' + this.props.className } ref="bt_send" onClick={this.props.click} >
              <i className={this.props.icon}/> {this.props.textBase} &nbsp;
           </button>
      )
    }
  }
}