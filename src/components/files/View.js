import React from 'react';

class FileForm extends React.Component {
  constructor(props) {
    super(props);
    this.store = this.props.context.store;
    this.state = this.props.context.store.getState();
    this._fd   = {};
    this.index = 0;
  }
  componentWillUnmount() {
    this.unsuscribe();
  }
  componentDidMount(){
    this.unsuscribe = this.store.subscribe(() => {
      this.setState(this.store.getState());
    });
  }
  close(){
    if( window.Light ){
      window.Light.close();
    }
  }
  toggle(){
    if (this.refs.video.paused === true) {
      this.refs.video.play();
    } else {
      this.refs.video.pause();
    }
  }
  render() {   
    switch(this.props.type){
      case 'image/png': case 'image/jpg': case 'image/jpeg': 
        return (
          <div className="">
              <img src={this.props.src} width="100%" height="auto" alt="" />                  
          </div>
        )     
      case 'video/mp4':
        return (
          <div className="">
            <video ref="video"  autoPlay controls className="preview" onClick={this.toggle.bind(this)} ><source src={this.props.src} type="video/mp4" /> Your browser does not support the video tag.</video>
          </div>
        )
      default:
        return null;
    }   
  }
}
export default FileForm;