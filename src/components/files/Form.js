
import React   from 'react';
import Globals from '../../globals';
import {Translate} from 'react-redux-i18n';

import {
  loadFiles,
  setTemporal,
  setTemporalProgress,
  setTemporalUploaded,
  removeTemporal
} from '../../actions/files';

class Item extends React.Component {
  componentDidMount() {
    this.refs['progress'+this.props.idx].style.display = 'none';
  }
  componentDidUpdate(prevProps, prevState) {
    if(!this.props.item.percent){
       this.refs['progress'+this.props.idx].style.display = 'none';
    }else{
      this.refs['progress'+this.props.idx].style.display = 'block';
      this.refs['progress'+this.props.idx].style.width = this.props.item.percent + '%';
    }
  }
  size(bytes){

   if(bytes === 0) return '0 Bytes';
   var k = 1000,
       dm = 2,
       sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
       i = Math.floor(Math.log(bytes) / Math.log(k));
      return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
  }
  remove(e){
    let idx = e.target.dataset.idx;
    this.props.store.dispatch(removeTemporal(idx))
  }
  render(){
    return(
      <li className="item">
        <h4>{this.props.item.name}</h4>
        <h5>{this.props.item.type}</h5>
        <cite>{this.size(this.props.item.size)}</cite>
        <i className="ion-ios-close-outline remove float-right" onClick={this.remove.bind(this)} data-idx={this.props.idx} />
         <div className="progresstrip" ref={'progress'+this.props.idx} >
           <div className="progress-inner"></div>
         </div>
      </li>
    )
  }
}

class FileForm extends React.Component {
  constructor(props) {
    super(props);
    this.store = this.props.context.store;
    this.state = this.props.context.store.getState();
    this._fd   = {};
    this.index = 0;
  }

  componentWillUnmount() {
    this.unsuscribe();

  }
  componentDidMount(){
    this.unsuscribe = this.store.subscribe(() => {
      this.setState(this.store.getState());
    });
    this.dragInit();
  }

  dragInit(){

    // file area input
    var filedrag    = this.refs.dragfiles;
    var ifile       = this.refs.ifile;

    ifile.addEventListener("change", this.onDropFile.bind(this), false);

    // file drop
    filedrag.addEventListener("dragover",  this.onFileDragOver.bind(this), false);
    filedrag.addEventListener("dragleave",  this.onFileDragOver.bind(this), false);
    filedrag.addEventListener("drop",  this.onDropFile.bind(this), false);

  }
  onDropFile(e){
    // cancel event and hover styling
    this.onFileDragOver(e);

    // fetch FileList object
    var files = e.target.files || e.dataTransfer.files;

    if(files.length < 4){
      for( let x in files){
        if( typeof files[x]  === 'object'){
          this.store.dispatch(setTemporal(files[x]));
        }
      }
    }else{
      window.bootbox.alert('You only can upload 3 files at time!');
    }
  }

  onFileDragOver(e) {
    e.stopPropagation();
    e.preventDefault();

    if( !e.target.classList.contains('dragfilesarea-hover') ){
      if( e.type === "dragover" ){
        this.refs.dragfiles.classList.add('dragfilesarea-hover');
      }else{
        this.refs.dragfiles.classList.remove('dragfilesarea-hover');
      }
    }
  }

  uploadProgress (e,idx) {
    if (e.lengthComputable) {
      var percentComplete = Math.round(e.loaded * 100 / e.total);
      this.store.dispatch(setTemporalProgress(idx,percentComplete));
    }else {
      console.log('unable to compute');
    }
  }
  submit(e){
    e.preventDefault();
    if( this.state.files.temporal.length > 0 ){
      this.upload();
    }
  }
  upload(){
    var file = this.state.files.temporal[this.index];
    var xhr = new XMLHttpRequest();

    xhr.upload.addEventListener("progress",(e)=>{
        this.uploadProgress(e,this.index);
    },false);

    if (xhr.upload) {
      var formData = new FormData();
      formData.set("idx", this.index);
      formData.set("file", file);

      // start upload
      xhr.open("POST",Globals.api+'/files/upload', true);
      xhr.setRequestHeader("Authorization", `bearer ${ window.localStorage.getItem('appToken') }`);
      xhr.send(formData);
      xhr.ontimeout = function (e) {
        // XMLHttpRequest timed out. Do something here.
        console.log('timeout->',e);
      };
      xhr.onprogress = function () {
          console.log('LOADING', xhr.readyState);
      };
      xhr.onreadystatechange = (()=>{

        if (xhr.readyState === 4 && xhr.status === 200 ){
          if(xhr.responseText){
            var data     = JSON.parse(xhr.responseText+'');
            this.index = data.idx + 1;
            if(this.index < this.state.files.temporal.length ){
              this.upload();
            }

            if(this.index === this.state.files.temporal.length ){
              this.store.dispatch(setTemporalUploaded());
              this.store.dispatch(loadFiles(this.props.filesData));
              this.close();
            }
          }
        }
      });

    }
  }
  close(){
    if( window.Light ){
      window.Light.close();
    }
  }
  render() {
    let List = [];
    this.state.files.temporal.forEach((item, index) => {
      List.push(<Item key={'rowFiledata-'+index} idx={index} item={item} {...this.props} store={this.store} />);
    });
    return (
      <div className="padding-50 fileForm">
          <form onSubmit={this.submit.bind(this)}>

            <div className="row">
                <div className="col-md-12">
                  <input type="file"  className="hide" name="files" id="files" ref="ifile" multiple/>
                  <div className="form-group dragfilesarea" ref="dragfiles"  >
                        <div className="dragfiles">
                           <label htmlFor="files"><Translate value="Drop files here (3 files max)"/> </label>
                            <cite className="hide">500 GB</cite>
                        </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12">
                  <ul className="nav nav-tabs">
                    <li className="nav-item">
                      <a className="nav-link active" data-toggle="tab" href="#tab1" role="tab"><Translate value="Files"/></a>
                    </li>
                     <li className="nav-item">
                      <a className="nav-link hide " data-toggle="tab" href="#tab2" role="tab"><Translate value="Otros"/></a>
                    </li>
                  </ul>
                  <div className="tab-content">
                    <div className="tab-pane active" id="tab1" role="tabpanel">
                        {List}
                    </div>
                    <div className="tab-pane" id="tab2" role="tabpanel">profile</div>
                  </div>
                </div>
              </div>
              <hr className="separator-10" />
              <div className="row">
                <div className="col-md-12">
                  <div className="form-group form-group-button float-right">
                      <button className="btn btn-success btn-sm btn-submit"><i className="ion-ios-upload-outline" /> <Translate value="Upload"/>   </button>
                      <button className="btn btn-danger btn-sm btn-submit" type="button" onClick={this.close.bind(this)}><i className="ion-ios-close" /> <Translate value="Cancel"/> </button>
                  </div>
                </div>
              </div>
          </form>
      </div>
    )
  }
}
export default FileForm;
