import React from 'react';

class Result extends React.Component {

  render(){
    return (<a  data-label={this.props.item.title} data-id={this.props.item.id} onClick={this.props.select } to="#" className="list-group-item list-group-item-action">{this.props.item.title}</a>)
  }
}

class SearchInput extends React.Component {
  constructor(props) {
    super(props);
     this.enable = 'hide';

  }
  componentDidMount() {
    if(this.props.value){
      this.refs.keyword.value = this.props.label;
      this.refs.val.value     = this.props.value;
      this.value              = this.props.value;
      this.label              = this.props.label;
    }
  }
  focus(){
    this.refs.results.classList.remove('hide');
  }
  blur(){
    setTimeout(()=>{
      this.refs.results.classList.add('hide');
    },100)
  }
  select(e){
    this.refs.keyword.value = e.target.dataset.label;
    this.refs.val.value     = e.target.dataset.id;
    this.value              = e.target.dataset.id;
    this.label              = e.target.dataset.label;
  }
  keyup(e){
    let keyword = e.target.value;
    this.props.request({keyword:keyword,rows:3});
  }
  render(){

    let items = [];
      this.props.data.forEach((item,i)=>{
          items.push(<Result  key={'result-search-'+i} item={item} select={this.select.bind(this)}  />)
      })
    return (
        <div className="row search-input">
          <div className="col-md-12 form-group search-input-container">
              <input type="hidden" ref="val" />
              <input ref="keyword" className="form-control" placeholder={this.props.placeholder} onFocus={this.focus.bind(this)} onBlur={this.blur.bind(this)} onKeyUp={this.keyup.bind(this)}/>
          </div>
          <div ref="results" className={`col-md-12 result-input-search ${this.enable}`}>
              <div className="list-group">
                {items}
              </div>
          </div>
        </div>
        )
  }
}
export default SearchInput;
