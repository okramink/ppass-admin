import React                  from 'react';
// import ReactDOM               from 'react-dom';
import PropTypes              from 'prop-types';
// import {I18n, Translate}      from 'react-redux-i18n';
import Select from 'react-select';


// import Globals  from '../../globals';


class PostsTags extends React.Component {

  static get contextTypes() {
    return {
        router: PropTypes.object.isRequired,
        store: PropTypes.object.isRequired
    }
  }
  constructor(props) {
  	super(props);
  	this.options = []
  }

  render(){
  	return(
  		<div className="card ">
          <div className="card-header"> Tags </div>
          <div className="card-body">
            <Select.Creatable
            	multi
            	name     	= "tagsInput"
            	placeholder = "Search tags"
                disabled 	= {this.props.disabled}
                options  	= {this.options}
                value 	 	= {this.props.value}
                onChange	= {this.props.onChange}
              />
          </div>
      </div>
  	)
  }

 }


 export default  PostsTags;
