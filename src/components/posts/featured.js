import React                  from 'react';
import ReactDOM               from 'react-dom';
import PropTypes              from 'prop-types';
import {I18n}      from 'react-redux-i18n';
// import Select from 'react-select';


// import Globals from '../../globals';

import UploadFile from '../../components/posts/upload';

//Actions
import {
  removeImageFeature
} from '../../actions/posts';

class UploadLink extends React.Component {
  render() {
    // if ( (this.props.featured !== '' && this.props.featureImage === true) || this.props.selectedImage) {
    //   return(
    //      <a href="#" onClick={this.props.remove}>Remove image</a>
    //   )
    // } else {
    //   return(
    //      <a href="#" onClick={this.props.open}>Upload image</a>
    //   )
    // }
    if (this.props.featureImage === true) {
      if (this.props.featured !== '') {
        return(
          <a to="#" onClick={this.props.remove}>Remove image</a>
        );
      } else {
        return(
          <a to="#" onClick={this.props.open}>Upload image</a>
        );
      }
      // return(
      //   <div ref="image-feature-container" className="image-feature-container">
      //     <img src={this.state.posts.featured} />
      //   </div>
      // );
    } else {
      if (this.props.multi !== true) {
        if (this.props.selectedImage) {
          return(
              <a to="#" onClick={this.props.remove}>Remove image</a>
            );
        } else {
          return(
            <a to="#" onClick={this.props.open}>Upload image</a>
          );
        }
      } else {
        if (this.props.selectedImage) {
          return(
              <a to="#" onClick={this.props.open}>Add More</a>
            );
        } else {
          return(
            <a to="#" onClick={this.props.open}>Upload images</a>
          );
        }
      }
    }
    return null;
  }
}
class ImageComponent extends React.Component {
  static contextTypes = {
    router: PropTypes.object.isRequired,
    store:  PropTypes.object.isRequired
  }

  constructor(props,context) {
  	super(props,context);
    this.context = context;
    this.store   = this.context.store;
    this.state   = this.store.getState();
    this.getFeatureImg = this.getFeatureImg.bind(this);
    this.returnImages  = this.returnImages.bind(this);
    this.deleteImg     = this.deleteImg.bind(this);

    this.phimage = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAACWCAYAAADwkd5lAAARKUlEQVR4Xu2ca6xdQxTHV7XoVRXcq6UIglJB1FtI0CJEPELaSCTSDxqhHqEUFYJ4R4jEB6pfxFv4IvFIvOOVet5qaYMKorTVlmhLaams4Vy37emdvffZs+f1O1/kdmbPrPmttec/a805BvX29q7766+/pKurS4YNGyZDhgwRPhCAAAQgAIENCaxdu1ZWrVolv//+uwwePFgGLVy4cN3IkSNl2bJlsnTpUtO/p6dHuru7TQc+EIAABCCQLwFNMNrpw5IlS/4VkFGjRvXRUWVRIdEHhg8fboRk2223zZceK4cABCCQIYFffvnF6MCKFSuMDmhioZWq1ufHH3/cWED6c7INkCFTlgwBCEAgWQJlEgirgLQobSqFocSVbByxMAhAIBMCVff3wgLSn2MZhcqEP8uEAAQgEB2BTitMlQSEEld0cYLBEIAABAyBOhOAjgWEEhdRCQEIQCBsAlVLVLZV1SYglLhsqGmHAAQg0CyBTktUNmudCAglLht22iEAAQi4IVBnicpmoXMBocRlcwHtEIAABDoj4KpEZbOqMQGhxGVzBe0QgAAEyhFwXaKyWeNFQChx2dxCOwQgAIH2BJosUdl84F1AKHHZXEQ7BCCQOwFfJSob92AEhBKXzVW0QwACuRHwXaKy8Q5SQChx2dxGOwQgkCqBkEpUNsbBCwglLpsLaYcABGInEGqJysY1GgGhxGVzJe0QgEBsBEIvUdl4RikglLhsbqUdAhAIlUBMJSobw+gFhBKXzcW0QwACvgnEWqKycUtGQChx2VxNOwQg0DSB2EtUNl5JCgglLpvbaYcABFwRSKlEZWOUvIBQ4rKFAO0QgECnBFItUdm4ZCMglLhsoUA7BCBQlkDqJSobjywFhBKXLSxohwAENkUgpxKVLQqyFxBKXLYQoR0CEMi1RGXzPALShhAnDFvY0A6BPAjkXqKyeRkBsRAigGwhRDsE0iLAAbK4PxGQgqxIYQuCohsEIiTA+13NaQhIBW6cUCpA4xEIBEiACkNnTkFAOuMnBGCHAHkcAg0T4ABYH3AEpCaWpMA1gWQYCDggwPvpAKqIICAOuHLCcQCVISFQgQAVggrQSjyCgJSAVaUrAVyFGs9AoDoBDnDV2ZV9EgEpS6xif1LoiuB4DAIFCPB+FYDkoAsC4gCqbUhOSDZCtEOgGAEy/GKcXPVCQFyRLTguL0BBUHSDwH8EOICFEwoISCC+IAUPxBGYESQB3o8g3cK3sEJ0CyesEL2CTT4IkKH7oF58TjKQ4qy89OQF8oKdST0S4ADlEX7JqRGQksB8dSeF90WeeZsgQHw3Qbn+ORCQ+pk6H5ETmnPETNAQATLshkA7mgYBcQS2qWF5AZsizTx1EeAAVBdJ/+MgIP59UIsFlABqwcggjggQn47Aeh4WAfHsABfTc8JzQZUxqxAgQ65CLZ5nEJB4fFXJUl7gSth4qAMCHGA6gBfZowhIZA6rai4lhKrkeK4IAeKrCKX0+iAg6fnUuiJOiFZEdChIgAy3IKhEuyEgiTq26LLYAIqSol+LAAcQYqFFAAEhFgwBShAEwkAEiA/iox0BBIS42IgAJ0yCokWADJVYGIgAAkJ8DEiADSS/AOEAkZ/Pq64YAalKLrPnKGGk7XD8m7Z/Xa0OAXFFNuFxOaGm41wyzHR86WMlCIgP6gnNyQYUnzM5AMTns1AtRkBC9UxkdlECCdth+Cds/8RqHQISq+cCtpsTbjjOIUMMxxcpWoKApOjVgNbEBta8MxDw5pnnOiMCkqvnG143JRS3wOHrli+jtyeAgBAZjRPghFwfcjK8+lgyUnkCCEh5ZjxRIwE2wPIwEeDyzHjCDQEExA1XRi1JgBLMwMDgUzKg6N4IAQSkEcxMUoYAJ+z/aZGhlYkc+jZNAAFpmjjzlSKQ4waKgJYKETp7JICAeITP1MUJpF7CSX19xT1Nz5gIICAxeQtbDYGUTug5ZliEcToEEJB0fJnlSmLcgFMSwCyDjkX3EUBACIYkCIReAgrdviSCgEU0TgABaRw5E7omENIJP8YMybV/GD8dAghIOr5kJW0I+NjAQxIwggICLgkgIC7pMnYwBFyXkFyPHwxIDIFAPwIICOGQHYE6MwQfGU52DmPBwRJAQIJ1DYY1QaCKANQpQE2skTkg4IoAAuKKLONGRcBWgrK1R7VYjIVATQQQkJpAMkw6BPpnGEOHDjULW716tXR3d0tPT490dXWls1hWAoEOCCAgHcDj0TQJICBp+pVV1U8AAamfKSNGSMBWorK1R7hkTIZAxwQQkI4RMkDMBLhEj9l72O6bAALi2wPM3ziBOr9FVUWAGl8wE0LAEQEExBFYhg2LgOsSlOvxw6KJNRD4lwACQiQkTcBHhlBnhpO0c1hc9AQQkOhdyAI2JBDSBu5DwIgICDRFAAFpijTzOCUQegkpdPucOofBkyWAgCTr2jwWFuMJP6QMKY8oYZWuCCAgrsgyrjMCKW3AMQqgM8cycHQEEJDoXJanwamXgFJfX55Rm/6qEZD0fRz1CnM8oaeUYUUdfBhvJYCAWBHRoWkCbKD/E89RQJuON+arTgABqc6OJ2skQAlnYJjwqTHYGKo2AghIbSgZqAoBTtjlqZGhlWfGE24IICBuuDLqAATYAOsLDwS4PpaMVJ4AAlKeGU9UIEAJpgK0Eo/AtwQsutZGAAGpDSUDtSPACbn5uCDDa555rjMiILl63uG62cAcwi05NAJeEhjdSxFAQErhovOmCFBCCTs28E/Y/onVOgQkVs8FYjcn3EAcUcIMMsQSsOg6IAEEhAApTYANqDSyYB/gABCsa6IwDAGJwk3+jaQE4t8HLi3Avy7ppjs2ApKub2tZGSfUWjBGNQgZZlTu8mosAuIVf5iTs4GE6RcfVnGA8EE9njkRkHh85dRSShhO8UY/OPERvQudLAABcYI1nkE5Ycbjq1AsJUMNxRP+7UBA/PugcQvYABpHnuyEHECSdW2hhSEghTDF34kSRPw+DHkFxFfI3nFnGwLijm0QI3NCDMINWRlBhpuPuxGQBH3NC5ygUyNdEgeYSB1X0GwEpCCo0LtRQgjdQ3nbR3ym6X8EJHK/csKL3IEZmk+GnI7TEZAIfckLGKHTMLktAQ5AcQcGAhKJ/ygBROIozKxEgPiuhM37QwiIdxcMbAAntMAdhHm1EyDDrh2pswEREGdoqw/MC1SdHU+mRYADVNj+REAC8Q8pfCCOwIwgCfB+BOkWQUA8+4UTlmcHMH10BMjQw3EZAuLBF7wAHqAzZZIEOID5dSsC0hB/UvCGQDNNlgR4v/y4HQFxzJ0TkmPADA+BDQiQ4TcXEgiIA9YEsAOoDAmBCgQ4wFWAVuIRBKQErIG6kkLXBJJhIOCAAO+nA6gifAurU6yccDolyPMQaJYAFYL6eJOBVGBJAFaAxiMQCJAAB8DOnIKAFORHClwQFN0gECEB3u9qTkNALNw4oVQLLJ6CQKwEqDAU9xwC0oYVAVQ8gOgJgZQJcIAc2LsIyH98SGFT3gZYGwQ6I8D+0J5f9gLCCaOzF4unIZAbASoU/3s8SwEhAHJ75VkvBNwQyP0Amo2AkIK6eYEYFQIQEMl1f0leQHI/IfByQwACzRLIqcKRpIDk5MBmXw1mgwAEyhBI/QCbjIDkmkKWCWb6QgACfgikuj9FLyCpK7yfcGdWCEDAFYGUKiRRCkhKDnAVpIwLAQiETyD2A3A0ApJqChh+iGMhBCDgmkCs+1vwAhK7QrsOPMaHAATSIhBThSVIAYkJYFqhy2ogAIGQCIR+gA5GQGJN4UIKNmyBAATSJBDq/uhdQEJX2DTDkVVBAAKxEgipQuNFQEICEGsQYTcEIJAGgQ8++ECGDRsm++23X9+CfvrpJ/nss89k3bp1cvTRR8sWW2zR1/bHH3/IRx99JCtXrpQ99thDNttsM1mxYoV0d3dLT0+PdHV1bRKMPvvpp5/KIYccYp7Tj2Y333zzjZmr9dExdt55576/Z8+eLYsXL5Zdd91VxowZ0/fvjQlIqClYGiHIKiAAgRgJfPXVV7L33nvL3XffLVOnTjVLmDNnjhx44IF9y1EBefnll40w/Prrr3LUUUfJ559/btqHDx8uvb29sttuu8myZctk6dKl5t9VSFRQBg8evB6WV155Rc466yz59ttvZbvttjNtX375pYwePXq9fvq3zqEic9FFF8kDDzzQ1/7CCy/IKaecYv52LiCUqGIMa2yGAARcE1izZo0cf/zx8s4778j9998vU6ZMMVnAhAkT5JNPPjFZxqJFi8yJ/9Zbb5Xp06fLgw8+KFdddZW8/fbbsvvuu8sJJ5wgQ4cOlddee02GDBliTG5X4fn555/lySefNGOMGDFC5s+f3ycgKlhnnnmmPPfcc/L333+bMVSs9tprL2PHwQcfbJ6dOHGiXH311fLwww+LZiQ77rijGwGhROU69BgfAhCIncC9994rV1xxhVlGS0C0lKRlqffee0+OPPJI06an/6eeekqef/55k31MmjRJLr/8ctOmG/zJJ59sNvQbbrhB9t9/f7nkkktk0KBBMnPmTCM0Kjgvvvii3HTTTabstaGAPPHEE/L666/LjBkzNkJ6zTXXyLvvvmvaNZtRIdp3332NLYceemh9AkKJKvZwxn4IQKApAnrvcfjhh8vcuXPlkUceMfcNuvFrOemYY45ZL0N45plnzMn/ww8/NG2aAejm3b/89MUXX5iy1IknnihvvvmmyUqOOOIIefXVV2XcuHF9/7v5xx9/XG655RYjCnvuuacRBRWoCy+8UE499VQjDNdee61ceeWVsv3228v5558vY8eONdmRflavXm3uT+644w457bTTOhcQSlRNhRzzQAACKRBo3WNcf/31cs4558gFF1xgBGHy5Mkya9YsOf3009cTkHnz5slxxx1nsgm9m1DxaQnIqlWrzH3JY489ZjKW2267Ta677jqD6fbbbxfNIPp/VIxUDPQuRA/9W2+9tUybNs2U0VTI9M7jsssuk7Vr15rsRkVC70xaAqIlNrVZxU/vbCrdgVCiSiGMWQMEIOCDwI033ij33HOPKStttdVWZsPXspVu0ptvvrnJMjQT2WabbYx5utmryKi4aFbx7LPPykEHHWTavv/+e/PNKM1A9DJeL9F32GEH2WWXXcwluF6ytxOQ1h2IJgAqAvotrp122slcvn/33XemTKVCpdlJ/wxE7230Ul8zorPPPru4gFCi8hFqzAkBCKRGQMs/b7zxhuhXdfXz8ccfm//qyV7vKfbZZx/5+uuvjajoRzdxLT299NJLcthhh/WVj7StdQfSEoT+GYiKVOuupMWwlYG0+mumodmLlrm23HJLI0ALFy6Uk046Sd5//32TlWg57K677jJDaPakQlX4DoQSVWrhy3ogAIGQCPS/Z9BvQY0fP978LkQ39gULFpg7B733OO+88+TOO+802YuWnLTPGWecIQcccIA89NBD8tZbb5lSl17AL1++3Nxp9L+M1zVvKCCaUWg5TLMevRvREpVeumvWoxf3KmTnnnuuES/toxf1Tz/9tPktiX4NuG0JixJVSOGFLRCAQMoELr30UpN1tO4ZtCx17LHHms1bP3pXomUvvZ/QHwKq4Dz66KOmTe899DciLSHQzf7mm282QnDxxRebjV+/oaV3HfrRr+rqPP3LW5oB6VeHW/Np+Uu/daVf49UE4r777jPz60dLYlpKa/2YsE9ARo4cWeiHKCk7krVBAAIQCIHAn3/+aUpc+nXcUaNGbWTSDz/8YERD7z9avyjv1O4lS5aYIfQOReft/9Hfo+icmiHpvU3rh4r6zKDe3t51esehDZoWtX6Q0qlBPA8BCEAAAmkR0HsT/fbXb7/9Zr4G/A9V/3O5qgXkFwAAAABJRU5ErkJggg==';
  }

  componentWillUnmount() {
    this.unsuscribe();
  }

  componentDidMount(){
    this.unsuscribe = this.store.subscribe(() => {
      this.setState(this.store.getState());
    });
  }

  openUploadForm(){
    window.Light = window.YagamiBox.init({
        title: I18n.t('Add file'),
        width:'70%',
        height:'600px',
        setDomContent : (container) => {
            ReactDOM.render(
              <UploadFile
                context={this.context}
                modal={true}
                multi={this.props.multi}
                getData={(data) => this.props.getData(data)}
                featureImage={this.props.featureImage}
              />
            , container);
        }
    })
  }

  removeImage(){
    if (this.props.featureImage === true) {
      this.store.dispatch(removeImageFeature());
    } else {
      this.props.removeSelected();
    }
  }

  getFeatureImg() {
    if (this.props.featureImage === true) {
      return(
        <div ref="image-feature-container" className="image-feature-container">
          <img src={this.state.posts.featured} alt="featured" />
          <section id="jumbotronOffer" className="container-fluid"></section>
        </div>
      );
    } else {
      if (this.props.selectedImage) {
        if (this.props.multi !== true) {
          return(
            <div ref="image-feature-container" className="image-feature-container">
              <img src={this.props.selectedImage} alt="selected" />
            </div>
          );
        } else {
          return(
            <div ref="image-feature-container" className="image-feature-container">
              <div className="row">
                {this.returnImages(this.props.selectedImage)}
              </div>
            </div>
          );
        }
      }
      return null;
    }
  }

  deleteImg(e,i) {
    this.props.removeImage(i);
    e.preventDefault();
  }

  returnImages(images) {
    return images.map((image,i) => {
      return(
        <div className="col-sm-4" key={`galleryImage-${i}`}>
          <div className="imgGallCont">
            <a to="#" className="deleteImg" onClick={(e) => this.deleteImg(e,i)}>X</a>
            <img className="img-response img-gallery" src={image.src} alt={`galleryImage-${i}`} />
          </div>
        </div>
      );
    });
  }

  render() {
  	return(
  	  <div className="card ">
        <div className="card-header"> {this.props.title} </div>
        <div className="card-body">
          {this.getFeatureImg()}
          <UploadLink
            featureImage={this.props.featureImage}
            featured={this.state.posts.featured}
            selectedImage={this.props.selectedImage}
            open={this.openUploadForm.bind(this)}
            remove={this.removeImage.bind(this)}
            multi={this.props.multi}
          />
        </div>
      </div>
  	)
  }
}


 export default  ImageComponent;
