import React, {Component} from 'react';
import {I18n, Translate} from 'react-redux-i18n';
import { connect } from 'react-redux';
import moment       from 'moment';

import Select from 'react-select';

//Actions
import {
    saveRoomUpgrade,
} from '../../actions/passports';


class RoomUpgrade extends Component {
    constructor(props) {
        super(props);

        let passport = this.props.data.credit
        let passport2 = this.props.data.credit ? this.props.data.credit : this.props.data.initial_credit
        let days = this.props.data.days
        let remanent = 0
        let daysOption = []
        for(let loop = 1; passport >= 150; loop ++){
            passport = passport - 150
            remanent = remanent + 150
            if(days >= loop){
                daysOption.push({value:loop, label:loop, id:this.props.data.id, credit:passport, remanent: remanent, icredit: passport2})
            }
        }

        this.state = {
            options: daysOption, 
            passport: this.props.data.credit, 
            remanent: null,
            selectedOption: null
        }
    }

    handleChange = (selectedOption) => {
        this.setState({ selectedOption });
        if(selectedOption != null){
            this.setdata(selectedOption)
        }else{
            this.setState({
                passport: this.props.data.credit, 
                remanent: null
            })
        }
    }

    setdata(selectedOption){
        const {credit, remanent} = selectedOption
        this.setState({passport: credit, remanent: remanent})
        console.log(`Option selected:`, selectedOption.credit);
    }

    onSaveRoom = () => {
        this.props.store.dispatch(saveRoomUpgrade(this.state.selectedOption))
    }

    render() {
        console.log(this.props)
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-12">
                        <label>Credit: {this.state.passport}</label><br/>
                        {
                        this.state.remanent != null &&
                        <div>
                        <label>Remanent: {this.state.remanent}</label><br/>
                        </div>
                        }<label>Nights: {this.props.data.days}</label>
                        <Select 
                            className="row col-md-8 mt-4"
                            style={{color: '#000'}}
                            value={this.state.selectedOption}
                            onChange={this.handleChange}
                            options={this.state.options}
                        />
                        <button 
                            className="btn btn-success col-md-2 mt-3 pull-right" 
                            style={{float: 'right'}}
                            onClick={this.onSaveRoom}
                        >save</button>
                    </div>
                </div>
            </div>
        )
    }
}

export default RoomUpgrade;