import React, {Component} from 'react';
import {I18n, Translate} from 'react-redux-i18n';
import { connect } from 'react-redux';
import moment       from 'moment';

import Select from 'react-select';

//Actions
import {
    loadpostcredit,
} from '../../actions/posts';

import {
    cancel,
    findBookings,
    savebooking,
} from '../../actions/bookings';

import {
    loadPassports,
  } from '../../actions/passports';

//Components
import Table    from '../Table';

class bookingList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            time: moment().format('HH:mm'),
            selectedOption: null,
            options: null,
            bookings: null,
            credit: null
        }

        this.headers = [
            {db:'passport',label:I18n.t('#Control'), icon:'ion-edit', width:'150px',badge:'link', value: (o) => {
              //console.log(o)  
              if(o.icontrol == undefined){
                return 'Not Found'
              }else{
                return o.icontrol;
              }
            }},
            {db:"guest",label:I18n.t('Guest'), type:'text' , align:'left', width:'150px', value:(o,i)=>{
              let name = this.props.passports ? this.props.passports.lastname ? o.name  +' '+ this.props.passports.lastname : o.name : o.name
              //console.log(o)
              return `${name} - ${o.email}`;
            }},             
            {db:"nameOffer",label:I18n.t('Offer'),type:'text' , align:'center',width:'120px', value:(o,i)=>{
                return o.offerData.title ? o.offerData.title.toUpperCase() : 'N/A';
            }},            
            {db:"payment",label:I18n.t('Payment'),type:'text' , align:'center',width:'50px', value:(o,i)=>{
                return o.discPrice ? '$'+o.discPrice : 'N/A';
            }},       
            {db:'arrival',label:I18n.t('Activity date'),type:'datetime',width:'100px', align:'center', icon:'ion-calendar', badge:'primary', value:(o)=>{
              return moment(o.arrival).format('L')
            }},
            {db:"checkout",label:I18n.t('Check Out'),type:'date',align:'center',width:'100px', icon:'ion-calendar', badge:'primary' , value:(o)=>{
              return this.props.passports ? moment(this.props.passports.checkOut).format('L') : 'N/A (this version do not have this data)'
            }},
            {db:'status',label:'Status', icon:'ion-backspace-outline', badge:'primary',width:'20px', align:'center',
            value:(o)=>{
                console.log(o)
                if(!o.bookingCanceled) { 
                    return 'Active';
                }else {
                    return 'Canceled';
                }
            }}, 
        ]
        
        this.loadposts()
    }

    async loadposts(){
        let credit = this.props.passports.initial_credit ? this.props.passports.initial_credit : Number(this.props.passports.credit);
        let posts = await this.props.store.dispatch(loadpostcredit({credit:Number(credit)}))
        //console.log(posts)
        let options = []
        posts.data.map((res) => {
            let addOption = 0
            res.categories.map(res => {
                if(res == `masterservice${credit}`){
                    addOption = addOption + 1
                }
            })
            if(addOption == 0){
                options.push({value:res.id, label:res.title, data:res})
            }
        })
        this.setState({options: options})
        this.request()
    }
    handleChange = (selectedOption) => {
        this.setState({ selectedOption });
        //console.log(`Option selected:`, selectedOption);
    }
    request(params){
        let data = {email:this.props.passports.email}
        this.props.store.dispatch(findBookings(data))
    }
    onsavebooking(ev){
        ev.stopPropagation()
        //console.log((this.props.passports.credit ? Number(this.props.passports.credit) : this.props.passports.initial_credit) - Number(this.state.selectedOption.data.discPrice))
        if(this.state.selectedOption){
            let data = {                                
                post        :this.state.selectedOption.data.id,
                topaid      :this.state.selectedOption.data.discPrice,
                remain      :(this.state.credit == null ? this.props.passports.credit ? Number(this.props.passports.credit) : this.props.passports.initial_credit : this.state.credit) - Number(this.state.selectedOption.data.discPrice),
                stay        :null,
                people      :this.props.passports.people[0],
                date        :moment().format('L'),
                passport    :this.props.passports.id
            }
            this.props.store.dispatch(savebooking(data)).then(res => {
                let credit = this.state.credit == null ? (this.props.passports.credit ? Number(this.props.passports.credit) : this.props.passports.initial_credit) - Number(this.state.selectedOption.data.discPrice) : this.state.credit - Number(this.state.selectedOption.data.discPrice) 
                this.setState({bookings: res, credit:credit})
                let params = {}
                    params.roomUpgrade = true
                    params.page = 1
                    params.rows = 10
                    params.credit = 'all'
                    params.code = 'cbcfa'
                this.props.store.dispatch(loadPassports(params));
            })
        }
    }
    
    render() {
        let creditcompare = this.state.selectedOption && (this.state.selectedOption.data.discPrice) > (this.state.credit == null ? this.props.passports.credit ? Number(this.props.passports.credit) : Number(this.props.passports.initial_credit) : this.state.credit);
        console.log(this.props)
        
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-12">
                        <div className="row mt-4">
                            <div className="col-md-3">
                                Credit: ${this.props.passports.initial_credit ? this.props.passports.initial_credit : Number(this.props.passports.credit)} usd<br/>
                                Remaining: ${this.state.credit == null ? this.props.passports.credit ? Number(this.props.passports.credit) : this.props.passports.initial_credit : this.state.credit} usd<br/>
                                CheckIn: {this.props.passports.checkIn ? moment(this.props.passports.checkIn).format('L') : 'N/A'} <br/>
                                CheckOut: {this.props.passports.checkOut ? moment(this.props.passports.checkOut).format('L') : 'N/A'}</div>
                            <div className="col-md-6">
                                Book an activity:
                                <Select 
                                    value={this.state.selectedOption}
                                    onChange={this.handleChange}
                                    options={this.state.options}
                                />
                            </div>
                            <div className="col-md-3">
                                <img style={{width: '75px', marginRight: '10px', marginTop: '13px', borderRadius: '5px'}} src={this.state.selectedOption && this.state.selectedOption.data.featured}/>
                                <button type="button" className="btn btn-success mt-3 pull-right" onClick={ ev => this.onsavebooking(ev)} disabled={this.state.selectedOption == null ? true : creditcompare}>Book</button>
                            </div>
                            <div className="col-md-3"></div>
                            <div className={`col-md-6 ${!this.state.selectedOption && 'sr-only'}`}>
                                <p>Price: ${this.state.selectedOption && this.state.selectedOption.data.discPrice} usd</p>
                                <p>{creditcompare && 'Insufficient funds'}</p>
                            </div>
                        </div>
                        <hr className="divider" />
                    </div>
                </div>
                <Table 
                    data={this.state.bookings == null ? this.props.data : this.state.bookings} 
                    source={this.request.bind(this)} 
                    headers={this.headers}  
                    placeholder={I18n.t('Search posts')} 
                    search={false} 
                />
            </div>
        )
    }
}

function mapStateToProps(state) {
    console.log(state)
    return {
        data: state.bookings.data,
    }
  }
  export default connect(mapStateToProps, null)(bookingList);