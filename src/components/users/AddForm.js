import React from 'react';
import BtnSubmitLoader from '../BtnSubmitLoader';
import {I18n} from 'react-redux-i18n';
import {
  save
} from '../../actions/users';

class AddForm extends React.Component {
  componentDidMount(){
    this.setDefaultData();
  }
  setDefaultData(){
     if(  this.props.data ){
      for (let ref in this.refs) {
          this.refs[ref].value = this.props.data[ref];
      }
      this.refs.password.value = '';
    }else{
          setTimeout(()=>{
          this.refs.email.value    = '';
          this.refs.password.value = '';
        },60)
    }
  }
  submit(e){
    e.preventDefault();
    var data = {};
    for( let x in this.refs ){
      if( this.refs[x].value !== '' ){
        data[x] = this.refs[x].value;
        this.refs[x].disabled = true;
      }
    }
    this.props.context.store.dispatch(save(data)).then((res)=> console.log(res))
  }
  close(){
    if( window.Light ){
      window.Light.close();
    }
  }
  render() {
    return (
      <div className="padding-50">
          <form onSubmit={this.submit.bind(this)}>
              <div className="row">
                <div className="col-md-12">
                  <div className="form-group">
                    <label>Nombre:</label>
                    <input className="form-control" type="hidden" ref="id" />
                    <input className="form-control" type="text" ref="name" required/>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12">
                  <div className="form-group">
                    <label>Email:</label>
                    <input className="form-control" type="email" ref="email" required/>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12">
                  <div className="form-group">
                    <label>Password:</label>
                    <input className="form-control" type="password" ref="password" />
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-md-6">
                  <div className="form-group">
                    <label className="label">Estatus: </label>
                    <select className="form-control custom-select form-control-md" ref="status">
                      <option value="1">Activo</option>
                      <option value="0">Inactivo</option>
                    </select>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="form-group">
                    <label className="label">Rol: </label>
                      <select className="form-control custom-select form-control-md" ref="role" >
                        <option value="admin">Admin</option>
                        <option value="operator">Operador</option>
                        <option value="operator-adds">Operator only Adds</option>
                        <option value="invited">Invited</option>
                      </select>
                    </div>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12">
                  <div className="form-group form-group-button float-right">
                      <BtnSubmitLoader textBase={I18n.t('Save')} textProcess={I18n.t('Saving data')} icon="ion-edit" className="btn-success btn-sm float-right" />
                      <BtnSubmitLoader  textBase={I18n.t('Cancel')} textProcess={I18n.t('Updating info')} icon="ion-android-cancel"      className="btn-danger btn-sm " type="button" click={this.close.bind(this)}/>
                  </div>
                </div>
              </div>
          </form>
      </div>
    )
  }
}
export default AddForm;
