import React     from 'react';
import BtnSubmitLoader from '../BtnSubmitLoader';
import {I18n,Translate} from 'react-redux-i18n';

import Select from 'react-select';


import{
  save,
  update,
  loadDevices
} from '../../actions/devices';


import{
  search,
  setSearchResult,
  setSelectedValue
} from '../../actions/playlists';


class DeviceForm extends React.Component{
  constructor(props){
    super(props)
    this.store  = this.props.context.store;
    this.state  = this.props.context.store.getState();
    this.socket = this.props.socket;
    this._fd   = {};    
    this.readOnly = this.props.data ? true : false;

    this.diff    = this.props.data.hdd.total - this.props.data.hdd.free  ;
    this.percent = (( this.diff / this.props.data.hdd.total ) * 100 ).toFixed(2);     
    

    this.optionsSelect =  []
  }
  componentDidMount(){
     this.unsuscribe = this.store.subscribe(() => {
        this.setState(this.store.getState());
     });

     if(this.props.data){
       this.refs.serial.value       = this.props.data.serial;
       this.refs.label.value        = this.props.data.label;
       this.refs.list.value         = this.props.data.list;
       this.refs.id.value           = this.props.data.id;
       this.store.dispatch(setSelectedValue({label:this.props.data.list_name,value:this.props.data.list})); 
     }


  }
   componentWillUnmount() {
    this.unsuscribe();

  } 
  submit(e){
    e.preventDefault();

    this.stateForm(true);
    this.getDataForm();
   
   if(this.props.data ){      
      if( this.refs.list.label ){ this._fd.list_name =  this.refs.list.label }      
      this.props.context.store.dispatch(update(this._fd,(device)=>{       
        if(device.status === 1 ){
          this.socket.emit('device:update',device);
          window.bootbox.alert('Listo! El dispositivo ha comenzado la sincronización de la nueva lista de reproducción');          
          this.props.context.store.dispatch(loadDevices());
        }else{
          window.bootbox.alert('Listo! Los datos han sido guardados, pero el dispositivo se encuentra inactivo, la lista se sincronizará al reiniciar.');                   
        }
        this.close();
      })) 

   }else{
      this.props.context.store.dispatch(save(this._fd,(device)=>{
          this.stateForm(false);
          window.bootbox.alert('Los datos ha sido registrados correctamente');
          this.close();
      }))
    }
  }
   stateForm(def){
    for( let x in this.refs ){
      if( ['INPUT','SELECT','BUTTON'].indexOf( this.refs[x].tagName )  ){
        this.refs[x].disabled = def;
      }
    }
  }
  getDataForm(){
    for (const i in this.refs) {
      if( typeof(this.refs[i].value) !== 'undefined' ){
        this._fd[i] = this.refs[i].value;
      }
    }
  }
  resetForm(){
    for (const i in this.refs) {
      if(this.refs[i].tagName === 'INPUT'){
        this.refs[i].value = '';
      }
    }
  }
  close(){
    if( window.Light ){
      window.Light.close();
    }
  }
  refresh(){
     window.bootbox.alert('Refresh');
  }
  reboot(){
    window.bootbox.alert('Rebooting');
  }
  searchList(s){
    this.store.dispatch(search(s));
  }
 
  onChangeSelect(val){        
    this.refs.list.value      = val.value;
    this.refs.list_name.value = val.label;    
    this.store.dispatch(setSelectedValue(val));
  }

  
  loadOptions(value){

    let rdata = [];
    return this.store.dispatch(search(value)).then(response =>{

      response.data.forEach((item,i)=>{
        rdata.push({label:item.title,value:item.id})
      })

      this.store.dispatch(setSearchResult(rdata));
      this.isLoadingSelect = false;
      return {options:rdata}
    })


  }
  render(){

    return (
      <div className="padding-40">       
       <div className="row">       
          <div className="col-md-12">
            <form onSubmit={this.submit.bind(this) }>
              <input type="hidden" ref="id"  />
              <div className="form-group">
                <label><Translate value="Serial"/></label>
                <input type="text" className="form-control" ref="serial"  required readOnly={this.readOnly} />
              </div>
              <div className="form-group">
                <label><Translate value="Label"/></label>
                <input type="text" className="form-control" ref="label"  required />
              </div>            
               <div className="form-group">
                  <label><Translate value="Set list"/></label>
                  <input type="hidden" ref="list" />
                  <input type="hidden" ref="list_name" />
                  <Select.Async name="list"  value={this.state.playlists.list_value} searchable={true} loadOptions={this.loadOptions.bind(this)} isLoading={this.isLoadingSelect} onChange={this.onChangeSelect.bind(this)} />
              </div>
              <div className="form-group">                
               <label> <strong>{ this.props.data.hdd.free } GB</strong> free space from <strong>{ this.props.data.hdd.total } GB</strong> in device </label>
                <div className="progress">
                  <div className="progress-bar bg-success" role="progressbar"  style={{width: this.percent +'%',height:'20px'}} aria-valuenow={this.percent} aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </div>
              
              <hr className="separator" />
              <div className="form-group float-right">
                <BtnSubmitLoader  textBase={I18n.t('Reboot ')} textProcess={I18n.t('Rebooting')} icon="ion-gear-a"      className="btn-warning btn-sm " type="button" click={this.reboot.bind(this)}/>                
                <BtnSubmitLoader  textBase={I18n.t('Refresh device info')} textProcess={I18n.t('Updating info')} icon="ion-refresh"      className="btn-success btn-sm " type="button" click={this.refresh.bind(this)}/>                
                <BtnSubmitLoader  textBase={I18n.t('Update ')}       textProcess={I18n.t('Saving data')}   icon="ion-edit" className="btn-sm "/>
                <BtnSubmitLoader  textBase={I18n.t('Cancel')} textProcess={I18n.t('Updating info')} icon="ion-android-cancel"      className="btn-danger btn-sm " type="button" click={this.close.bind(this)}/>
              </div>
            </form>
          </div>
       </div>
      </div>
    )
  }
}

export default DeviceForm;