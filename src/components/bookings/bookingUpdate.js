import React, {Component} from 'react';
import PropTypes    from 'prop-types';
import moment       from 'moment';

//import Clock from 'react-clock'

import { bookConfirmation } from '../../actions/bookings';

//import { TimePicker } from "react-time-picker";



class bookingUpdate extends Component {
    static get contextTypes() {
        return {
            router: PropTypes.object.isRequired,
            store: PropTypes.object.isRequired,
            data: PropTypes.object.isRequired
        } 
    }
    constructor(props, context){
        super(props, context);
        this.context = context;
        this.store   = this.context.store;

        this.state = {
            time: moment().format('HH:mm'),
        }

        this.saveReservation = this.saveReservation.bind(this)
    }
    componentDidMount(){
        
    }
    handleChange(time){
        console.log(time.target.value)
        this.setState({time:time.target.value})
    }
    saveReservation(){
        const id     = this.refs.id.value
        const time   = this.state.time
        const checkactive = this.refs.timepicker.checked
        console.log(checkactive)
        if(checkactive){
            this.props.context.store.dispatch(bookConfirmation( {id: id, time: time} ))  
        }else{
            alert('Select the checkbox to continue')
        }
        //this.props.context.store.dispatch(bookConfirmation( {id: id, time: time} ))
    }
    render() {
        console.log(this.props)
        const { name, email, id, offerData, reservation } = this.props.data;
        return (
            <div className="padding-50">
                <form>
                    <div className="row">
                        <div className="col-md-6">
                            <div className="form-group">
                                <label>Name:</label>
                                <input className="form-control" type="hidden" ref="id" value={id}/>
                                <input className="form-control" type="text" ref="name" value={name} readOnly />
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="form-group">
                                <label>Email:</label>
                                <input className="form-control" type="text" ref="email" value={email} readOnly />
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-12">
                        <div className="form-group">
                            <label>offer:</label>
                            <input className="form-control" type="text" ref="offer" value={offerData.title} readOnly />
                        </div>
                        </div>
                    </div>

                    <div className="row">   

                        <div className="col-md-12">
                            <div className="form-group">
                                <label><i style={{color: `red`}}>(Assign a timestamp of 24 hours to send the notification)</i></label><br/>
                                <label>Time Requested: {reservation}</label><br/>
                                
                                <label>Time Assigned: </label> 
                                <input
                                    className="form-control"
                                    type="time"
                                    onChange={this.handleChange.bind(this)}
                                    value={this.state.time}
                                    required
                                /> 
                                <label><i style={{color: `red`}}> (Don't forget to assign Time)</i></label><br/>
                                <input type="checkbox" ref="timepicker" /> Check this box to confirm the time assigned
                            </div>
                        </div>

                    </div>
                    <button type="button" className="btn btn-primary active" style={{position: `relative`,float: `right`}} onClick={this.saveReservation}>Save</button>
                    
                </form>
            </div>
        )
    }
}


export default bookingUpdate;
